<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('auth/login', ['as' => 'login', 'uses' => 'Auth\\LoginController@showLoginForm']);

Route::post('auth/login', ['as' => 'login.post', 'uses' => 'Auth\\LoginController@login'] );
Route::get('auth/logout', ['as' => 'logout', 'uses' => 'Auth\\LoginController@logout']);

Route::get('auth/{provider}', 'Auth\\SocialAuthController@redirect');
Route::get('auth/{provider}/callback', 'Auth\\SocialAuthController@callback');

Route::get('/vodoem/{id?}', 'IndexController@getVodoemAction');
Route::get('/region/', ['as' => 'region-info.get', 'uses' => 'IndexController@getRegionsAction']);
Route::get('/region/{id?}', ['as' => 'region-posts.get', 'uses' => 'IndexController@getRegionAction']);
Route::get('/district/{id?}', ['as' => 'district-posts.get', 'uses' => 'IndexController@getDistrictAction']);
Route::get('/region/{regionID?}/water/{waterID?}', ['as' => 'region-water-posts.get', 'uses' => 'IndexController@getRegionWaterAction']);
Route::get('/district/{districtID?}/water/{waterID?}', ['as' => 'district-water-posts.get', 'uses' => 'IndexController@getDistrictWaterAction']);

Route::get('/communities', ['as' => 'communities.get', 'uses' => 'CommunitiesController@getListAction']);
Route::get('/communities/{id}', ['as' => 'communities-posts.get', 'uses' => 'CommunitiesController@getPostsAction']);

Route::get('/vodoem/{id}', ['as' => 'water-posts.get', 'uses' => 'IndexController@getVodoemAction']);

Route::group(['prefix' => 'import'], function () {
   Route::get('country/{id}', 'ImportController@getImportCountries');
   Route::get('regions', 'ImportController@getImportRegions');
   Route::get('districts', 'ImportController@getImportDistricts');
   Route::get('localities', 'ImportController@getImportLocalities');

    Route::get('posts', ['uses' => 'ImportController@getImportPosts']);
    Route::get('comments', ['uses' => 'ImportController@getImportComments']);
    Route::get('users', ['uses' => 'ImportController@getImportUsers']);
});

Route::group(['prefix' => 'osm'], function () {
   Route::get('/lakes', ['uses' => 'OsmController@lakes']);
   Route::get('/rivers', ['uses' => 'OsmController@rivers']);
   Route::get('test', ['uses' => 'OsmController@test']);
    Route::get('find-group-waters', ['uses' => 'OsmController@findGroupWaters']);
});

Route::post('/comments', ['as' => 'comments:create.post', 'uses' => 'CommentsController@create']);
Route::post('/comments/load', ['as' => 'comments:load.post', 'uses' => 'CommentsController@load']);
Route::post('/comments/add_images', ['as' => 'comments:add_images.post', 'uses' => 'CommentsController@addImages']);

Route::group(['prefix' => 'posts', 'as' => 'posts:'], function () {
    Route::get('/create', ['as' => 'create.get', 'uses' => 'PostsController@create']);
    Route::get('/{id}', ['as' => 'view.get', 'uses' => 'PostsController@view'])->where('id', '[0-9]+');
});

Route::get('/search', ['as' => 'search.get', 'uses' => 'SearchController@searchAction']);

Route::get('/youtube', ['as' => 'youtube.index', 'uses' => 'YoutubeController@index']);

Route::get('/{slug?}', ['as' => 'index.get', 'uses' => 'IndexController@getIndexAction'])
    ->middleware(\App\Http\Middleware\CheckPostCategory::class)
    ->where(['slug' => '^((post)|(humor)|(vopros)|(recept)|(interesnoe)|(undefined))']);