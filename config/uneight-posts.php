<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 20.04.2018
 * Time: 12:10
 */

return [

    'relations' => [
        \App\Entities\PostAdditionalInformationEntity::class,
        \Uneight\Categories\Entities\CategoryEntity::class,
    ]

];