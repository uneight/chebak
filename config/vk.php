<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 6/10/18
 * Time: 5:38 PM
 */

return [

    'groups' => [
        ['id' => -99302662, 'email' => 'AdmPervouralsk@chebak.ru'],
        ['id' => -76200174, 'email' => 'AdmKamensk@chebak.ru'],
        ['id' => -52318018, 'email' => 'AdmTagil@chebak.ru'],
        ['id' => -9496874, 'email' => 'AdmEkb@chebak.ru'],
        ['id' => -39481810, 'email' => 'AdmMagnitogorsk@chebak.ru'],
        ['id' => -74526345, 'email' => 'admuchalu@chebak.ru'],
        ['id' => -76403964, 'email' => 'AdmBeloretsk@chebak.ru'],
        ['id' => -64982281, 'email' => 'AdmSibai@chebak.ru'],
        ['id' => -64982281, 'email' => 'AdmMiass@chebak.ru'],
        ['id' => -68066470, 'email' => 'AdmChel@chebak.ru'],
        ['id' => -85385794, 'email' => 'AdmUstKatav@chebak.ru'],
        ['id' => -46348121, 'email' => 'AdmKumertau@chebak.ru'],
        ['id' => -51470129, 'email' => 'AdmSalavat@chebak.ru'],
        ['id' => -73667968, 'email' => "AdmOrenburg@chebak.ru"],
        ['id' => -99062614, 'email' => "AdmBuzuluk@chebak.ru"],
        ['id' => -23262411, 'email' => "admtumen@chebak.ru"],
        ['id' => -72190180, 'email' => "admsurgut@chebak.ru"],
        ['id' => -102749484, 'email' => "AdmNojabrsk@chabak.ru"],
        ['id' => -46414866, 'email' => "admtobolsk@chebak.ru"],
        ['id' => -84926639, 'email' => "AdmIshim@chebak.ru"],
        ['id' => -102454810, 'email' => "admkogalim@chebak.ru"],
        ['id' => -73913493, 'email' => "AdmOmsk@chebak.ru"],
        ['id' => -102817713, 'email' => "AdmShadrinsk@chebak.ru"],
        ['id' => -32053004, 'email' => "AdmTomsk@chebak.ru"],
        ['id' => -92950000, 'email' => "AdmPerm@chebak.ru"],
    ],

];