<?php

use Illuminate\Database\Seeder;

class AddPostCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'old_id' => 1,
                'name' => 'Юмор',
                'slug' => 'humor',
            ],
            [
                'old_id' => 2,
                'name' => 'Вопросы',
                'slug' => 'vopros',
            ],
            [
                'old_id' => 3,
                'name' => 'Мастерская',
                'slug' => 'kulibin',
            ],
            [
                'old_id' => 4,
                'name' => 'Рецепты',
                'slug' => 'recept',
            ],
            [
                'old_id' => 5,
                'name' => 'Интересное',
                'slug' => 'interesnoe',
            ],
            [
                'old_id' => 0,
                'name' => 'Отчеты',
                'slug' => 'post',
            ],
            [
                'old_id' => 6,
                'name' => 'undefined',
                'slug' => 'undefined',
            ],
        ];

        foreach ($categories as $category) {
            \Uneight\Categories\Entities\CategoryEntity::create([
                'type' => 'uneight-posts',
                'name' => $category['name'],
                'slug' => $category['slug'],
                'old_id' => $category['old_id']
            ]);
        }
    }
}
