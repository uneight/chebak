<?php

use Illuminate\Database\Seeder;

class AddRootUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Database\Eloquent\Model::unguard();

        \App\Entities\UserEntity::create([
            'login' => 'root',
            'email'    => 'root@chebak.ru',
            'password' => Hash::make('chebak2018root'),
            'role' => \App\Entities\UserEntity::ROLE_ROOT
        ]);
    }
}
