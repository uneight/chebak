<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->renameColumn('login', 'phone')->unique()->change();
            $table->string('email')->nullable()->change();

            $table->string('social_provider')->nullable();
            $table->string('social_provider_id')->nullable();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->string('phone')->nullable()->change();
        });

        DB::table('users')->update(['phone' => null]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->renameColumn('phone', 'login');
            $table->string('email')->nullable(false)->change();
            $table->dropColumn('social_provider');
            $table->dropColumn('social_provider_id');
        });
    }
}
