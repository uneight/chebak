<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeoTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /** Таблица стран **/
        Schema::create('geo_countries', function (Blueprint $table) {
            $table->increments('id');

            $table->string('api_id_htmlweb', 2)->unique();
            $table->string('api_id_kladr')->nullable();

            $table->string('name');
            $table->string('full_name');

            $table->string('iso_code', 3);
            $table->string('iso_name', 2);

            $table->jsonb('source');
            $table->boolean('active');
            $table->softDeletes();
            $table->timestamps();
        });

        /** Таблица областей **/
        Schema::create('geo_regions', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('api_id_htmlweb')->unsigned()->unique();
            $table->bigInteger('api_id_kladr')->unsigned()->nullable();

            $table->string('wikidata')->nullable();

            $table->integer('country_id')->unsigned();
            $table->foreign('country_id')->references('id')->on('geo_countries')->onDelete('cascade');

            $table->string('type')->nullable();
            $table->string('name');

            $table->string('auto_number_codes');
            $table->string('iso', 3);
            $table->string('timezone');

            $table->jsonb('source');
            $table->boolean('active');
            $table->softDeletes();
            $table->timestamps();
        });

        /** Таблица районов **/
        Schema::create('geo_districts', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('api_id_htmlweb')->unsigned()->unique();
            $table->bigInteger('api_id_kladr')->unsigned()->nullable();

            $table->string('wikidata')->nullable();

            $table->integer('region_id')->unsigned();
            $table->foreign('region_id')->references('id')->on('geo_regions')->onDelete('cascade');

            $table->string('type')->nullable();
            $table->string('name');

            $table->jsonb('source');
            $table->boolean('active');
            $table->softDeletes();
            $table->timestamps();
        });

        /** Таблица населенных пунктов **/
        Schema::create('geo_localities', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('api_id_htmlweb')->unsigned()->unique();
            $table->bigInteger('api_id_kladr')->unsigned()->nullable();

            $table->string('wikidata')->nullable();

            $table->integer('district_id')->unsigned();
            $table->foreign('district_id')->references('id')->on('geo_districts')->onDelete('cascade');

            $table->integer('region_id')->unsigned()->nullable();
            $table->foreign('region_id')->references('id')->on('geo_regions')->onDelete('cascade');

            $table->string('type')->nullable();
            $table->string('name');

            $table->float('latitude', 10, 6)->nullable();
            $table->float('longitude', 10, 6)->nullable();

            $table->jsonb('source');
            $table->boolean('active');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geo_localities');
        Schema::dropIfExists('geo_districts');
        Schema::dropIfExists('geo_regions');
        Schema::dropIfExists('geo_countries');
    }
}
