<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('old_id')->nullable();

            $table->string('login')->unique();
            $table->string('email')->unique();

            $table->string('password')->nullable();

            $table->integer('role')->default(\App\Entities\UserEntity::ROLE_USER);

            $table->string('full_name')->nullable();

            $table->string('avatar')->nullable();

            $table->date('birthday')->nullable();
            $table->integer('rating')->default(0);
            $table->string('referrer_id')->nullable();

            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
