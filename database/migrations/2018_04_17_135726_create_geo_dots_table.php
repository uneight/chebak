<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeoDotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geo_dots', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->text('alias');

            $table->integer('region_id')->nullable()->unsigned();
            $table->foreign('region_id')->references('id')->on('geo_regions')->onDelete('cascade');

            $table->integer('district_id')->nullable()->unsigned();
            $table->foreign('district_id')->references('id')->on('geo_districts')->onDelete('cascade');

            $table->integer('locality_id')->nullable()->unsigned();
            $table->foreign('locality_id')->references('id')->on('geo_localities')->onDelete('cascade');

            $table->integer('water_id')->nullable()->unsigned();
            $table->foreign('water_id')->references('id')->on('geo_water')->onDelete('cascade');

            $table->float('latitude', 10, 6)->nullable();
            $table->float('longitude', 10, 6)->nullable();

            $table->boolean('active')->default(true);
            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geo_dots');
    }
}
