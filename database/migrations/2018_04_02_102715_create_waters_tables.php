<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWatersTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geo_water', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('api_id')->nullable();
            $table->string('api_type')->nullable();

            $table->string('wikidata')->nullable();

            $table->string('type')->nullable();
            $table->string('name')->nullable();
            $table->string('alias')->nullable();

            $table->jsonb('nodes')->nullable();
            $table->boolean('active')->default(true);
            $table->softDeletes();
            $table->timestamps();

            $table->index(['api_id']);
        });

        Schema::create('geo_region_waters', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('region_id')->nullable()->unsigned();
            $table->foreign('region_id')->references('id')->on('geo_regions')->onDelete('cascade');

            $table->integer('water_id')->nullable()->unsigned();
            $table->foreign('water_id')->references('id')->on('geo_water')->onDelete('cascade');

            $table->index(['region_id', 'water_id']);
        });

        Schema::create('geo_district_waters', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('district_id')->nullable()->unsigned();
            $table->foreign('district_id')->references('id')->on('geo_districts')->onDelete('cascade');

            $table->integer('water_id')->nullable()->unsigned();
            $table->foreign('water_id')->references('id')->on('geo_water')->onDelete('cascade');

            $table->index(['district_id', 'water_id']);
        });

        Schema::create('geo_locality_waters', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('locality_id')->nullable()->unsigned();
            $table->foreign('locality_id')->references('id')->on('geo_localities')->onDelete('cascade');

            $table->integer('water_id')->nullable()->unsigned();
            $table->foreign('water_id')->references('id')->on('geo_water')->onDelete('cascade');

            $table->index(['locality_id', 'water_id']);
        });

        Schema::create('geo_water_coordinates', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('water_id')->nullable()->unsigned();
            $table->foreign('water_id')->references('id')->on('geo_water')->onDelete('cascade');

            $table->float('latitude', 10, 6)->nullable();
            $table->float('longitude', 10, 6)->nullable();

            $table->index(['water_id', 'latitude', 'longitude']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geo_water_coordinates');
        Schema::dropIfExists('geo_locality_waters');
        Schema::dropIfExists('geo_district_waters');
        Schema::dropIfExists('geo_region_waters');
        Schema::dropIfExists('geo_water');
    }
}
