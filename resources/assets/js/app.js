
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

require('photoswipe');

Vue.component('uneight-autocomplete', require('./../../../modules/uneight/apanel/resources/assets/js/components/autocomplete'));
Vue.component('search-form', require('../js/components/search/SearchFormComponent'));
Vue.component('comments', require('../js/components/comments/Comments'));

const app = new Vue({
    el: '#app'
});
