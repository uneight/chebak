<div class="box">
    <div class="box-header pb-0">
        <div class="chat-item mb-0">
            <span class="w-40 circle green avatar">P </span>
            <div>
                <div>
                    <a href="/profile/{{ $post->user_id }}">{{ (!!$post->user) ? $post->user->full_name : 'chebak.ru' }}</a>
                    <span class="text-muted"><i
                                class="fa fa-fw fa-clock-o"></i> {{ $post->created_at->diffForHumans() }}</span>
                </div>
                <div class="text-muted">
                    @if(!empty($post->group->region))
                        <a href="{{ route('region-posts.get', $post->group->region->id) }}">{{$post->group->region->name}}</a>,
                        <a href="{{ route('district-posts.get', $post->group->district->id) }}">{{$post->group->district->name}}</a>
                    @endif

                    @if(isset($post->group->locality->name) && $post->group->locality->name != $post->group->district->name)
                        ,
                        <a href="{{ route('district-posts.get', $post->group->district->id) }}">{{$post->group->locality->name}}</a>
                    @endif

                    @if((isset($post->water) && !empty($post->water->name)))
                        , <a href="{{ route('water-posts.get', $post->water->id) }}">{{$post->water->name}}</a>
                    @endif

                </div>
                @if(Auth::user() && Auth::user()->role != Uneight\Users\Entities\UserEntity::ROLE_USER && !is_null($post->waters_founded))
                    <div class="">
                        @foreach(json_decode($post->waters_founded) as $water)
                            {{ $water->name . ', ' }}
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
        <div class="box-tool">
            <ul class="nav nav-xs">
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown"><i class="fa fa-fw fa-ellipsis-v"></i></a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="{{ route('posts:view.get', $post->id) }}">Читать
                            полностью</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item">Сообщить о спаме</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-body">

        @if(!empty($post->title))
            <div>{!! $post->title !!}</div>
        @endif

        <div>{!! $post->content !!}</div>
        @if(count($post->attachments) > 0)
            <div id="nanogallery2-{{$post->id}}" class="mt-3"></div>
            @push('scripts')
                <script type="text/javascript">
                    $('#nanogallery2-{{$post->id}}').nanogallery2({
                        items: [
                                @foreach($post->attachments as $key => $attachment)
                                @if($attachment->type == 'image' && $attachment->active == 't')
                            {
                                src: '{{ $attachment->value }}',
                                srct: '{{ $attachment->value }}',
                            },
                                @endif
                                @if($attachment->type == 'youtube' && $attachment->active == 't')
                            {
                                src: 'https://www.youtube.com/watch?v={{ $attachment->value }}',
                                srct: 'https://img.youtube.com/vi/{{ $attachment->value }}/0.jpg',
                            },
                                @endif
                                @if($attachment->type == 'vkontakte' && $attachment->active == 't')
                            {
                                src: '{{ $attachment->value }}',
                                srct: '/images/post-format-thumb-video.svg',
                            },
                            @endif
                            @endforeach

                        ],
                        itemsBaseURL: '{{ env('APP_URL') }}',
                        thumbnailBorderVertical: 0,
                        thumbnailBorderHorizontal: 0,
                        thumbnailLabel: {
                            display: false,
                        },
                        galleryDisplayMode: 'rows',
                        galleryMaxRows: 1,
                        thumbnailWidth: 'auto',
                        thumbnailHeight: 200,
                        locationHash: false,
                        thumbnailHoverEffect2: 'image_scale_1.00_1.20',
                        fnThumbnailOpen: viewWithFancybox
                    });

                    function viewWithFancybox(items) {
                        var lstItems = [];

                        for (var i = 0; i < items.length; i++) {
                            lstItems.push({src: items[i].responsiveURL()});
                        }
                        jQuery.fancybox.open(lstItems);

                    }
                </script>
            @endpush
        @endif
        <div class="mt-3 text-muted">
            Комментариев: {{ count($post->comments) }}
            <div class="pull-right">
                <a href="">0 <b class="fa fa-thumbs-o-up text-muted mr-2"></b></a>
                <a href="">0 <b class="fa fa-thumbs-o-down text-muted"></b></a>
            </div>
        </div>
    </div>
    <comments :user="{{ json_encode(Auth::user()) }}" :post_id="{{ $post->id }}"></comments>
</div>