@foreach($posts as $item)
    @include('chunks.post-item', ['post' => $item])
@endforeach

<div class="row">
    <div class="">{{ $posts->links('vendor.pagination.bootstrap-4') }}</div>
</div>