<div class="box">
    <div class="box-header light lt">
        <h3>Поиск</h3>
        <small>Поиск по сайту</small>
    </div>
    <div class="box-body">
        {{ Form::open(['route' => 'search.get', 'method' => 'GET']) }}
        <div class="row form-group">
            <div class="col-md-6">
                <label>Поиск:</label>
                {{ Form::input('text', 'q', null, ['class' => 'form-control', 'placeholder' => 'Введите текст для поиска']) }}
            </div>
            <div class="col-md-6">
                <label>Где искать:</label>
                {{ Form::select('type', ['all' => 'Везде', 'waters' => 'Водоемы', 'posts' => 'Публикации'], 'all', ['class' => 'form-control form-control-sm']) }}
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                {{ Form::submit('Поиск', ['class' => 'btn btn-fw primary']) }}
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>