@extends('layouts.app')

@section('page.content')

    @include('chunks.search.form')

    <div class="box">
        <div class="box-header light lt">
            <h3>{{ $district->name }}</h3>
            <small>ТОП 15 самых популярных водоемов России, по количеству размещенных отчетов с рыбалок.</small>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-8">Вести с водоемов. База данных отчетов рыбаков отсортированная по водоемам и
                    регионам {{ $district->name }}.
                </div>
                <div class="col-4">
                    <div><strong>Водоемов:</strong> {{ $waters }}</div>
                    <div><strong>Отчетов:</strong> {{ $postCount }}</div>
                </div>
            </div>
        </div>
    </div>

    <div class="box">
        <div class="box-header light lt">
            <h3>Водоемы</h3>
            <small>Самые популярные водоемы 7, по количеству размещенных отчетов с рыбалок.</small>
        </div>
        <div class="box-body">
            <div class="row">
                @foreach(array_chunk($topWaters->all(), 2) as $chunk)
                    <div class="col-4">
                        @foreach($chunk as $water)
                            @if(!empty($water->name))
                                <div>
                                    <a href="{{ route('water-posts.get', [$water->id]) }}"
                                       class="btn btn-link">
                                        {{ $water->name }}
                                        @if($water->posts_count > 0)
                                            <b class="badge badge-pill primary">{{ $water->posts_count }}</b>
                                        @endif
                                    </a>
                                </div>
                            @endif
                        @endforeach
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    @include('chunks.posts')

@endsection