@extends('layouts.app')

@section('page.content')

    @include('chunks.search.form')

    <div class="box">
        <div class="box-header light lt">
            <h3>{{ (!empty(\Uneight\Geo\Entities\WaterEntity::TYPES[$water->type]) ? \Uneight\Geo\Entities\WaterEntity::TYPES[$water->type] : '') . ' ' . $water->name }}</h3>
            <small>Вести с водоемов. Отчеты с рыбалки
                на {{ !empty(\Uneight\Geo\Entities\WaterEntity::TYPES[$water->type]) ? \Uneight\Geo\Entities\WaterEntity::TYPES[$water->type] : '' }} {{ $water->name }}</small>
        </div>
    </div>


    <div class="box">
        <div class="box-header light lt">
            <h3>География</h3>
            <small>{{ (!empty(\Uneight\Geo\Entities\WaterEntity::TYPES[$water->type]) ? \Uneight\Geo\Entities\WaterEntity::TYPES[$water->type] : '') . ' ' . $water->name }} в других
                регионах
            </small>
        </div>
        <div class="box-body">
            @foreach($regions as $region)
                @if(!empty($region->name))
                    <a href="{{ route('region-posts.get', $region->id) }}"
                       class="btn btn-sm btn-outline btn-rounded b-primary mb-1">
                        {{ $region->name }}
                    </a>
                @endif
            @endforeach
        </div>
    </div>

    @if(!empty($node))
        <div class="box">
            <div class="box-header light lt">
                <h3>Карта</h3>
                <small>Как проехать на {{ $water->name }}? Где оно находится? Местоположение {{ $water->name }}
                    на карте
                </small>
            </div>
            <div class="box-body">
                <div id="map" class="col-12" style="height: 300px"></div>
            </div>
        </div>
    @endif

    <div class="box">
        <div class="box-header light lt">
            <h3>Рыба</h3>
            <small>
                Что ловится на {{ $water->name }}? Виды рыбы которые можно поймать на {{ $water->name }}
            </small>
        </div>
        <div class="box-body">
            <div class="text-muted text-center">Недоступно</div>
        </div>
    </div>

    @include('chunks.posts')

@endsection

@push('scripts')
    @if(!empty($node))
        <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
        <script type="text/javascript">
            ymaps.ready(init);
            var myMap,
                myPlacemark;

            function init() {
                myMap = new ymaps.Map("map", {
                    center: [{{ $node->lat }}, {{ $node->lon }}],
                    zoom: 10
                });

                myPlacemark = new ymaps.Placemark([{{ $node->lat }}, {{ $node->lon }}], {
                    hintContent: '{{ (!empty(\Uneight\Geo\Entities\WaterEntity::TYPES[$water->type]) ? \Uneight\Geo\Entities\WaterEntity::TYPES[$water->type] : '') . ' ' . $water->name }}',
                    balloonContent: '{{ (!empty(\Uneight\Geo\Entities\WaterEntity::TYPES[$water->type]) ? \Uneight\Geo\Entities\WaterEntity::TYPES[$water->type] : '') . ' ' . $water->name }}'

                }, {draggable: false});

                myMap.geoObjects.add(myPlacemark);

                myMap.events.add('click', function (e) {
                    var coords = e.get('coords');

                    if (myPlacemark) {
                        myPlacemark.geometry.setCoordinates(coords);
                    } else {
                        myPlacemark = createPlacemark(coords);
                        myMap.geoObjects.add(myPlacemark);
                        // Слушаем событие окончания перетаскивания на метке.
                        myPlacemark.events.add('dragend', function () {
                            getAddress(myPlacemark.geometry.getCoordinates());
                        });
                    }

                    $("#edPostCoords").val(coords);
                    getLocation(coords);

                    var ret = squery("/service.php", "code=geo&sub=getvodoem&x=" + $("#edPostCoords").val());

                    if (ret.text) {
                        $("#divVodoems").html(ret.text);
                    } else {
                        $("#divVodoems").html("");
                    }
                });

            }
        </script>
    @endif
@endpush