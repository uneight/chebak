<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>CHEBAK.RU - социальная сеть рыбаков, охотников, грибников и туристов.</title>

    <!-- Styles -->
    <link href="/dist/css/app.css" rel="stylesheet">
    <link href="https://unpkg.com/nanogallery2@2.2.0/dist/css/nanogallery2.min.css" rel="stylesheet" type="text/css">
    <link href="https://nanogallery2.nanostudio.org/codepen/jquery.fancybox.min.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="app" id="app">
    <div id="content" class="app-content box-shadow-0" role="main">
        @include('layouts.chunks.navbar')
        <div class="content-main " id="content-main">
            <div class="container pt-4">
                <div class="row">
                    @include('layouts.chunks.sidebar')
                    <div class="col-12 col-sm-8 col-md-9 col-lg-9">
                        @yield('page.content')
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.chunks.footer')
    </div>
</div>
<!-- Scripts -->
<script type="text/javascript" src="/dist/js/app.js"></script>
<script type="text/javascript" src="https://nanogallery2.nanostudio.org/js/jquery.nanogallery2.min.js"></script>
<script type="text/javascript" src="/vendor/jquery.fancybox.js"></script>
@stack('scripts')
</body>
</html>
