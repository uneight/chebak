<div class="col-12 col-sm-4 col-md-4 col-lg-3 mb-4">
    <div class="box">
        <div class="box-header light lt">
            <h3>Все записи</h3>
            <small>Все записи пользователей.</small>
        </div>
        <div class="box-body">
            @foreach($categories as $category)
                <div>
                    <a href="/{{ $category->slug }}" class="btn btn-outline b-primary mb-1 btn-block text-left">
                        {{ $category->name }}
                    </a>
                </div>
            @endforeach
        </div>
    </div>

    <a href="{{ route('region-info.get') }}" class="box mb-4 d-block">
        <div class="box-header primary">
            <h3>Водоемы</h3>
            <small>База водоемов с отчетами рыбаков.</small>
        </div>
    </a>

    <a href="{{ route('communities.get') }}" class="box mb-4 d-block">
        <div class="box-header success">
            <h3>Сообщества</h3>
            <small>Список сообществ с отчетами.</small>
        </div>
    </a>

    <div class="text-center mt-2">
        <span id="vk_groups"></span>
    </div>

    <div class="text-center mt-2">
        <span id="ok_group_widget"></span>
    </div>
</div>

@push('scripts')
    <script type="text/javascript" src="https://userapi.com/js/api/openapi.js?52"></script>
    <script type="text/javascript">
        VK.Widgets.Group("vk_groups", {mode: 0, width: "213", height: "190"}, 97749383);
    </script>

    <script>
        !function (d, id, did, st) {
            var js = d.createElement("script");
            js.src = "https://connect.ok.ru/connect.js";
            js.onload = js.onreadystatechange = function () {
                if (!this.readyState || this.readyState == "loaded" || this.readyState == "complete") {
                    if (!this.executed) {
                        this.executed = true;
                        setTimeout(function () {
                            OK.CONNECT.insertGroupWidget(id,did,st);
                        }, 0);
                    }
                }};
            d.documentElement.appendChild(js);
        }(document,"ok_group_widget","52764681240660","{width:240,height:215}");
    </script>
@endpush