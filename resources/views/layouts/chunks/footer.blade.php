<!-- ############ FOOTER START-->
{{--<div class="content-footer">
    <div class="d-flex p-3">
        <div class="row">
            <div class="col-12">
                <p>Социальная сеть рыболовов, охотников, грибников и всех людей предпочитающих загородный отдых. Обмениваемся опытом, размещаем отчеты с наших поездок.</p>
                <p>Знаете отличное место для пикника? Поделитесь с друзьями. Ездили на рыбалку? - расскажите другим. Набрали грибов? - выкладывайте фото! Хотите поехать, но скучно одному? - Найдите себе компанию, найдите попутчиков или организуйте встречу на водоеме.</p>
                <p>Присоединяйтесь к сообществам и делитесь информацией с другими, общайтесь и делитесь впечатлениями. Находите новые места для себя и делитесь открытиями с друзьями.</p>
            </div>
        </div>
    </div>
</div>--}}
<div class="content-footer white " id="content-footer">
    <div class="container">
        <div class="d-flex p-3">
            <span class="text-sm text-muted flex">&copy; Copyright. <a target="_blank" href="https://uneight.com/">Uneight Development</a></span>
            <div class="text-sm text-muted">Version 0.0.1</div>
        </div>
    </div>
</div>
<!-- ############ FOOTER END-->