<!-- ############ NAVBAR START-->
<div class="content-header white  box-shadow-0" id="content-header">
    <div class="container">
        <div class="navbar navbar-expand-lg">
            <div class="lt text-center">
                <!-- brand -->
                <a href="{{ route('index.get') }}" class="navbar-brand">Chebak</a>
                <!-- / brand -->
            </div>
            <!-- btn to toggle sidenav on small screen -->
            <a class="d-lg-none mx-2" data-toggle="modal" data-target="#aside">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 512 512">
                    <path d="M80 304h352v16H80zM80 248h352v16H80zM80 192h352v16H80z"/>
                </svg>
            </a>
            <!-- Page title -->
            <div class="navbar-text nav-title flex"></div>

            <ul class="nav flex-row order-lg-2">
                @if (Route::has('login'))
                    @auth
                        @if(in_array(Auth::user()->role, [
                            App\Entities\UserEntity::ROLE_ROOT,
                            App\Entities\UserEntity::ROLE_ADMIN,
                            App\Entities\UserEntity::ROLE_MODERATOR,
                        ]))
                            <li class="nav-item"><a href="{{ route('ap::index.get') }}" class="nav-link px-3"><span
                                            class="">Админпанель</span></a></li>
                        @endif
                        {{--<li class="nav-item"><a href="{{ route('logout') }}" class="nav-link px-3"><span class="">Профиль</span></a></li>--}}
                        <li class="nav-item">
                            <a href="{{ route('logout') }}" class="nav-link px-3"><span class="">Выйти</span></a>
                        </li>
                    @else
                        <li class="nav-item"><a href="{{ route('login') }}" class="nav-link px-3"><span
                                        class="">Войти</span></a></li>
                        <li class="nav-item"><a href="{{ route('register') }}" class="nav-link px-3"><span class="">Регистрация</span></a>
                        </li>
                    @endauth
                @endif
            </ul>
        </div>
    </div>
</div>
<!-- ############ NAVBAR END-->