@extends('layouts.app')

@section('page.content')

    @include('chunks.search.form')

    @include('chunks.posts')

@endsection