@extends('layouts.app')

@section('page.content')

    @include('chunks.search.form')

    <div class="box">
        <div class="box-header light lt">
            <h3>Водоемы</h3>
            <small>Водоемы где любят провести время рыбаки из сообщества "{{ $community->name }}".</small>
        </div>
        <div class="box-body">
            @foreach($waters as $water)
                @if(!empty($water->name))
                    <a href="{{ route('water-posts.get', $water->id) }}"
                       class="btn btn-sm btn-outline btn-rounded b-primary mb-1">
                        {{ $water->name }}
                        <b class="badge badge-pill primary">{{ $water->posts_count }}</b>
                    </a>
                @endif
            @endforeach
        </div>
    </div>

    @include('chunks.posts')

@endsection