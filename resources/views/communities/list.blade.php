@extends('layouts.app')

@section('page.content')

    @include('chunks.search.form')

    <div class="box">
        <div class="box-header light lt">
            <h3>Список сообществ</h3>
        </div>
        <div class="box-body">
            <div class="row">
                @foreach(array_chunk($communities->all(), 2) as $chunk)
                    <div class="col-6">
                        @foreach($chunk as $community)
                            <div><a href="{{ route('communities-posts.get', $community->id) }}"
                                    class="btn btn-link">{{ $community->name }}</a></div>
                        @endforeach
                    </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection