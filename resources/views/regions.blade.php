@extends('layouts.app')

@section('page.content')

    @include('chunks.search.form')

    <div class="box">
        <div class="box-header light lt">
            <h3>Водоемы</h3>
            <small>ТОП 15 самых популярных водоемов России, по количеству размещенных отчетов с рыбалок.</small>
        </div>
        <div class="box-body">
            @foreach($waters as $water)
                @if(!empty($water->name))
                    <a href="{{ route('water-posts.get', $water->id) }}"
                       class="btn btn-sm btn-outline btn-rounded b-primary mb-1">
                        {{ $water->name }}
                        <b class="badge badge-pill primary">{{ $water->posts_count }}</b>
                    </a>
                @endif
            @endforeach
        </div>
    </div>

    <div class="box">
        <div class="box-header light lt">
            <h3>Регионы России в алфавитном порядке: </h3>
            <small>Отчеты рыболовов отсортированные с учетом региона России</small>
        </div>
        <div class="box-body">
            @php $current = ''; @endphp
            <div class="row">
                @foreach(array_chunk($regions->all(), 2) as $chunk)
                    <div class="col-6">
                        @foreach($chunk as $region)
                            <div><a href="{{ route('region-posts.get', $region->id) }}"
                                    class="btn btn-link">{{ $region->name }}</a></div>
                        @endforeach
                    </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection