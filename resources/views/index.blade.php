@extends('layouts.app')

@section('page.content')

    @include('chunks.search.form')

    <div class="box">
        <div class="box-header light lt">
            <h3>Что нового на сайте</h3>
            <small>Статистика сети</small>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-4">
                    <div><strong>Сегодня</strong></div>
                    @foreach($categories as $category)
                        <div>
                            {{ $category->name }} {{ $category->posts_count }}
                        </div>
                    @endforeach
                </div>
                <div class="col-8">
                    <div><strong>Всего:</strong></div>
                    <div>Размещено <strong>{{ $postCount }}</strong> отчетов рыбаков.</div>
                    <div>На <strong>{{ $waterCount }}</strong> водоемах.</div>
                    <div>Зарегистрированно <strong>{{ $userCount }}</strong> активных рыбаков.</div>
                </div>
            </div>
        </div>
    </div>

    @include('chunks.posts')

@endsection