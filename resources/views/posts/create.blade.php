@extends('layouts.app')

@section('page.content')

    <div class="box">
        <div class="box-body">
            <div class="row">

                <div class="col-6">
                    <div class="form-group">
                        {{ Form::label('form[locality]', 'Населенный пункт:') }}
                        <uneight-autocomplete id="locality"
                                              name="form[locality]"
                                              :selected='{!! json_encode((!empty($entity->locality) ? ['id' => $entity->locality, 'text' => $entity->locality->name] : [])) !!}'
                                              placeholder=""
                                              route="{!! route('ap::geo:localities:autocomplete.post') !!}">
                        </uneight-autocomplete>

                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        {{ Form::label('form[place]', 'Место:') }}
                        <uneight-autocomplete id="place"
                                              name="form[place]"
                                              :selected='{!! json_encode((!empty($entity->place) ? ['id' => $entity->place, 'text' => $entity->place->name] : [])) !!}'
                                              placeholder=""
                                              route="{!! route('ap::geo:dots:autocomplete.post') !!}">
                        </uneight-autocomplete>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        {{ Form::label('form[water]', 'Водоем:') }}
                        <uneight-autocomplete id="water"
                                              name="form[water]"
                                              :selected='{!! json_encode((!empty($entity->place) ? ['id' => $entity->place, 'text' => $entity->place->name] : [])) !!}'
                                              placeholder=""
                                              route="{!! route('ap::geo:waters:autocomplete.post') !!}">
                        </uneight-autocomplete>
                    </div>
                </div>

                <div class="col-12">
                    <div class="form-group">
                        {{ Form::label('form[content]', 'Контент:') }}
                        {{ Form::textarea('form[content]', null, ['class' => 'form-control', 'rows' => 20]) }}
                    </div>
                </div>

                <div class="col-12 text-right">
                    <div class="form-group">
                        <button class="btn primary">Опубликовать</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection