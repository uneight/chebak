<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 23.04.2018
 * Time: 16:05
 */

namespace Uneight\Comments\Traits;

use Uneight\Comments\Entities\CommentEntity;

trait HasComments
{
    public function comments ()
    {
        return $this->morphToMany(CommentEntity::class, 'commentable', 'commentables',
            'commentable_id', 'comment_id');
    }
}