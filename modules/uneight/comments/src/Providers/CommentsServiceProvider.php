<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 23.04.2018
 * Time: 15:56
 */

namespace Uneight\Comments\Providers;

use Uneight\Core\Providers\ModuleServiceProvider;

class CommentsServiceProvider extends ModuleServiceProvider
{
    protected $moduleName = 'uneight-comments';
    protected $modulePath = __DIR__;
}