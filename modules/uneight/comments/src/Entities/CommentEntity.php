<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 23.04.2018
 * Time: 16:21
 */

namespace Uneight\Comments\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Uneight\Attachments\Traits\HasAttachment;
use Eloquent;
use Uneight\Posts\Entities\PostEntity;
use Uneight\Users\Entities\UserEntity;

class CommentEntity extends Eloquent
{
    use HasAttachment, SoftDeletes;

    protected $table = 'comments';

    protected $fillable = [
        'user_id', 'content', 'rate', 'active', 'created_at', 'updated_at',
    ];

    public function user()
    {
        return $this->belongsTo(UserEntity::class);
    }

    public function post ()
    {
        return $this->morphedByMany(PostEntity::class, 'commentable', 'commentables', 'comment_id');
    }
}