<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 19.04.2018
 * Time: 16:32
 */

namespace Uneight\Geo\Providers;

use Uneight\Core\Providers\ModuleServiceProvider;

class GeoServiceProvider extends ModuleServiceProvider
{
    protected $moduleName = 'uneight-geo';
    protected $modulePath = __DIR__;
}