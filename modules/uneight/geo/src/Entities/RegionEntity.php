<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 19.03.2018
 * Time: 10:03
 */

namespace Uneight\Geo\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class RegionEntity
 * @package App\Entities\Geo
 *
 * @property $country
 */
class RegionEntity extends Eloquent
{
    use SoftDeletes;

    const TYPES = [
        'Область' => 10001,
        'Край' => 10002,
        'Республика' => 10003,
        'Автономная область' => 10004,
    ];

    protected $table = 'geo_regions';

    protected $fillable = [
        'type', 'country_id', 'name', 'auto_number_codes', 'iso', 'timezone', 'source', 'active', 'api_id_htmlweb', 'api_id_kladr'
    ];

    protected $dates = ['deleted_at'];

    public function country()
    {
        return $this->belongsTo(CountryEntity::class, 'country_id', 'id');
    }

    public function waters()
    {
        return $this->belongsToMany(WaterEntity::class, 'geo_region_waters', 'region_id', 'water_id');
    }

    public function districts()
    {
        return $this->hasMany(DistrictEntity::class, 'region_id', 'id');
    }

    public function localities ()
    {
        return $this->hasMany(LocalityEntity::class, 'region_id', 'id');
    }
}