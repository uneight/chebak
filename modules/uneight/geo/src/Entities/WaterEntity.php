<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 4/9/18
 * Time: 9:02 PM
 */

namespace Uneight\Geo\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class WaterEntity extends Eloquent
{
    use SoftDeletes;

    const TYPES = [
        'river'     => 'река',
        'pond'      => 'пруд',
        'reservoir' => 'водохранилище',
        'canal'     => 'канал',
        'cove'      => 'бухта',
        'lake'      => 'озеро',
        'lock'      => 'НЕДОСТУПНО',
        'oxbow'     => 'заводь',
    ];

    protected $table = 'geo_water';

    protected $fillable = [
        'api_id', 'api_type', 'type', 'name', 'alias', 'nodes', 'active',
    ];

    public function regions ()
    {
        return $this->belongsToMany(RegionEntity::class, 'geo_region_waters', 'water_id', 'region_id');
    }

    public function districts ()
    {
        return $this->belongsToMany(DistrictEntity::class, 'geo_district_waters', 'water_id', 'district_id');
    }
}