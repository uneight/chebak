<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 19.04.2018
 * Time: 16:54
 */

namespace Uneight\Geo\Entities;

use Eloquent;
use Uneight\Posts\Entities\PostEntity;

class DotEntity extends Eloquent
{
    protected $table = 'geo_dots';

    protected $fillable = [
        'district_id', 'region_id', 'locality_id', 'water_id', 'type', 'name', 'latitude', 'longitude', 'alias', 'active',
    ];

    public function district ()
    {
        return $this->belongsTo(DistrictEntity::class, 'district_id', 'id');
    }

    public function region ()
    {
        return $this->belongsTo(RegionEntity::class, 'region_id', 'id');
    }

    public function locality ()
    {
        return $this->belongsTo(LocalityEntity::class, 'locality_id', 'id');
    }

    public function water ()
    {
        return $this->belongsTo(WaterEntity::class, 'water_id', 'id');
    }

    public function posts ()
    {
        return $this->hasMany(PostEntity::class, 'dot_id', 'id');
    }
}