<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 20.04.2018
 * Time: 11:35
 */

namespace Uneight\Geo\Http\DataTables;

use Uneight\Geo\Entities\DistrictEntity;
use Yajra\DataTables\Services\DataTable;

class DistrictsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->setRowId('id')
            ->editColumn('region_id', function ($row) {
                return $row->region->name;
            })
            ->editColumn('active', function ($row) {
                return ($row->active)
                    ? '<span class="badge badge-pill py-1 px-3 success">Да</span>'
                    : '<span class="badge badge-pill py-1 px-3 danger">Нет</span>';
            })
            ->rawColumns(['active', 'action'])
            ->addColumn('action', function($row) {
                return view('uneight-apanel::dataTables.row-tools', [
                    'items' => [
                        [
                            'type' => 'link',
                            'name' => 'Редактировать',
                            'route' => route('ap::geo:districts:edit.get', $row->id),
                            'icon' => 'fa-pencil'
                        ],
                        [
                            'type' => 'link',
                            'name' => 'Удалить',
                            'route' => '',
                            'icon' => 'fa-trash',
                            'onClickFunction' => ''
                        ],
                    ]
                ])->render();
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param DistrictEntity $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(DistrictEntity $model)
    {
        return $model->newQuery()->select('id', 'type', 'region_id', 'name', 'active')
            ->with(['region']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->parameters([
                'render' => 'custom',
                'order' => [0, 'asc'],
                'select' => [
                    'style' => 'os',
                    'selector' => 'td:first-child',
                ],
                'initComplete' => view('uneight-apanel::dataTables.init-complete'),
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => [
                'title' => 'ID',
                'class' => 'text-center',
                'placeholder' => '',
                'filter' => [
                    'type' => 'input'
                ]
            ],
            'region_id' => [
                'title' => 'Регион',
                'class' => 'text-center',
                'placeholder' => '',
                'filter' => [
                    'type' => 'input'
                ]
            ],
            'type' => [
                'title' => 'Тип',
                'class' => 'text-center',
                'placeholder' => '',
                'filter' => [
                    'type' => 'select',
                    'data' => [
                        'Район' => 'Район',
                        'Городской округ' => 'Городской округ',
                    ]
                ]
            ],
            'name' => [
                'title' => 'Название',
                'class' => 'text-center',
                'placeholder' => '',
                'filter' => [
                    'type' => 'input'
                ]
            ],
            'active' => [
                'title' => 'Активный',
                'class' => 'text-center',
                'placeholder' => '',
                'filter' => [
                    'type' => 'select',
                    'data' => [
                        'true' => 'Да',
                        'false' => 'Нет',
                    ]
                ]
            ],
            'tools' => [
                'name' => 'tools',
                'data' => 'action',
                'orderable' => false,
                'searchable' => false,
                'filter' => false,
                'title' => '',
                'class' => 'text-center',
            ]
        ];
    }
}