<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 19.04.2018
 * Time: 17:54
 */

namespace Uneight\Geo\Http\DataTables;

use Uneight\Geo\Entities\RegionEntity;
use Yajra\DataTables\Services\DataTable;

class RegionsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->setRowId('id')
            ->editColumn('country_id', function ($row) {
                return $row->country->name;
            })
            ->editColumn('active', function ($row) {
                return ($row->active)
                    ? '<span class="badge badge-pill py-1 px-3 success">Да</span>'
                    : '<span class="badge badge-pill py-1 px-3 danger">Нет</span>';
            })
            ->rawColumns(['active', 'action'])
            ->addColumn('action', function($row) {
                return view('uneight-apanel::dataTables.row-tools', [
                    'items' => [
                        [
                            'type' => 'link',
                            'name' => 'Редактировать',
                            'route' => route('ap::geo:regions:edit.get', $row->id),
                            'icon' => 'fa-pencil'
                        ],
                        [
                            'type'  => 'link',
                            'name'  => 'Просмотр',
                            'route' => route('ap::geo:regions:view.get', $row->id),
                            'icon'  => 'fa-eye',
                        ],
                        [
                            'type' => 'remove',
                            'name' => 'Удалить',
                            'route' => route('ap::geo:regions:remove.delete', $row->id),
                            'icon' => 'fa-trash',
                            'id' => 'region_' . $row->id
                        ],
                    ]
                ])->render();
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param RegionEntity $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(RegionEntity $model)
    {
        return $model->newQuery()->select('id', 'type', 'country_id', 'name', 'iso', 'timezone', 'active')
            ->with(['country']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->parameters([
                'render' => 'custom',
                'order' => [0, 'asc'],
                'select' => [
                    'style' => 'os',
                    'selector' => 'td:first-child',
                ],
                'initComplete' => view('uneight-apanel::dataTables.init-complete'),
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => [
                'title' => 'ID',
                'class' => 'text-center',
                'placeholder' => '',
                'filter' => [
                    'type' => 'input'
                ]
            ],
            'country_id' => [
                'title' => 'Страна',
                'class' => 'text-center',
                'placeholder' => '',
                'filter' => [
                    'type' => 'input'
                ]
            ],
            'type'           => [
                'title' => 'Тип',
                'class' => 'text-center',
                'placeholder' => '',
                'filter' => [
                    'type' => 'select',
                    'data' => [
                        'Область' => 'Область',
                        'Край' => 'Край',
                        'Республика' => 'Республика',
                        'Автономный округ' => 'Автономный округ',
                        'Автономная область' => 'Автономная область',
                    ]
                ]
            ],
            'name'           => [
                'title' => 'Название',
                'class' => 'text-center',
                'placeholder' => '',
                'filter' => [
                    'type' => 'input'
                ]
            ],
            'iso'              => [
                'title' => 'Код',
                'class' => 'text-center',
                'placeholder' => '',
                'filter' => [
                    'type' => 'input'
                ]
            ],
            'active'           => [
                'title' => 'Активный',
                'class' => 'text-center',
                'placeholder' => '',
                'filter' => [
                    'type' => 'select',
                    'data' => [
                        'true' => 'Да',
                        'false' => 'Нет',
                    ]
                ]
            ],
            'tools'            => [
                'name' => 'tools',
                'data' => 'action',
                'orderable' => false,
                'searchable' => false,
                'filter' => false,
                'title' => '',
                'class' => 'text-center',
            ]
        ];
    }
}