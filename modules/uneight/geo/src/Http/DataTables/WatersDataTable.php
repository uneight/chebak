<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 7/9/18
 * Time: 6:18 PM
 */

namespace Uneight\Geo\Http\DataTables;

use Uneight\Geo\Entities\WaterEntity;
use Yajra\DataTables\Services\DataTable;

class WatersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     *
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable ($query)
    {
        return datatables($query)
            ->setRowId('id')
            ->editColumn('type', function ($row) {
                return (isset(WaterEntity::TYPES[$row->type])) ? WaterEntity::TYPES[$row->type] : '';
            })
            ->editColumn('regions_list', function ($row) {
                return $row->regions->implode('name', ', ');
            })
            ->editColumn('districts_list', function ($row) {
                return $row->districts->implode('name', ', ');
            })
            ->editColumn('active', function ($row) {
                return ($row->active)
                    ? '<span class="badge badge-pill py-1 px-3 success">Да</span>'
                    : '<span class="badge badge-pill py-1 px-3 danger">Нет</span>';
            })
            ->rawColumns(['active', 'action'])
            ->addColumn('action', function ($row) {
                return view('uneight-apanel::dataTables.row-tools', [
                    'items' => [
                        [
                            'type'  => 'link',
                            'name'  => 'Редактировать',
                            'route' => route('ap::geo:waters:edit.get', $row->id),
                            'icon'  => 'fa-pencil',
                        ],
                        [
                            'type'  => 'remove',
                            'name'  => 'Удалить',
                            'route' => route('ap::geo:waters:remove.delete', $row->id),
                            'icon'  => 'fa-trash',
                            'id'    => 'region_' . $row->id,
                        ],
                    ],
                ])->render();
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param RegionEntity $model
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query (WaterEntity $model)
    {
        return $model->newQuery()->select('id', 'type', 'alias', 'name', 'active');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html ()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->parameters([
                'render'       => 'custom',
                'order'        => [0, 'asc'],
                'select'       => [
                    'style'    => 'os',
                    'selector' => 'td:first-child',
                ],
                'initComplete' => view('uneight-apanel::dataTables.init-complete'),
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns ()
    {
        return [
            'id'             => [
                'title'       => 'ID',
                'class'       => 'text-center',
                'placeholder' => '',
                'filter'      => [
                    'type' => 'input',
                ],
            ],
            'type'           => [
                'title'       => 'Тип',
                'class'       => 'text-center',
                'placeholder' => '',
                'filter'      => [
                    'type' => 'select',
                    'data' => WaterEntity::TYPES,
                ],
            ],
            'name'           => [
                'title'       => 'Название',
                'class'       => 'text-center',
                'placeholder' => '',
                'filter'      => [
                    'type' => 'input',
                ],
            ],
            'regions_list'   => [
                'title'       => 'Список регионов',
                'class'       => 'text-center',
                'placeholder' => '',
                'filter'      => [
                    'type' => 'input',
                ],
            ],
            'districts_list' => [
                'title'       => 'Список районов',
                'class'       => 'text-center',
                'placeholder' => '',
                'filter'      => [
                    'type' => 'input',
                ],
            ],
            'active'         => [
                'title'       => 'Активный',
                'class'       => 'text-center',
                'placeholder' => '',
                'filter'      => [
                    'type' => 'select',
                    'data' => [
                        'true'  => 'Да',
                        'false' => 'Нет',
                    ],
                ],
            ],
            'tools'          => [
                'name'       => 'tools',
                'data'       => 'action',
                'orderable'  => false,
                'searchable' => false,
                'filter'     => false,
                'title'      => '',
                'class'      => 'text-center',
            ],
        ];
    }
}