<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 19.04.2018
 * Time: 16:23
 */

namespace Uneight\Geo\Http\DataTables;

use Uneight\Geo\Entities\DotEntity;
use Yajra\DataTables\Services\DataTable;

class DotsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->setRowId('id')
            ->editColumn('region_id', function ($row) {
                return (!empty($row->region->name)) ? $row->region->name : '';
            })
            ->editColumn('district_id', function ($row) {
                return (!empty($row->district->name)) ? $row->district->name : '';
            })
            ->editColumn('posts_count', function ($row) {
                return count($row->posts);
            })
            ->addColumn('action', function($row) {
                return view('uneight-apanel::dataTables.row-tools', [
                    'items' => [
                        [
                            'type' => 'link',
                            'name' => 'Просмотр',
                            'route' => '',
                            'icon' => 'fa-eye'
                        ],
                        [
                            'type' => 'link',
                            'name' => 'Редактировать',
                            'route' => route('ap::geo:dots:edit.get', $row->id),
                            'icon' => 'fa-pencil'
                        ],
                        [
                            'type' => 'link',
                            'name' => 'Удалить',
                            'route' => '',
                            'icon' => 'fa-trash',
                            'onClickFunction' => ''
                        ],
                    ]
                ])->render();
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param DotEntity $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(DotEntity $model)
    {
        return $model->newQuery()->select('id', 'name', 'alias', 'region_id', 'district_id', 'locality_id', 'water_id', 'active');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->parameters([
                'render' => 'custom',
                'order' => [0, 'asc'],
                'select' => [
                    'style' => 'os',
                    'selector' => 'td:first-child',
                ],
                'initComplete' => view('uneight-apanel::dataTables.init-complete'),
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => [
                'title' => 'ID',
                'class' => 'text-center',
                'placeholder' => '',
                'filter' => [
                    'type' => 'input'
                ]
            ],
            'name'        => [
                'title' => 'Название',
                'class' => 'text-center',
                'placeholder' => '',
                'filter' => [
                    'type' => 'input'
                ]
            ],
            'alias'       => [
                'title' => 'Псевдонимы',
                'class' => 'text-center',
                'placeholder' => '',
                'filter' => [
                    'type' => 'input'
                ]
            ],
            'region_id'   => [
                'title' => 'Регион',
                'class' => 'text-center',
            ],
            'district_id' => [
                'title' => 'Район',
                'class' => 'text-center',
            ],
            'posts_count' => [
                'title' => 'Кол-во постов',
                'class' => 'text-center',
            ],
            'active'      => [
                'title' => 'Активный',
                'class' => 'text-center',
                'placeholder' => '',
                'filter' => [
                    'type' => 'select',
                    'data' => [
                        true => 'Да',
                        false => 'Нет',
                    ]
                ]
            ],
            'tools'       => [
                'name' => 'tools',
                'data' => 'action',
                'orderable' => false,
                'searchable' => false,
                'filter' => false,
                'title' => '',
                'class' => 'text-center',
            ]
        ];
    }
}