<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 19.04.2018
 * Time: 16:22
 */

namespace Uneight\Geo\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Uneight\Core\Http\Controllers\CRUDController;
use Uneight\Geo\Entities\DistrictEntity;
use Uneight\Geo\Entities\RegionEntity;
use Uneight\Geo\Entities\WaterEntity;
use Uneight\Geo\Http\DataTables\DotsDataTable;
use Uneight\Geo\Entities\DotEntity;
use Uneight\Geo\Http\Requests\DotSaveRequest;
use Uneight\Settings\Entities\SettingEntity;

class DotsController extends CRUDController
{
    const TYPE_LAKES = 'lakes';
    const TYPE_RIVERS = 'rivers';
    const TYPES = [
        'lakes'  => '"natural"="water"',
        'rivers' => '"waterway"="river"',
    ];

    public function __construct ()
    {
        $this->_viewPath = 'uneight-geo::dots.';
        $this->entity = new DotEntity();
        $this->relations = ['region', 'district', 'water'];

        parent::__construct();
    }

    public function indexAction (DotsDataTable $dataTable)
    {
        $this->header->setTitle('Места')
            ->setDescription('Список мест')
            ->render();

        return $this->getIndex($dataTable);
    }

    public function editAction (Request $request)
    {
        $this->header->setTitle('Места')
            ->setDescription('Редактировать мето')
            ->setButton('Назад', route('ap::geo:dots:index.get'), 'success')
            ->render();

        return $this->getEdit($request);
    }

    public function storeAction (DotSaveRequest $request)
    {
        $return = $this->postEdit($request);
        $settings = SettingEntity::where('key', SettingEntity::TYPE_WATER_STOP_WORDS)->first();
        $list = trim($settings->value, ',');
        $value = implode("','", array_map('trim', explode(',', $list)));

        $posts = \DB::table('posts')
            ->whereRaw("plainto_tsquery('russian', content) @@ to_tsvector('russian', '" .
                $this->entity->name . "') AND ('" . $this->entity->name . "' NOT IN ('" . $value . "') OR ('" .
                $this->entity->name . "' IN ('" . $value . "') AND (array_upper(string_to_array('" .
                $this->entity->name . "', ' '), 1) > 1)))")
            ->orWhereRaw("plainto_tsquery('russian', content) @@ to_tsvector('russian', '" . $this->entity->alias . "')")
            ->update([
                'dot_id'      => $this->entity->id,
                'water_id'    => $this->entity->water_id,
                'region_id'   => $this->entity->region_id,
                'district_id' => $this->entity->district_id,
            ]);

        return $return->getTargetUrl();
    }

    public function removeAction (Request $request, $id)
    {
        return $this->deleteRemove($request, $id);
    }

    public function autocompleteAction (Request $request)
    {
        return $this->entity->where('name', 'ILIKE', '%' . $request->input('query') . '%')->get()->map(function ($item) {
            return [
                'id'   => $item->id,
                'text' => $item->name,
            ];
        });
    }

    public function loadNearestWaters (Request $request)
    {
        $result = [
            'waters'   => [],
            'region'   => null,
            'district' => null,
        ];
        $client = new Client();
        $coords = $request->all();
        $around = 'around:5000,' . $coords[0] . ',' . $coords[1];

        $type = self::TYPES['lakes'];
        $data = $client->request('GET', 'https://overpass-api.de/api/interpreter', [
            'query' => [
                'data' => '[out:json][timeout:25];(node(' . $around . ')[' . $type . '];way(' . $around . ')[' . $type . '];relation(' . $around . ')[' . $type . '];);out;>;out skel qt;',
            ],
        ])->getBody()->getContents();

        $lakes = json_decode($data)->elements;

        $type = self::TYPES['rivers'];
        $data = $client->request('GET', 'https://overpass-api.de/api/interpreter', [
            'query' => [
                'data' => '[out:json][timeout:25];(node(' . $around . ')[' . $type . '];way(' . $around . ')[' . $type . '];relation(' . $around . ')[' . $type . '];);out;>;out skel qt;',
            ],
        ])->getBody()->getContents();

        $ids = [];
        $rivers = json_decode($data)->elements;
        $_tmp = array_merge($lakes, $rivers);

        foreach ($_tmp as $item) {
            if ($item->type == 'way' || $item->type == 'relation') {
                $ids[] = $item->id;
            }

        }

        $result['waters'] = WaterEntity::whereIn('api_id', $ids)->get();

        $data = $client->request('GET', 'https://overpass-api.de/api/interpreter', [
            'query' => [
                'data' => '[out:json][timeout:25];is_in(' . $coords[0] . ',' . $coords[1] . ')->.a;way(pivot.a);relation(pivot.a);out tags bb;',
            ],
        ])->getBody()->getContents();

        $_tmp = json_decode($data)->elements;

        foreach ($_tmp as $item) {
            if (!empty($item->tags->admin_level)) {
                switch ($item->tags->admin_level) {
                    case 4:
                        $result['region'] = RegionEntity::where('name', 'ILIKE', '%' . $item->tags->name . '%')->first();
                        break;
                    case 6:
                        $entity = DistrictEntity::where('name', 'ILIKE', '%' . $item->tags->name . '%');

                        if (!empty($item->tags->wikidata)) {
                            $entity->orWhere('wikidata', $item->tags->wikidata);
                        }

                        $result['district'] = $entity->first();

                        break;
                }
            }
        }

        return $result;
    }
}