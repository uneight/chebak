<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 19.04.2018
 * Time: 16:24
 */

namespace Uneight\Geo\Http\Controllers;

use Illuminate\Http\Request;
use Uneight\Core\Http\Controllers\CRUDController;
use Uneight\Geo\Http\DataTables\LocalitiesDataTable;
use Uneight\Geo\Entities\LocalityEntity;
use Uneight\Geo\Http\Requests\LocalitySaveRequest;

class LocalitiesController extends CRUDController
{
    public function __construct()
    {
        $this->_viewPath = 'uneight-geo::localities.';
        $this->entity = new LocalityEntity();

        parent::__construct();
    }

    public function indexAction(LocalitiesDataTable $dataTable)
    {
        $header = $this->header->setTitle('Населенные пункты')
            ->setDescription('Список населенных пунктов')
            ->setButton('Добавить населенный пункт', route('ap::geo:localities:edit.get'))
            ->render();

        return $this->getIndex($dataTable);
    }

    public function editAction(Request $request)
    {
        $this->header->setTitle('Населенные пункты')
            ->setDescription('Редактировать населенный пункт')
            ->setButton('Назад', route('ap::geo:localities:index.get'), 'success');

        return $this->getEdit($request);
    }

    public function storeAction(LocalitySaveRequest $request)
    {
        return $this->postEdit($request);
    }

    public function removeAction(Request $request, $id)
    {
        return $this->deleteRemove($request, $id);
    }

    public function autocompleteAction(Request $request)
    {
        return $this->entity->with($this->relations)->where('name', 'LIKE', '%' . $request->input('query') . '%')->get()->map(function ($item) {
            return [
                'id' => $item->id,
                'text' => $item->name . ' (' . $item->district->name . ' '. $item->region->name . ')',
            ];
        });
    }
}