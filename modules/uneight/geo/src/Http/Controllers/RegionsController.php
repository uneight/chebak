<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 19.04.2018
 * Time: 16:24
 */

namespace Uneight\Geo\Http\Controllers;

use App\Jobs\ProceedRegionDistrictsJob;
use Illuminate\Http\Request;
use Uneight\Core\Http\Controllers\CRUDController;
use Uneight\Geo\Http\DataTables\RegionsDataTable;
use App\Entities\Geo\RegionEntity;
use Uneight\Geo\Http\Requests\RegionSaveRequest;

class RegionsController extends CRUDController
{
    public function __construct()
    {
        $this->_viewPath = 'uneight-geo::regions.';
        $this->entity = new RegionEntity();
        $this->relations = ['country'];

        parent::__construct();
    }

    public function indexAction(RegionsDataTable $dataTable)
    {
        $this->header->setTitle('Регионы')
            ->setDescription('Список регионов')
            ->setButton('Добавить регион', route('ap::geo:regions:edit.get'))
            ->render();

        return $this->getIndex($dataTable);
    }

    public function editAction(Request $request)
    {
        $this->header->setTitle('Регионы')
            ->setDescription('Редактировать регион')
            ->setButton('Назад', route('ap::geo:regions:index.get'), 'success');

        return $this->getEdit($request);
    }

    public function storeAction(RegionSaveRequest $request)
    {
        return $this->postEdit($request);
    }

    public function removeAction(Request $request, $id)
    {
        return $this->deleteRemove($request, $id);
    }

    public function autocompleteAction(Request $request)
    {
        return $this->entity->with($this->relations)->where('name', 'LIKE', '%' . $request->input('query') . '%')->get()->map(function ($item) {
            return [
                'id' => $item->id,
                'text' => $item->name . ' (' . $item->country->name . ')',
            ];
        });
    }

    public function viewAction (Request $request, $id)
    {
        $this->relations = array_merge($this->relations, ['localities', 'districts']);

        return $this->getView($request);
    }

    public function getDistrictsAction (Request $request, $id)
    {
        $region = $this->entity->find($id);

        if (empty($region)) {
            return redirect()->route('ap::index.get');
        }

        $region = RegionEntity::find($id);

        if (!empty($region)) {
            ProceedRegionDistrictsJob::dispatch($region);

            return redirect()->back()->with('notify', [
                'text'   => 'Парсинг районов успешно запущен.',
                'header' => 'Успешно',
                'type'   => 'success',
            ]);
        } else {
            return redirect()->back()->with('notify', [
                'text'   => 'Произошла непредвиденная ошибка, проверьте логи.',
                'header' => 'Ошибка',
                'type'   => 'danger',
            ]);
        }
    }

    public function getLocalitiesAction (Request $request, $id)
    {
        $region = $this->entity->find($id);

        if (empty($region)) {
            return redirect()->route('ap::index.get');
        }

        $region = RegionEntity::find($id);

        if (!empty($region)) {
            ProceedRegionDistrictsJob::dispatch($region);

            return redirect()->back()->with('notify', [
                'text'   => 'Парсинг населенных пунктов успешно запущен.',
                'header' => 'Успешно',
                'type'   => 'success',
            ]);
        } else {
            return redirect()->back()->with('notify', [
                'text'   => 'Произошла непредвиденная ошибка, проверьте логи.',
                'header' => 'Ошибка',
                'type'   => 'danger',
            ]);
        }


    }
}