<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 19.04.2018
 * Time: 16:45
 */

namespace Uneight\Geo\Http\Controllers;

use Illuminate\Http\Request;
use Uneight\Core\Http\Controllers\CRUDController;
use Uneight\Geo\Http\DataTables\WatersDataTable;
use Uneight\Geo\Http\Requests\WaterSaveRequest;
use Uneight\Users\Http\DataTables\UsersDataTable;
use Uneight\Geo\Entities\WaterEntity;

class WatersController extends CRUDController
{
    public function __construct()
    {
        $this->_viewPath = 'uneight-geo::waters.';
        $this->entity = new WaterEntity();
        $this->relations = ['regions', 'districts'];

        parent::__construct();
    }

    public function indexAction (WatersDataTable $dataTable)
    {
        $this->header->setTitle('Водоемы')
            ->setDescription('Список водоемов')
            ->setButton('Добавить водоем', route('ap::geo:waters:edit.get'))
            ->render();

        return $this->getIndex($dataTable);
    }

    public function editAction(Request $request)
    {
        $this->header->setTitle('Водоемы')
            ->setDescription('Редактировать водоем')
            ->setButton('Назад', route('ap::geo:waters:index.get'), 'success');

        return $this->getEdit($request);
    }

    public function storeAction(WaterSaveRequest $request)
    {
        return $this->postEdit($request);
    }

    public function removeAction(Request $request, $id)
    {
        return $this->deleteRemove($request, $id);
    }

    public function viewAction(Request $request, $id)
    {
        return $this->getView($request);
    }

    public function autocompleteAction(Request $request)
    {
        return $this->entity->with($this->relations)->where('name', 'LIKE', '%' . $request->input('query') . '%')
            ->get()
            ->map(function ($item) {
                return [
                    'id'   => $item->id,
                    'text' => sprintf('%s', $item->name),
                ];
            });
    }

    public function typesAutocompleteAction (Request $request)
    {
        $types = WaterEntity::TYPES;
        $result = [];

        array_walk($types, function ($value, $key) use ($request, &$result) {
            if (str_contains($value, $request->input('query'))) {
                $result[] = [
                    'id'   => $key,
                    'text' => $value,
                ];
            }
        });

        return $result;
    }
}