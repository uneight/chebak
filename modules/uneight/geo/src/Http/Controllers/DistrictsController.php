<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 19.04.2018
 * Time: 16:24
 */

namespace Uneight\Geo\Http\Controllers;

use Illuminate\Http\Request;
use Uneight\Core\Http\Controllers\CRUDController;
use Uneight\Geo\Http\DataTables\DistrictsDataTable;
use Uneight\Geo\Entities\DistrictEntity;
use Uneight\Geo\Http\Requests\DistrictSaveRequest;

class DistrictsController extends CRUDController
{
    public function __construct()
    {
        $this->_viewPath = 'uneight-geo::districts.';
        $this->entity = new DistrictEntity();

        parent::__construct();
    }

    public function indexAction(DistrictsDataTable $dataTable)
    {
        $header = $this->header->setTitle('Районы')
            ->setDescription('Список районов')
            ->setButton('Добавить район', route('ap::geo:districts:edit.get'))
            ->render();

        return $this->getIndex($dataTable);
    }

    public function editAction(Request $request)
    {
        $this->header->setTitle('Районы')
            ->setDescription('Редактировать район')
            ->setButton('Назад', route('ap::geo:districts:index.get'), 'success');

        return $this->getEdit($request);
    }

    public function storeAction(DistrictSaveRequest $request)
    {
        return $this->postEdit($request);
    }

    public function removeAction(Request $request, $id)
    {
        return $this->deleteRemove($request, $id);
    }

    public function autocompleteAction(Request $request)
    {
        return $this->entity->with($this->relations)->where('name', 'LIKE', '%' . $request->input('query') . '%')->get()->map(function ($item) {
            return [
                'id' => $item->id,
                'text' => $item->name . ' (' . $item->region->name . ')',
            ];
        });
    }
}