<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 19.04.2018
 * Time: 16:23
 */

namespace Uneight\Geo\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Uneight\Apanel\View\Header;
use Uneight\Core\Contracts\SaveRequest;
use Uneight\Core\Http\Controllers\CRUDController;
use Uneight\Geo\Http\DataTables\CountriesDataTable;
use Uneight\Geo\Entities\CountryEntity;
use Uneight\Geo\Http\Requests\CountrySaveRequest;

class CountriesController extends CRUDController
{
    public function __construct()
    {
        $this->_viewPath = 'uneight-geo::countries.';
        $this->entity = new CountryEntity();

        parent::__construct();
    }

    public function indexAction(CountriesDataTable $dataTable)
    {
        $this->header->setTitle('Страны')
            ->setDescription('Список стран')
            ->setButton('Добавить страну', route('ap::geo:countries:edit.get'))
            ->render();

        return $this->getIndex($dataTable);
    }

    public function editAction(Request $request, $id = null)
    {
        $this->header->setTitle('Места')
            ->setDescription('Редактировать мето')
            ->setButton('Назад', route('ap::geo:dots:index.get'), 'success');

        return $this->getEdit($request);
    }

    public function storeAction(CountrySaveRequest $request)
    {
        return $this->postEdit($request);
    }

    public function deleteAction(Request $request)
    {

    }

    public function autocompleteAction(Request $request)
    {
        return $this->entity->where('name', 'LIKE', '%' . $request->input('query') . '%')->get()->map(function ($item) {
            return [
                'id' => $item->id,
                'text' => $item->name
            ];
        });
    }
}