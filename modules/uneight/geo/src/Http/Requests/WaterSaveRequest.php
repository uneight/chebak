<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 23.04.2018
 * Time: 13:28
 */

namespace Uneight\Geo\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Uneight\Core\Contracts\SaveRequest;

class WaterSaveRequest extends FormRequest implements SaveRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules ()
    {
        return [

        ];
    }

    public function messages ()
    {
        return [

        ];
    }
}