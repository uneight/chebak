<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 4/22/18
 * Time: 8:03 PM
 */

namespace Uneight\Geo\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Uneight\Core\Contracts\SaveRequest;

class CountrySaveRequest extends FormRequest implements SaveRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules ()
    {
        return [

        ];
    }

    public function messages ()
    {
        return [

        ];
    }
}