<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 23.04.2018
 * Time: 13:28
 */

namespace Uneight\Geo\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Uneight\Core\Contracts\SaveRequest;

class DotSaveRequest extends FormRequest implements SaveRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize ()
    {
        return true;
    }

    public function rules ()
    {
        return [
            'form.entity.name'        => 'required|string',
            'form.entity.alias'       => 'string|nullable',
            'form.entity.region.id'   => 'sometimes|integer',
            'form.entity.district.id' => 'sometimes|integer',
            'form.entity.water_id'    => 'required|integer',
            'form.entity.latitude'    => 'required|numeric',
            'form.entity.longitude'   => 'required|numeric',
        ];
    }

    public function messages ()
    {
        return parent::messages();
    }
}