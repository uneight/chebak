<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 19.04.2018
 * Time: 16:25
 */

Route::group(['prefix' => 'ap', 'as' => 'ap::', 'namespace' => 'Uneight\\Geo\\Http\\Controllers', 'middleware' => ['web']], function() {

    Route::group(['prefix' => 'geo', 'as' => 'geo:'], function () {

       Route::group(['prefix' => 'countries', 'as' => 'countries:'], function () {
           Route::get('/', ['as' => 'index.get', 'uses' => 'CountriesController@indexAction']);
           Route::get('/edit/{id?}', ['as' => 'edit.get', 'uses' => 'CountriesController@editAction']);
           Route::post('/edit/{id?}', ['as' => 'edit.post', 'uses' => 'CountriesController@storeAction']);
           Route::delete('/delete/{id}', ['as' => 'remove.delete', 'uses' => 'CountriesController@removeAction']);
           Route::post('/autocomplete', ['as' => 'autocomplete.post', 'uses' => 'CountriesController@autocompleteAction']);
       });

       Route::group(['prefix' => 'regions', 'as' => 'regions:'], function () {
           Route::get('/', ['as' => 'index.get', 'uses' => 'RegionsController@indexAction']);
           Route::get('/edit/{id?}', ['as' => 'edit.get', 'uses' => 'RegionsController@editAction']);
           Route::post('/edit/{id?}', ['as' => 'edit.post', 'uses' => 'RegionsController@storeAction']);
           Route::delete('/delete/{id}', ['as' => 'remove.delete', 'uses' => 'RegionsController@removeAction']);
           Route::post('/autocomplete', ['as' => 'autocomplete.post', 'uses' => 'RegionsController@autocompleteAction']);

           Route::get('/view/{id?}', ['as' => 'view.get', 'uses' => 'RegionsController@viewAction']);

           Route::get('/get-districts/{id}', ['as' => 'get_districts.get', 'uses' => 'RegionsController@getDistrictsAction']);
           Route::get('/get-localities/{id}', ['as' => 'get_localities.get', 'uses' => 'RegionsController@getLocalitiesAction']);
       });

       Route::group(['prefix' => 'districts', 'as' => 'districts:'], function () {
           Route::get('/', ['as' => 'index.get', 'uses' => 'DistrictsController@indexAction']);
           Route::get('/edit/{id?}', ['as' => 'edit.get', 'uses' => 'DistrictsController@editAction']);
           Route::post('/edit/{id?}', ['as' => 'edit.post', 'uses' => 'DistrictsController@storeAction']);
           Route::delete('/delete/{id}', ['as' => 'remove.delete', 'uses' => 'DistrictsController@removeAction']);
           Route::post('/autocomplete', ['as' => 'autocomplete.post', 'uses' => 'DistrictsController@autocompleteAction']);
       });

       Route::group(['prefix' => 'localities', 'as' => 'localities:'], function () {
           Route::get('/', ['as' => 'index.get', 'uses' => 'LocalitiesController@indexAction']);
           Route::get('/edit/{id?}', ['as' => 'edit.get', 'uses' => 'LocalitiesController@editAction']);
           Route::post('/edit/{id?}', ['as' => 'edit.post', 'uses' => 'LocalitiesController@storeAction']);
           Route::delete('/delete/{id}', ['as' => 'remove.delete', 'uses' => 'LocalitiesController@removeAction']);
           Route::post('/autocomplete', ['as' => 'autocomplete.post', 'uses' => 'LocalitiesController@autocompleteAction']);
       });

        Route::group(['prefix' => 'waters', 'as' => 'waters:'], function () {
            Route::get('/', ['as' => 'index.get', 'uses' => 'WatersController@indexAction']);
            Route::get('/edit/{id?}', ['as' => 'edit.get', 'uses' => 'WatersController@editAction']);
            Route::post('/edit/{id?}', ['as' => 'edit.post', 'uses' => 'WatersController@storeAction']);
            Route::delete('/delete/{id}', ['as' => 'remove.delete', 'uses' => 'WatersController@removeAction']);
            Route::post('/autocomplete', ['as' => 'autocomplete.post', 'uses' => 'WatersController@autocompleteAction']);
            Route::post('/types/autocomplete', ['as' => 'types:autocomplete.post', 'uses' => 'WatersController@typesAutocompleteAction']);
            Route::get('/view/{id}', ['as' => 'view.get', 'uses' => 'WatersController@viewAction']);
       });

       Route::group(['prefix' => 'dots', 'as' => 'dots:'], function () {
           Route::get('/', ['as' => 'index.get', 'uses' => 'DotsController@indexAction']);
           Route::get('/edit/{id?}', ['as' => 'edit.get', 'uses' => 'DotsController@editAction']);
           Route::post('/edit/{id?}', ['as' => 'edit.post', 'uses' => 'DotsController@storeAction']);
           Route::delete('/delete/{id}', ['as' => 'remove.delete', 'uses' => 'DotsController@removeAction']);
           Route::post('/autocomplete', ['as' => 'autocomplete.post', 'uses' => 'DotsController@autocompleteAction']);
           Route::post('/load-nearest-waters', ['as' => 'loadNearestWaters.post', 'uses' => 'DotsController@loadNearestWaters']);
       });

    });

});