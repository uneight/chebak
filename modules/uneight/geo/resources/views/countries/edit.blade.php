@extends('uneight-apanel::layouts.master-layout')

@section('page.content')

    {{ Form::open(['route' => Request::route()->getName(), 'action' => 'POST']) }}
    {{ Form::hidden('form[row][id]', (!empty($entity->id) ? $entity->id : null)) }}

    <div class="box">
        <div class="box-header light lt">
            <h4>Основные</h4>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="form-group col-6">
                    {{ Form::label('form[entity][name]', 'Название:') }}
                    {{ Form::text('form[entity][name]', (!empty($entity->name) ? $entity->name : null), ['class' => 'form-control']) }}
                </div>
                <div class="form-group col-6">
                    {{ Form::label('form[[entity]full_name]', 'Полное название:') }}
                    {{ Form::text('form[entity][full_name]', (!empty($entity->full_name) ? $entity->full_name : null), ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="row">
                <div class="form-group col-6">
                    {{ Form::label('form[entity][iso_code]', 'ISO code:') }}
                    {{ Form::text('form[entity][iso_code]', (!empty($entity->iso_code) ? $entity->iso_code : null), ['class' => 'form-control']) }}
                </div>
                <div class="form-group col-6">
                    {{ Form::label('form[entity][iso_name]', 'ISO name:') }}
                    {{ Form::text('form[entity][iso_name]', (!empty($entity->iso_name) ? $entity->iso_name : null), ['class' => 'form-control']) }}
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 text-right">
            {!! Form::button('Сохранить', ['class' => 'btn btn-fw submit white', 'type' => 'submit']) !!}
        </div>
    </div>

    {{ Form::close() }}

@endsection
