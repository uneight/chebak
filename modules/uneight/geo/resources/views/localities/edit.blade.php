@extends('uneight-apanel::layouts.master-layout')

@section('page.content')

    {{ Form::open() }}
    {{ Form::hidden('form[id]', (!empty($entity->id) ? $entity->id : null)) }}

    <div class="box">
        <div class="box-header light lt">
            <h4>Основные</h4>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="form-group col-6">
                    {{ Form::label('form[name]', 'Название:') }}
                    {{ Form::text('form[name]', (!empty($entity->name) ? $entity->name : null), ['class' => 'form-control']) }}
                </div>
                <div class="form-group col-6">
                    {{ Form::label('form[full_name]', 'Полное название:') }}
                    {{ Form::text('form[full_name]', (!empty($entity->full_name) ? $entity->full_name : null), ['class' => 'form-control']) }}
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 text-right">
            <div class="btn-group">
                <button class="btn white">
                    <a href="">Сохранить</a>
                </button>
                <button class="btn white dropdown-toggle" data-toggle="dropdown"></button>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="#">Сохранить и выйти</a>
                    <a class="dropdown-item">Назад</a>
                </div>
            </div>
        </div>
    </div>

    {{ Form::close() }}

@endsection
