@extends('uneight-apanel::layouts.master-layout')

@section('page.content')

    {{ Form::open() }}
    {{ Form::hidden('form[id]', (!empty($entity->id) ? $entity->id : null)) }}

    <div class="box">
        <div class="box-header light lt">
            <h4>Основные</h4>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="form-group col-6">
                    {{ Form::label('form[entity][name]', 'Название:') }}
                    {{ Form::text('form[entity][name]', (!empty($entity->name) ? $entity->name : null), ['class' => 'form-control']) }}
                </div>
                <div class="form-group col-6">
                    {{ Form::label('form[entity][type]', 'Страна:') }}
                    <uneight-autocomplete id="country"
                                          name="form[country_id]"
                                          :selected='{!! json_encode((!empty($entity->type) ? ['id' => $entity->type,
                                            'text' => \Uneight\Geo\Entities\WaterEntity::TYPES[$entity->type]] : [])) !!}'
                                          placeholder=""
                                          route="{!! route('ap::geo:waters:types:autocomplete.post') !!}">
                    </uneight-autocomplete>
                </div>
            </div>
        </div>
    </div>

    <div class="box">
        <div class="box-header light lt">
            <h4>К чему относится:</h4>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-12">
                    <div><strong>Регионы:</strong> {{ $entity->regions->implode('name', ', ') }}</div>
                    <div><strong>Районы:</strong> {{ $entity->districts->implode('name', ', ') }}</div>
                </div>
            </div>
        </div>
    </div>

    <div class="box">
        <div class="box-header light lt">
            <h4>Список координат:</h4>
        </div>
        <div class="box-body">
            <div class="row">
                @foreach(json_decode($entity->nodes) as $node)
                    <div class="col-3">
                        <strong>Lat:</strong> {{ $node->lat }}, <strong>Lon:</strong> {{ $node->lon }}
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 text-right">
            <div class="btn-group">
                <button class="btn white">
                    <a href="">Сохранить</a>
                </button>
                <button class="btn white dropdown-toggle" data-toggle="dropdown"></button>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="#">Сохранить и выйти</a>
                    <a class="dropdown-item">Назад</a>
                </div>
            </div>
        </div>
    </div>

    {{ Form::close() }}

@endsection
