@extends('uneight-apanel::layouts.master-layout')

@section('page.header')

    @parent

    @if(in_array(Auth::user()->role, [Uneight\Users\Entities\UserEntity::ROLE_ROOT, Uneight\Users\Entities\UserEntity::ROLE_ADMIN, Uneight\Users\Entities\UserEntity::ROLE_MODERATOR]))
        <dot-add class="col-6 justify-content-end text-right"></dot-add>
    @endif

@append

@section('page.content')

    <div class="box">
        <div class="box-body">
            {!! $dataTable->table(['class' => 'table table-striped table-bordered'], true) !!}
        </div>
    </div>

@endsection

@push('styles')
    <link rel="stylesheet" href="https://nightly.datatables.net/buttons/css/buttons.bootstrap4.min.css">
@endpush

@push('scripts')
    <script src="/dist/uneight/apanel/js/libs/jquery.dataTables.min.js"></script>
    <script src="/dist/uneight/apanel/js/libs/dataTables.bootstrap4.js"></script>
    {!! $dataTable->scripts() !!}
    <script src="https://nightly.datatables.net/buttons/js/dataTables.buttons.min.js"></script>
    <script src="https://nightly.datatables.net/buttons/js/buttons.bootstrap4.min.js"></script>
@endpush