@extends('uneight-apanel::layouts.master-layout')

@section('page.content')

    <div class="box">
        <div class="box-header light lt">
            <h4>Основные</h4>
        </div>
        <div class="box-body">
            <dot-edit :dot="{{ json_encode($entity) }}" :region="{{ json_encode($entity->region) }}"
                      :district="{{ json_encode($entity->district) }}"
                      :water="{{ json_encode($entity->water) }}"></dot-edit>
        </div>
    </div>

@endsection
