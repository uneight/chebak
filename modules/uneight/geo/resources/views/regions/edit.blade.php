@extends('uneight-apanel::layouts.master-layout')

@section('page.content')

    {{ Form::open(['route' => Request::route()->getName(), 'action' => 'POST']) }}
    {{ Form::hidden('form[row][id]', (!empty($entity->id) ? $entity->id : null)) }}

    <div class="box">
        <div class="box-header light lt">
            <h4>Основные</h4>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="form-group col-6">
                    {{ Form::label('form[entity][country_id]', 'Страна:') }}
                    <uneight-autocomplete id="country"
                                          name="form[entity][country_id]"
                                          :selected='{!! json_encode((!empty($entity->country_id) ? ['id' => $entity->country_id, 'text' => $entity->country->name] : [])) !!}'
                                          placeholder=""
                                          route="{!! route('ap::geo:countries:autocomplete.post') !!}">
                    </uneight-autocomplete>
                </div>
                <div class="form-group col-6">
                    {{ Form::label('form[entity][name]', 'Название:') }}
                    {{ Form::text('form[entity][name]', (!empty($entity->name) ? $entity->name : null), ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="row">
                <div class="form-group col-6">
                    {{ Form::label('form[[entity]type]', 'Тип:') }}
                    {{ Form::select('form[entity][type]', \App\Entities\Geo\RegionEntity::TYPES, (!empty($entity->type) ? $entity->type : null), ['class' => 'form-control form-control-sm']) }}
                </div>
                <div class="form-group col-6">
                    {{ Form::label('form[entity][iso]', 'ISO code:') }}
                    {{ Form::text('form[entity][iso]', (!empty($entity->iso) ? $entity->iso : null), ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="row">
                <div class="form-group col-6">
                    {{ Form::label('form[entity][timezone]', 'Временная зона:') }}
                    {{ Form::text('form[entity][timezone]', (!empty($entity->timezone) ? $entity->timezone : null), ['class' => 'form-control']) }}
                </div>
                <div class="form-group col-6">
                    {{ Form::label('form[entity][auto_number_codes]', 'Коды авто номеров (через запятую):') }}
                    {{ Form::text('form[entity][auto_number_codes]', (!empty($entity->auto_number_codes) ? $entity->auto_number_codes : null), ['class' => 'form-control']) }}
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 text-right">
            {!! Form::button('Сохранить', ['class' => 'btn btn-fw submit white', 'type' => 'submit']) !!}
        </div>
    </div>

    {{ Form::close() }}

@endsection
