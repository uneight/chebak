@extends('uneight-apanel::layouts.master-layout')

@section('page.content')

    {{ Form::open(['route' => Request::route()->getName(), 'action' => 'POST']) }}
    {{ Form::hidden('form[row][id]', (!empty($entity->id) ? $entity->id : null)) }}

    <div class="box">
        <div class="box-header light lt">
            <h4>Основные</h4>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="form-group col-6">
                    {{ Form::label('form[entity][country_id]', 'Страна:') }}
                    {{ $entity->country->name }}
                </div>
                <div class="form-group col-6">
                    {{ Form::label('form[entity][name]', 'Название:') }}
                    {{ $entity->name }}
                </div>
            </div>
            <div class="row">
                <div class="form-group col-6">
                    {{ Form::label('form[[entity]type]', 'Тип:') }}
                    {{ \App\Entities\Geo\RegionEntity::TYPES[$entity->type] }}
                </div>
                <div class="form-group col-6">
                    {{ Form::label('form[entity][iso]', 'ISO code:') }}
                    {{ $entity->iso }}
                </div>
            </div>
            <div class="row">
                <div class="form-group col-6">
                    {{ Form::label('form[entity][timezone]', 'Временная зона:') }}
                    {{ $entity->timezone }}
                </div>
                <div class="form-group col-6">
                    {{ Form::label('form[entity][auto_number_codes]', 'Коды авто номеров (через запятую):') }}
                    {{ $entity->auto_number_codes }}
                </div>
            </div>
        </div>
    </div>

    <div class="box">
        <div class="box-header light lt">
            <h4>Список районов ({{ $entity->districts->count() }})</h4>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="form-group col-12">
                    @foreach($entity->districts as $district)
                        <a target="_blank" href="{{ route('district-posts.get', $district->id) }}"
                           class="btn btn-link mb-1">{{ $district->name }}</a>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="box-footer">
            <a class="btn success" href="{{ route('ap::geo:regions:get_districts.get', $entity->id) }}">Спарсить
                районы</a>
        </div>
    </div>

    <div class="box">
        <div class="box-header light lt">
            <h4>Список населенных пунктов ({{ $entity->localities->count() }})</h4>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-12">
                    @foreach($entity->localities as $locality)
                        <span class="text-primary mr-2">{{ $locality->name }}</span>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="box-footer">
            <a class="btn success" href="{{ route('ap::geo:regions:get_localities.get', $entity->id) }}">Спарсить
                населенные пункты</a>
        </div>
    </div>

    <div class="row">
        <div class="col-12 text-right">
            {!! Form::button('Сохранить', ['class' => 'btn btn-fw submit white', 'type' => 'submit']) !!}
        </div>
    </div>

    {{ Form::close() }}

@endsection
