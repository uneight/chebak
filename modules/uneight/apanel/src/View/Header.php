<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 20.04.2018
 * Time: 10:17
 */

namespace Uneight\Apanel\View;

use Illuminate\Contracts\View\Factory as ViewFactory;
use Illuminate\Support\HtmlString;
use Exception;
use stdClass;

/**
 * Class Header
 * @package Uneight\Apanel\View
 */
class Header
{
    /**
     * @var string
     */
    protected $title = '';

    /**
     * @var string
     */
    protected $description = '';

    /**
     * @var null|stdClass
     */
    protected $button = null;

    /**
     * @var ViewFactory
     */
    protected $viewFactory;

    /**
     * Header constructor.
     * @param ViewFactory $viewFactory
     *
     * @return void
     */
    public function __construct(ViewFactory $viewFactory)
    {
        $this->viewFactory = $viewFactory;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle(string $title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return $this
     */
    public function setDescription(string $description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getButton()
    {
        return $this->button;
    }

    /**
     * @param string $name
     * @param string $route
     * @param string $class
     *
     * @return $this
     */
    public function setButton(string $name, string $route, $class = 'primary')
    {
        $this->button = new stdClass();
        $this->button->name = $name;
        $this->button->route = $route;
        $this->button->class = $class;

        return $this;
    }

    /**
     * @return HtmlString
     * @throws Exception
     */
    public function render(): HtmlString
    {
        $html = $this->viewFactory->make('uneight-apanel::layouts.chunks.header', [
            'title' => $this->title,
            'description' => $this->description,
            'button' => $this->button
        ])->render();

        return new HtmlString($html);
    }

}