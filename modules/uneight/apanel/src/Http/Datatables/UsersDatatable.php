<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 29.03.2018
 * Time: 12:15
 */

namespace Uneight\Apanel\Http\Datatables;

use Uneight\Apanel\Entities\UserEntity;
use Yajra\DataTables\Services\DataTable;

class UsersDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)->setRowId('id')->addColumn('password', '');
    }

    /**
     * Get query source of dataTable.
     *
     * @param UserEntity $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(UserEntity $model)
    {
        return $model->newQuery()->select('id', 'login', 'email', 'first_name', 'last_name');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->parameters([
                'buttons' => ['csv', 'excel', 'pdf'],
                'renderer' => 'bootstrap',
                'order' => [0, 'asc'],
                'select' => [
                    'style' => 'os',
                    'selector' => 'td:first-child',
                ],
                'initComplete' => "function () {
                    this.api().columns().every(function () {
                        var column = this;
                        var input = document.createElement(\"input\");
                        $(input).appendTo($(column.footer()).empty())
                        .on('change', function () {
                            column.search($(this).val(), false, false, true).draw();
                        });
                    });
                }",
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            [
                'data' => 'id',
                'title' => 'ID',
                'searchable' => false

            ],
            'login',
            'email',
            'first_name',
            'last_name',
            [
                'data' => null,
                'defaultContent' => '',
                'className' => 'select-checkbox',
                'title' => 'Actions',
                'orderable' => false,
                'searchable' => false
            ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'users_' . time();
    }
}