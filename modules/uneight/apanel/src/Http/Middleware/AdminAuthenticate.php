<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 09.03.2018
 * Time: 17:45
 */

namespace Uneight\Apanel\Http\Middleware;

use App\Entities\UserEntity;
use Closure;
use Auth;

class AdminAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (!Auth::user() || !in_array(Auth::user()->role, [UserEntity::ROLE_ROOT, UserEntity::ROLE_ADMIN])) {
            return redirect()->route('index.get');
        }

        return $next($request);
    }
}