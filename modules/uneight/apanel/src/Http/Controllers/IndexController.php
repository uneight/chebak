<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 26.03.2018
 * Time: 10:15
 */

namespace Uneight\Apanel\Http\Controllers;

use Uneight\Apanel\Entities\UserEntity;
use Uneight\Attachments\Entities\AttachmentEntity;
use Uneight\Comments\Entities\CommentEntity;
use Uneight\Core\Http\Controllers\WebController;
use Uneight\Geo\Entities\DistrictEntity;
use Uneight\Geo\Entities\LocalityEntity;
use Uneight\Geo\Entities\RegionEntity;
use Uneight\Geo\Entities\WaterEntity;
use Uneight\Posts\Entities\PostEntity;

class IndexController extends WebController
{
    protected $viewPath = 'uneight-apanel';

    public function __construct()
    {

    }

    public function dashboard()
    {
        $waters = WaterEntity::count('id');
        $regions = RegionEntity::count('id');
        $districts = DistrictEntity::count('id');
        $localities = LocalityEntity::count('id');

        $districtRegions = DistrictEntity::distinct('region_id')->count('region_id');
        $localityDistricts = LocalityEntity::distinct('district_id')->count('district_id');

        $users = UserEntity::count('id');
        $posts = PostEntity::count('id');
        $comments = CommentEntity::has('post')->count('id');
        $attachments = AttachmentEntity::has('post')->count('id');

        return view('uneight-apanel::index', compact('regions', 'districts', 'localities', 'waters',
            'districtRegions', 'localityDistricts', 'users', 'posts', 'comments', 'attachments'));
    }
}