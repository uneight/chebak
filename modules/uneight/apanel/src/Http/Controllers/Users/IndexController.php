<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 29.03.2018
 * Time: 12:11
 */

namespace Uneight\Apanel\Http\Controllers\Users;

use Uneight\Apanel\Entities\UserEntity;
use Uneight\Apanel\Http\Datatables\UsersDatatable;
use Uneight\Apanel\Repositories\UserRepository;
use Uneight\Core\Http\Controllers\WebController;

class IndexController extends WebController
{
    protected $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(UsersDatatable $datatable)
    {
//        factory(UserEntity::class, 30)->create();
        return $datatable->render('uneight-apanel::users.index');
    }
}