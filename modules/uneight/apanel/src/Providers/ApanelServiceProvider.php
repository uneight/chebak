<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 26.03.2018
 * Time: 10:11
 */

namespace Uneight\Apanel\Providers;

use Illuminate\Support\ServiceProvider;
use Uneight\Apanel\View\Header;

class ApanelServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/../../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'uneight-apanel');
        $this->loadTranslationsFrom(__DIR__ . '/../../resources/lang', 'uneight-apanel');
        $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');


        $this->publishes([
            __DIR__.'/../../resources/assets' => public_path('uneight/apanel'),
        ], 'uneight-apanel');
    }

    public function register()
    {
        $this->app->singleton(Header::class, Header::class);
    }

}