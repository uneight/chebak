<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 29.03.2018
 * Time: 12:09
 */

Route::group(['prefix' => 'users', 'as' => 'users:', 'namespace' => 'Users'], function () {

    Route::get('/', ['as' => 'index.get', 'uses' => 'IndexController@index']);

});