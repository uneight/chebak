<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 26.03.2018
 * Time: 10:17
 */

Route::group(['prefix' => 'ap', 'as' => 'ap::', 'namespace' => 'Uneight\\Apanel\\Http\\Controllers', 'middleware' => ['web', 'auth.admin']], function () {
    Route::get('/', ['as' => 'index.get', 'uses' => 'IndexController@dashboard']);
});