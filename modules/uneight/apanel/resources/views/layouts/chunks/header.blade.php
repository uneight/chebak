<div class="col-6">
    <h5>{{$title}}</h5>
    <p class="text-muted text-sm">{{$description}}</p>
</div>

@if(!empty($button))
    <div class="col-6 justify-content-end text-right">
        <a href="{{ $button->route }}" class="btn btn-fw {{ $button->class }}">{{ $button->name }}</a>
    </div>
@endif