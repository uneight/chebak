<!-- ############ FOOTER START-->
<div class="content-footer white " id="content-footer">
    <div class="d-flex p-3">
        <span class="text-sm text-muted flex">&copy; Copyright. Uneight Development</span>
        <div class="text-sm text-muted">Version 0.0.1</div>
    </div>
</div>
<!-- ############ FOOTER END-->