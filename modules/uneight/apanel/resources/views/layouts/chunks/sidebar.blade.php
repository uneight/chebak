<div id="aside" class="app-aside box-shadow-x nav-expand white" aria-hidden="true">
    <div class="sidenav modal-dialog dk">
        <!-- sidenav top -->
        <div class="navbar lt text-center">
            <!-- brand -->
            <a href="{{ route('ap::index.get') }}" class="navbar-brand">
                @include('uneight-apanel::layouts.chunks.logo')
            </a>
            <!-- / brand -->
        </div>

        <!-- Flex nav content -->
        <div class="flex hide-scroll">
            <div class="scroll">
                <div class="nav-border b-primary" data-nav>
                    <ul class="nav bg">
                        <li class="nav-header">
                            <div class="py-3">
                                <button class="btn ladda-button btn-sm success theme-accent btn-block parse-posts"
                                        data-color="success" data-style="zoom-out" data-size="xs">
                                    <span class="ladda-label">
                                        <i class="fa fa-fw fa-download"></i>
                                        <span class="hidden-folded d-inline">Запустить парсер</span>
                                    </span>
                                </button>
                            </div>
                            <span class="text-xs hidden-folded">Основные</span>
                        </li>
                        <li class="{!!  (Request::url() == route('ap::index.get')) ? 'active' : '' !!}">
                            <a href="{{ route('ap::index.get') }}">
                                <span class="nav-icon"><i class="fa fa-dashboard"></i></span>
                                <span class="nav-text">Главная</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('ap::index.get') }}">
                                <span class="nav-icon"><i class="fa fa-list"></i></span>
                                <span class="nav-text">Меню</span>
                            </a>
                        </li>
                        <li class="{!!  (str_contains(Route::currentRouteName(), 'ap::users:')) ? 'active' : '' !!}">
                            <a href="{{ route('ap::users:index.get') }}">
                                <span class="nav-icon"><i class="fa fa-users"></i></span>
                                <span class="nav-text">Пользователи</span>
                            </a>
                        </li>
                        <li class="{!!  (str_contains(Route::currentRouteName(), 'ap::settings:')) ? 'active' : '' !!}">
                            <a href="{{ route('ap::settings:index.get') }}">
                                <span class="nav-icon"><i class="fa fa-gear"></i></span>
                                <span class="nav-text">Настройки</span>
                            </a>
                        </li>

                        <li class="nav-header">
                            <span class="text-xs hidden-folded">Гео справочники</span>
                        </li>
                        <li class="{!!  (str_contains(Route::currentRouteName(), 'ap::geo:countries:')) ? 'active' : '' !!}">
                            <a href="{{ route('ap::geo:countries:index.get') }}">
                                <span class="nav-icon"><i class="badge badge-xs badge-o md b-black"></i></span>
                                <span class="nav-text">Страны</span>
                            </a>
                        </li>
                        <li class="{!!  (str_contains(Route::currentRouteName(), 'ap::geo:regions:')) ? 'active' : '' !!}">
                            <a href="{{ route('ap::geo:regions:index.get') }}">
                                <span class="nav-icon"><i class="badge badge-xs badge-o md b-warn"></i></span>
                                <span class="nav-text">Регионы (Области)</span>
                            </a>
                        </li>
                        <li class="{!!  (str_contains(Route::currentRouteName(), 'ap::geo:districts:')) ? 'active' : '' !!}">
                            <a href="{{ route('ap::geo:districts:index.get') }}">
                                <span class="nav-icon"><i class="badge badge-xs badge-o md b-info"></i></span>
                                <span class="nav-text">Районы</span>
                            </a>
                        </li>
                        <li class="{!!  (str_contains(Route::currentRouteName(), 'ap::geo:localities:')) ? 'active' : '' !!}">
                            <a href="{{ route('ap::geo:localities:index.get') }}">
                                <span class="nav-icon"><i class="badge badge-xs badge-o md b-warning"></i></span>
                                <span class="nav-text">Населенные пункты</span>
                            </a>
                        </li>
                        <li class="{!!  (str_contains(Route::currentRouteName(), 'ap::geo:waters:')) ? 'active' : '' !!}">
                            <a href="{{ route('ap::geo:waters:index.get') }}">
                                <span class="nav-icon"><i class="badge badge-xs badge-o md b-primary"></i></span>
                                <span class="nav-text">Водоемы</span>
                            </a>
                        </li>
                        <li class="{!!  (str_contains(Route::currentRouteName(), 'ap::geo:dots:')) ? 'active' : '' !!}">
                            <a href="{{ route('ap::geo:dots:index.get') }}">
                                <span class="nav-icon"><i class="badge badge-xs badge-o md b-danger"></i></span>
                                <span class="nav-text">Места</span>
                            </a>
                        </li>

                        <li class="nav-header">
                            <span class="text-xs hidden-folded">Content</span>
                        </li>
                        <li class="{!!  (str_contains(Route::currentRouteName(), 'ap::categories:')) ? 'active' : '' !!}">
                            <a href="{{ route('ap::categories:index.get') }}">
                                <span class="nav-icon"><i class="fa fa-bars"></i></span>
                                <span class="nav-text">Категории публикаций</span>
                            </a>
                        </li>
                        <li class="{!!  (str_contains(Route::currentRouteName(), 'ap::youtube:')) ? 'active' : '' !!}">
                            <a href="{{ route('ap::youtube:index.get') }}">
                                <span class="nav-icon"><i class="fa fa-youtube"></i></span>
                                <span class="nav-text">Youtube NEW</span>
                            </a>
                        </li>

                        <li class="nav-header">
                            <span class="text-xs hidden-folded">ВКонтакте</span>
                        </li>
                        <li class="{!!  (str_contains(Route::currentRouteName(), 'ap::vk:groups:')) ? 'active' : '' !!}">
                            <a href="{{ route('ap::vk:groups:index.get') }}">
                                <span class="nav-icon"><i class="fa fa-bars"></i></span>
                                <span class="nav-text">Список групп</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        @include('uneight-apanel::layouts.chunks.sidebar.bottom')
    </div>
</div>