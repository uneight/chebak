<!-- ############ NAVBAR START-->
<div class="content-header white  box-shadow-0" id="content-header">
    <div class="navbar navbar-expand-lg">
        <!-- btn to toggle sidenav on small screen -->
        <a class="d-lg-none mx-2" data-toggle="modal" data-target="#aside">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 512 512"><path d="M80 304h352v16H80zM80 248h352v16H80zM80 192h352v16H80z"/></svg>
        </a>
        <!-- Page title -->
        <div class="navbar-text nav-title flex"></div>

        <ul class="nav flex-row order-lg-2">
            <li class="nav-item">
                <a target="_blank" class="btn btn-sm primary mx-3" href="{{route('index.get')}}">
                    <i class="fa fa-link"></i> Перейти на сайт
                </a>
            </li>

            <!-- User dropdown menu -->
            <li class="dropdown d-flex align-items-center">
                <a href="#" data-toggle="dropdown" class="d-flex align-items-center">
		        <span class="avatar w-32">
		          <img src="/dist/uneight/apanel/images/a8.jpg" alt="...">
		        </span>
                    <span class=" mx-2 d-none l-h-1x d-lg-block">
		          <span>{{ Auth::user()->email }}</span>
		            <small class="text-muted d-block">{{ \App\Entities\UserEntity::ROLES[Auth::user()->role] }}</small>
		        </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right w pt-0 mt-2 animate fadeIn">
                    <a class="dropdown-item" href="profile.html">
                        <span>Profile</span>
                    </a>
                    <a class="dropdown-item" href="setting.html">
                        <span>Settings</span>
                    </a>
                    <a class="dropdown-item" href="app.inbox.html">
                        <span class="float-right"><span class="badge info">6</span></span>
                        <span>Inbox</span>
                    </a>
                    <a class="dropdown-item" href="app.message.html">
                        <span>Message</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="signin.html">Sign out</a>
                </div>
            </li>
            <!-- Navarbar toggle btn -->
            <li class="d-lg-none d-flex align-items-center">
                <a href="#" class="mx-2" data-toggle="collapse" data-target="#navbarToggler">
                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 512 512"><path d="M64 144h384v32H64zM64 240h384v32H64zM64 336h384v32H64z"></path></svg>
                </a>
            </li>
        </ul>

    </div>
</div>
<!-- ############ NAVBAR END-->