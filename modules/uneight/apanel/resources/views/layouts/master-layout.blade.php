<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>UNEIGHT | APANEL</title>
    <meta name="description" content="Responsive, Bootstrap, BS4" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- for ios 7 style, multi-resolution icon of 152x152 -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
    <meta name="apple-mobile-web-app-title" content="Flatkit">
    <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @stack('styles')
    <link rel="stylesheet" href="/dist/uneight/apanel/css/app.css" type="text/css" />
    <!-- endbuild -->
</head>
<body>


<div class="app" id="app">

@include('uneight-apanel::layouts.chunks.sidebar')

    <div id="content" class="app-content box-shadow-0" role="main">

    @include('uneight-apanel::layouts.chunks.navbar')

        <div class="content-main " id="content-main">

            <div class="padding">

            <div class="row">
                @section('page.header')
                    {{ isset($header) ? $header : '' }}
                @show
            </div>

            @yield('page.breadcrumb')

            @yield('page.content')

            </div>

        </div>

        @include('uneight-apanel::layouts.chunks.footer')

    </div>

</div>

<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script src="/dist/uneight/apanel/js/app.js" type="application/javascript"></script>
<script type="application/javascript">
    $(function () {
        $('.parse-posts').click(function (e) {
            var ladda = Ladda.create(this);
            ladda.start();
            startParse(null, function () {
                $.jGrowl('Посты из групп успешно спарсены.', {
                    header: 'Успешно!',
                    theme: 'bg success'
                });
                ladda.stop();
            });
        });

        function startParse(id = null, success) {
            $.ajax({
                type: "POST",
                url: "{{ route('ap::vk:start-parse.get') }}",
                data: {
                    id: id,
                    _token: '{{ csrf_token() }}'
                },
                success: success
            });
        }
    });
</script>

@stack('scripts')

@if(session('notify'))
    <script>
        $(function () {
            @php $notify = session('notify') @endphp

            $.jGrowl('{{ $notify['text'] }}', {
                header: '{{ $notify['header'] }}',
                theme: 'bg {{ $notify['type'] }}'
            });
        });
    </script>
@endif

@if(session('errors'))
    <script>
        $(function () {
            @php $errors = session('errors') @endphp

            @foreach($errors->all() as $error)
            $.jGrowl('{{ $error }}', {
                header: 'Ошибка валидации',
                theme: 'bg danger',
                closerTemplate: '<div></div>',
                sticky: true
            });
            @endforeach
        });
    </script>
@endif
</body>
</html>
