@extends('uneight-apanel::layouts.master-layout')

@section('page.content')

    <div class="box">
        <div class="box-body">
            {!! $dataTable->table(['class' => 'table table-striped table-bordered'], true) !!}
        </div>
    </div>

@endsection

@push('styles')
    <link rel="stylesheet" href="https://nightly.datatables.net/buttons/css/buttons.bootstrap4.min.css">
@endpush

@push('scripts')
    <script src="/dist/uneight/apanel/js/libs/jquery.dataTables.min.js"></script>
    <script src="/dist/uneight/apanel/js/libs/dataTables.bootstrap4.js"></script>
    {!! $dataTable->scripts() !!}
    <script src="https://nightly.datatables.net/buttons/js/dataTables.buttons.min.js"></script>
    <script src="https://nightly.datatables.net/buttons/js/buttons.bootstrap4.min.js"></script>
@endpush