<div class="dropdown">
    <button class="btn white dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-gear"></i></button>
    <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-start"style="position: absolute; transform: translate3d(0px, 33px, 0px); top: 0px; left: 0px; will-change: transform;">

        @foreach($items as $item)
            @if($item['type'] == 'link')
                <a class="dropdown-item" href="{{ $item['route'] }}"><i class="fa {{ $item['icon'] }}"></i>{{ $item['name'] }}</a>
            @endif
            @if($item['type'] == 'remove')
                {{ Form::open(['method' => 'delete', 'url' => $item['route'], 'id' => $item['id']]) }}
                    {{ Form::hidden('form[id]', null) }}
                    <a class="dropdown-item" onclick="removeItem('{{ $item['id'] }}')"><i class="fa {{ $item['icon'] }}"></i>{{ $item['name'] }}</a>
                {{ Form::close() }}
            @endif
        @endforeach

    </div>
</div>

<script>
    function removeItem(id) {
        if (confirm("Вы подтверждаете удаление?")) {
            document.getElementById(id).submit();
            /*$.ajax(route, {
                type: 'delete',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (responce) {
                    window.location.reload();
                }
            });*/
        } else {
            return false;
        }
    }
</script>