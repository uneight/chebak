{{--<script>--}}
    function () {
        this.api().columns().every(function (key) {
            var column = this;
            var filter = column.init().columns[key].filter;

            if (filter) {
                if (filter.type == 'input') {
                    var input = document.createElement("input");
                    $(input).addClass('form-control form-control-sm').appendTo($(column.footer()).empty())
                        .on('keyup keydown', function () {
                            column.search($(this).val(), false, false, true).draw();
                        });
                }

                if (filter.type == 'select') {
                    var select = $('<select class="form-control form-control-sm"><option value="">Все</option></select>')
                        .appendTo($(column.footer()).empty())
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex($(this).val());

                    column.search(val ? '^' + val + '$' : '', true, false).draw();
                });

                $.each(column.init().columns[key].filter.data, function (index, value) {
                    select.append('<option value="' + index + '">' + value + '</option>')
                });
                }

                if (filter.type == 'datepicker') {

                }
            }

        });
    }
{{--</script>--}}