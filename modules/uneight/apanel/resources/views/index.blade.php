@extends('uneight-apanel::layouts.master-layout')

@section('page.header')
    <div class="row">
        <div class="col-6">
            <h5>Главная</h5>
            <p class="text-muted text-sm">Статистика портала</p>
        </div>
    </div>
@endsection

@section('page.content')
    <div class="row">
        <div class="col-sm-6 col-lg-3">
            <div class="box list-item">
                <span class="avatar w-40 text-center rounded primary"><span class="fa fa-users"></span></span>
                <div class="list-body">
                    <h4 class="m-0 text-md"><a href="#">{{ $regions }} <span class="text-sm">Регион(-ов/а)</span></a>
                    </h4>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-lg-3">
            <div class="box list-item">
                <span class="avatar w-40 text-center rounded success"><span class="fa fa-newspaper-o"></span></span>
                <div class="list-body">
                    <h4 class="m-0 text-md"><a href="#">{{ $districts }} <span class="text-sm">Район(-ов/а)</span></a>
                    </h4>
                    <small class="text-muted">В {{ $districtRegions }} регионах.</small>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-lg-3">
            <div class="box list-item">
                <span class="avatar w-40 text-center rounded warning"><span class="fa fa-comment"></span></span>
                <div class="list-body">
                    <h4 class="m-0 text-md"><a href="#">{{ $localities }} <span
                                    class="text-sm">Населенных пункт(-ов/а)</span></a></h4>
                    <small class="text-muted">В {{ $localityDistricts }} районах.</small>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-lg-3">
            <div class="box list-item">
                <span class="avatar w-40 text-center rounded warn"><span class="fa fa-paperclip"></span></span>
                <div class="list-body">
                    <h4 class="m-0 text-md"><a href="#">{{ $waters }} <span class="text-sm">Водоем(-ов/а)</span></a>
                    </h4>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 col-lg-3">
            <div class="box list-item">
                <span class="avatar w-40 text-center rounded primary"><span class="fa fa-users"></span></span>
                <div class="list-body">
                    <h4 class="m-0 text-md"><a href="#">{{ $users }} <span class="text-sm">Пользователей</span></a></h4>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-lg-3">
            <div class="box list-item">
                <span class="avatar w-40 text-center rounded success"><span class="fa fa-newspaper-o"></span></span>
                <div class="list-body">
                    <h4 class="m-0 text-md"><a href="#">{{ $posts }} <span class="text-sm">Публикаций</span></a></h4>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-lg-3">
            <div class="box list-item">
                <span class="avatar w-40 text-center rounded warning"><span class="fa fa-comment"></span></span>
                <div class="list-body">
                    <h4 class="m-0 text-md"><a href="#">{{ $comments }} <span class="text-sm">Комментариев</span></a>
                    </h4>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-lg-3">
            <div class="box list-item">
                <span class="avatar w-40 text-center rounded warn"><span class="fa fa-paperclip"></span></span>
                <div class="list-body">
                    <h4 class="m-0 text-md"><a href="#">{{ $attachments }} <span class="text-sm">Вложений</span></a>
                    </h4>
                </div>
            </div>
        </div>
    </div>
@endsection