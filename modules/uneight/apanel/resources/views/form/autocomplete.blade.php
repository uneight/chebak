{{ Form::select('form[country]', [], $field['value'], ['class' => 'form-control', 'id' => '_' . $field['id'], 'placeholder' => $field['placeholder']]) }}
{{ Form::hidden($field['name'], $field['value'], ['id' => $field['id']]) }}

@push('scripts')
    <script>
        $(function () {
            var object = $('#_' + '{{ $field['id'] }}');

            object.select2({
                placeholder: '{{ $field['placeholder'] }}',
                allowClear: true,
                width: '100%',
                ajax: {
                    url: '{{ $route }}',
                    type: 'post',
                    data: function (params) {
                        return {
                            query: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    }
                },
                minimumInputLength: 1
            });

            $('#_' + '{{$field['id']}}').on('change', function () {
                $('#' + '{{$field['id']}}').val($(this).val());
            });
        });
    </script>
@endpush