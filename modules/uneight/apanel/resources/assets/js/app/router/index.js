import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import Dashboard from '../../components/DashboardComponent';

export default new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            component: Dashboard,
            activeClass: 'active'
        },
    ],
    scrollBehavior (to, from, savedPosition) {
        return !savedPosition ? { x: 0, y: 0 } : savedPosition
    }
});