<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('old_id')->nullable();

            $table->integer('category_id')->nullable();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('set null');

            $table->integer('group_id')->nullable();
            $table->foreign('group_id')->references('id')->on('vk_groups')->onDelete('set null');

            $table->integer('receive_type');

            $table->integer('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->string('title');
            $table->text('content')->nullable();

            $table->string('slug')->nullable();

            $table->integer('rate')->default(0);

            $table->integer('region_id')->nullable()->unsigned();
            $table->foreign('region_id')->references('id')->on('geo_regions')->onDelete('set null');

            $table->integer('district_id')->nullable()->unsigned();
            $table->foreign('district_id')->references('id')->on('geo_districts')->onDelete('set null');

            $table->integer('locality_id')->nullable()->unsigned();
            $table->foreign('locality_id')->references('id')->on('geo_localities')->onDelete('set null');

            $table->integer('water_id')->nullable()->unsigned();
            $table->foreign('water_id')->references('id')->on('geo_water')->onDelete('set null');

            $table->integer('dot_id')->nullable()->unsigned();
            $table->foreign('dot_id')->references('id')->on('geo_dots')->onDelete('set null');

            $table->string('external_from_id')->nullable();
            $table->string('external_owner_id')->nullable();
            $table->string('external_type')->nullable();

            $table->jsonb('old_data')->nullable();

            $table->boolean('active')->default(true);
            $table->softDeletes();

            $table->timestamps();
        });

        DB::raw('ALTER SEQUENCE posts_id_seq RESTART WITH 2000000;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
