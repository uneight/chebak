@extends('uneight-apanel::layouts.master-layout')

@section('page.content')

    <ul class="nav nav-sm nav-pills nav-active-primary mb-3 clearfix">
        <li class="nav-item"><a class="nav-link active show" href="#" data-toggle="tab" data-target="#tab_1">Все посты
                <span class="badge badge-sm primary ml-2">160</span></a></li>
        <li class="nav-item"><a class="nav-link" href="#" data-toggle="tab" data-target="#tab_2">Посты без категорий
                <span
                        class="badge badge-sm primary ml-2">12</span></a></li>
    </ul>

    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12 d-flex align-items-stretch">
            <div class="box">
                <div class="box-footer light lt">
                    <div>
                        <small>#200090 || Рыбаки Магнитогорска рыбалка в Магнитогорске</small>
                        <a class="btn btn-sm btn-icon" href=""><i class="fa fa-external-link"></i></a>
                    </div>
                </div>
                <div class="box-tool">
                    <ul class="nav nav-xs">
                        <li class="nav-item">
                            <button class="btn btn-sm btn-icon btn-outline b-success text-success"><i
                                        class="fa fa-check"></i></button>
                            <button class="btn btn-sm btn-icon btn-outline b-danger text-danger"><i
                                        class="fa fa-times"></i></button>
                        </li>
                    </ul>
                </div>
                <img class="w-100" src="https://pp.userapi.com/c831408/v831408181/13f939/4ExIZ0krS3c.jpg" alt="Card image cap">
                <div class="box-body">
                    <p class="m-0 text-muted">
                        Всем доброго вечера выходного дня 😉 Поздравляю всех с фантастической победой сборной над
                        испанцами 👏👏👏💪 Будучи в гостях в Наваринке, катался вчера вечером и сегодня днём на пару
                        часиков на Гумбейку спин помочить😊 Ездил на расстояние 2 километра с небольшим от трассы вверх
                        по течению, в сторону Скляра. Речушка заросла капитально, единственной пригодной в данных
                        условиях приманкой служил неогруженный силикон на оффсетнике... Ну и как следствие, в небольших
                        чистых окошках на силикон выстреливали шнурки 😂 Вчера вечером за пару часов было штук 17-20
                        "выстрелов", около 10 юных шнурков 200-400 грамм удалось засечь 😊 Всех домой. Сегодня же поехал
                        в обед, жара стояла дикая, щука практически бастовала, за пару часов было 5-6-7 выходов, дёрнул
                        одну 200-граммовую 😂 Слепни и "бзыки" обожрали всего 😂 Погулял, подышал, спасибо речке 😉👍
                    </p>
                </div>
                <div class="box-body light lt">
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Категория:</label>
                        <div class="col-sm-10">
                            <select class="form-control form-control-sm">
                                @foreach($categories as $category)
                                    <option>{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Водоем:</label>
                        <div class="col-sm-10">
                            <select class="form-control form-control-sm">
                                <option>Урал</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--<posts-list></posts-list>--}}

@endsection
