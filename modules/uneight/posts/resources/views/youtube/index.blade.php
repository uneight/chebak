@extends('uneight-apanel::layouts.master-layout')

@section('page.content')

    <youtube-list :categories="{{ json_encode($categories) }}"></youtube-list>

@endsection
