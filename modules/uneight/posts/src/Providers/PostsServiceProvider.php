<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 20.04.2018
 * Time: 12:10
 */

namespace Uneight\Posts\Providers;

use Uneight\Core\Providers\ModuleServiceProvider;

class PostsServiceProvider extends ModuleServiceProvider
{
    protected $moduleName = 'uneight-posts';
    protected $modulePath = __DIR__;

    public function boot()
    {
        parent::boot();

        $this->publishes([
            $this->modulePath . '/../../config/uneight-posts.php' => config_path('uneight-posts.php'),
        ], 'uneight-posts');
    }

    public function register()
    {
        $this->mergeConfigFrom(
            $this->modulePath . '/../../config/uneight-posts.php', 'uneight-posts'
        );
    }
}