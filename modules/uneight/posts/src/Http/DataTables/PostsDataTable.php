<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 23.04.2018
 * Time: 14:36
 */

namespace Uneight\Posts\Http\DataTables;

use Uneight\Posts\Entities\PostEntity;
use Yajra\DataTables\Services\DataTable;

class PostsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     *
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable ($query)
    {
        return datatables($query)
            ->setRowId('id')
            ->editColumn('active', function ($row) {
                return ($row->active)
                    ? '<span class="badge badge-pill py-1 px-3 success">Да</span>'
                    : '<span class="badge badge-pill py-1 px-3 danger">Нет</span>';
            })
            ->editColumn('category_id', function ($row) {
                return $row->category()->first()->name;
            })
            ->rawColumns(['active', 'action'])
            ->addColumn('action', function ($row) {
                return view('uneight-apanel::dataTables.row-tools', [
                    'items' => [
                        [
                            'type'  => 'link',
                            'name'  => 'Редактировать',
                            'route' => route('ap::posts:edit.get', $row->id),
                            'icon'  => 'fa-pencil',
                        ],
                        [
                            'type'            => 'link',
                            'name'            => 'Удалить',
                            'route'           => '',
                            'icon'            => 'fa-trash',
                            'onClickFunction' => '',
                        ],
                    ],
                ])->render();
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param PostEntity $model
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query (PostEntity $model)
    {
        return $model->newQuery()->select('id', 'user_id', 'title', 'slug', 'active');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html ()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->parameters([
                'render'       => 'custom',
                'order'        => [0, 'asc'],
                'select'       => [
                    'style'    => 'os',
                    'selector' => 'td:first-child',
                ],
                'initComplete' => view('uneight-apanel::dataTables.init-complete'),
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns ()
    {
        return [
            'id'          => [
                'title'       => 'ID',
                'class'       => 'text-center',
                'placeholder' => '',
                'filter'      => [
                    'type' => 'input',
                ],
            ],
            'user_id'     => [
                'title'       => 'Пользователь',
                'class'       => 'text-center',
                'placeholder' => '',
                'filter'      => [
                    'type' => 'input',
                ],
            ],
            'category_id' => [
                'title'       => 'Категория',
                'class'       => 'text-center',
                'placeholder' => '',
                'filter'      => [
                    'type' => 'input',
                ],
            ],
            'title'       => [
                'title'       => 'Название',
                'class'       => 'text-center',
                'placeholder' => '',
                'filter'      => [
                    'type' => 'input',
                ],
            ],
            'slug'        => [
                'title'       => 'URL',
                'class'       => 'text-center',
                'placeholder' => '',
                'filter'      => [
                    'type' => 'input',
                ],
            ],
            'active'      => [
                'title'       => 'Активный',
                'class'       => 'text-center',
                'placeholder' => '',
                'filter'      => [
                    'type' => 'select',
                    'data' => [
                        'true'  => 'Да',
                        'false' => 'Нет',
                    ],
                ],
            ],
            'tools'       => [
                'name'       => 'tools',
                'data'       => 'action',
                'orderable'  => false,
                'searchable' => false,
                'filter'     => false,
                'title'      => '',
                'class'      => 'text-center',
            ],
        ];
    }
}