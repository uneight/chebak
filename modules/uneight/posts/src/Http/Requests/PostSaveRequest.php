<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 27.04.2018
 * Time: 14:49
 */

namespace Uneight\Posts\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Uneight\Core\Contracts\SaveRequest;

class PostSaveRequest extends FormRequest implements SaveRequest
{
    public function rules ()
    {
        // TODO: Implement rules() method.
    }

    public function messages ()
    {
        // TODO: Implement messages() method.
    }
}