<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 23.04.2018
 * Time: 14:23
 */

namespace Uneight\Posts\Http\Controllers;

use Illuminate\Http\Request;
use Uneight\Categories\Entities\CategoryEntity;
use Uneight\Core\Http\Controllers\CRUDController;
use Uneight\Posts\Entities\PostEntity;
use Uneight\Posts\Http\DataTables\PostsDataTable;
use Uneight\Posts\Http\Requests\PostSaveRequest;

class PostsController extends CRUDController
{
    public function __construct()
    {
        $this->_viewPath = 'uneight-posts::';
        $this->entity = new PostEntity();

        parent::__construct();
    }

    public function indexAction(PostsDataTable $dataTable)
    {
        $categories = CategoryEntity::all();

        $this->header->setTitle('Публикации')
            ->setDescription('Список публикаций')
            ->setButton('Добавить публикацию', route('ap::posts:edit.get'))
            ->render();

        return $this->view('index', compact('categories'));

        return $this->getIndex($dataTable);
    }

    public function editAction(Request $request, $id = null)
    {
        $this->header->setTitle('Публикации')
            ->setDescription('Редактировать публикацию')
            ->setButton('Назад', route('ap::posts:index.get'), 'success');

        return $this->getEdit($request);
    }

    public function storeAction(PostSaveRequest $request)
    {
        return $this->postEdit($request);
    }

    public function deleteAction(Request $request)
    {

    }

    public function autocompleteAction(Request $request)
    {
        return $this->entity->where('name', 'LIKE', '%' . $request->input('query') . '%')->get()->map(function ($item) {
            return [
                'id' => $item->id,
                'text' => $item->name
            ];
        });
    }
}