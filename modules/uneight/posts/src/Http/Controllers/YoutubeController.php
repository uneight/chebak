<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 10/24/18
 * Time: 2:32 PM
 */

namespace Uneight\Posts\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Uneight\Categories\Entities\CategoryEntity;
use Uneight\Core\Http\Controllers\CRUDController;
use Uneight\Posts\Entities\PostEntity;
use Uneight\Posts\Http\DataTables\PostsDataTable;
use Uneight\Posts\Http\Requests\PostSaveRequest;
use Exception;
use DB;

class YoutubeController extends CRUDController
{
    public function __construct()
    {
        $this->_viewPath = 'uneight-posts::youtube.';
        $this->entity = new PostEntity();

        parent::__construct();
    }

    public function indexAction(PostsDataTable $dataTable)
    {
        $categories = CategoryEntity::all();

        $this->header->setTitle('Новые Youtube видео')
            ->setDescription('Список видео')
            ->render();

        $posts = PostEntity::where('source', 'youtube')->where('is_moderated', false)->orderBy('id', 'desc')->get();

        return $this->view('index', compact('categories', 'posts'));
    }

    public function editAction(Request $request, $id = null)
    {
        $this->header->setTitle('Youtube')
            ->setDescription('Редактировать видео')
            ->setButton('Назад', route('ap::posts:index.get'), 'success');

        return $this->getEdit($request);
    }

    public function storeAction(PostSaveRequest $request)
    {
        return $this->postEdit($request);
    }

    public function ajaxStoreAction(Request $request)
    {
        $id = $request->get('id');
        $this->entity = $this->entity->find($id);

        $this->entity->title = $request->get('title') ? $request->get('title') : "";
        $this->entity->content = $request->get('content');
        $this->entity->category_id = $request->get('category');
        $this->entity->is_moderated = true;

        try {
            DB::transaction(function () {
                $this->entity->save();
            });
        } catch (Exception $exception) {
            return new JsonResponse([
                'status' => 'error',
                'code' => 500,
                'message' => $exception->getMessage(),
                'data' => [],
            ], 500);
        }

        return new JsonResponse([
            'status' => 'success',
            'code' => 200,
            'message' => 'Успешно сохранено',
            'data' => [],
        ], 200);
    }

    public function ajaxDeleteAction(Request $request)
    {
        $id = $request->get('id');
        $this->entity = $this->entity->find($id);

        if (empty($this->entity)) {
            return new JsonResponse([
                'status' => 'error',
                'code' => 401,
                'message' => 'Сущности с идентификатором ' . $id . ' не существует.',
                'data' => [],
            ], 404);
        }

        try {
            $this->entity->delete();
        } catch (Exception $exception) {
            return new JsonResponse([
                'status' => 'error',
                'code' => 500,
                'message' => $exception->getMessage(),
                'data' => [],
            ], 500);
        }

        return new JsonResponse([
            'status' => 'success',
            'code' => 200,
            'message' => 'Успешно удалено',
            'data' => [],
        ], 200);
    }

    public function autocompleteAction(Request $request)
    {
        return $this->entity->where('name', 'LIKE', '%' . $request->input('query') . '%')->get()->map(function ($item) {
            return [
                'id' => $item->id,
                'text' => $item->name
            ];
        });
    }

    public function ajaxLoadingAction(Request $request)
    {
        $limit = $request->get('limit', 50);
        $offset = $request->get('offset', 0);

        $posts = PostEntity::with('attachments')->where('source', 'youtube')
            ->where('is_moderated', false)
            ->orderBy('id', 'desc')
            ->offset($offset)
            ->limit($limit)
            ->get();

        return new JsonResponse([
            'status' => 'success',
            'code' => 200,
            'message' => null,
            'data' => $posts,
        ], 200);
    }
}