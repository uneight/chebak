<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 23.04.2018
 * Time: 14:24
 */

namespace Uneight\Posts\Entities;

use App\Entities\Geo\RegionEntity;
use App\Entities\Geo\WaterEntity;
use App\Entities\UserEntity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Uneight\Attachments\Traits\HasAttachment;
use Uneight\Categories\Traits\HasCategory;
use Uneight\Comments\Traits\HasComments;
use Uneight\Geo\Entities\DotEntity;
use Uneight\VkParser\Entities\GroupEntity;
use Eloquent;

class PostEntity extends Eloquent
{
    use SoftDeletes, HasCategory, HasAttachment, HasComments;

    protected $table = 'posts';

    protected $fillable = [
        'old_id', 'receive_type', 'user_id', 'title', 'content', 'slug', 'active', 'created_at', 'region_id', 'district_id',
        'locality_id', 'water_id', 'dot_id', 'external_user', 'external_from_id', 'external_owner_id', 'old_data', 'updated_at',
        'group_id', 'category_id', 'waters_founded', 'source'
    ];

    public function user()
    {
        return $this->belongsTo(UserEntity::class);
    }

    public function group()
    {
        return $this->belongsTo(GroupEntity::class, 'group_id', 'id');
    }

    public function water()
    {
        return $this->belongsTo(WaterEntity::class, 'water_id', 'id');
    }

    public function region()
    {
        return $this->belongsTo(RegionEntity::class, 'region_id', 'id');
    }

    public function dot ()
    {
        return $this->belongsTo(DotEntity::class, 'dot_id', 'id');
    }
}