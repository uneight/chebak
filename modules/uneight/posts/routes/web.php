<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 20.04.2018
 * Time: 12:10
 */

Route::group(['prefix' => 'ap', 'as' => 'ap::', 'namespace' => 'Uneight\\Posts\\Http\\Controllers', 'middleware' => ['web']], function() {

    Route::group(['prefix' => 'posts', 'as' => 'posts:'], function () {
        Route::get('/', ['as' => 'index.get', 'uses' => 'PostsController@indexAction']);
        Route::get('/edit/{id?}', ['as' => 'edit.get', 'uses' => 'PostsController@editAction']);
        Route::post('/edit/{id?}', ['as' => 'edit.post', 'uses' => 'PostsController@storeAction']);
        Route::delete('/delete/{id}', ['as' => 'remove.delete', 'uses' => 'PostsController@removeAction']);
        Route::post('/autocomplete', ['as' => 'autocomplete.post', 'uses' => 'PostsController@autocompleteAction']);
    });

});

Route::group(['prefix' => 'ap', 'as' => 'ap::', 'namespace' => 'Uneight\\Posts\\Http\\Controllers', 'middleware' => ['web']], function() {

    Route::group(['prefix' => 'youtube', 'as' => 'youtube:'], function () {
        Route::get('/', ['as' => 'index.get', 'uses' => 'YoutubeController@indexAction']);
        Route::get('/edit/{id?}', ['as' => 'edit.get', 'uses' => 'YoutubeController@editAction']);
        Route::post('/edit/{id?}', ['as' => 'edit.post', 'uses' => 'YoutubeController@storeAction']);
        Route::delete('/delete/{id}', ['as' => 'remove.delete', 'uses' => 'YoutubeController@removeAction']);
        Route::post('/autocomplete', ['as' => 'autocomplete.post', 'uses' => 'YoutubeController@autocompleteAction']);

        Route::get('/ajax/loading', 'YoutubeController@ajaxLoadingAction');
        Route::post('/ajax/edit', 'YoutubeController@ajaxStoreAction');
        Route::post('/ajax/delete', 'YoutubeController@ajaxDeleteAction');
    });

});