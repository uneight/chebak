<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 8/9/18
 * Time: 8:31 PM
 */

namespace Uneight\Settings\Entities;

use Eloquent;

class SettingEntity extends Eloquent
{
    const TYPE_WATER_CUT_WORDS = 'water_cut_words';
    const TYPE_WATER_STOP_WORDS = 'water_stop_words';

    protected $table = 'settings';

    protected $fillable = [
        'id', 'key', 'name', 'value', 'active',
    ];
}