<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 8/9/18
 * Time: 7:03 PM
 */

namespace Uneight\Settings\Providers;

use Uneight\Core\Providers\ModuleServiceProvider;

class SettingsServiceProvider extends ModuleServiceProvider
{
    protected $moduleName = 'uneight-settings';
    protected $modulePath = __DIR__;

    public function boot ()
    {
        parent::boot();

        $this->publishes([
            $this->modulePath . '/../../config/uneight-settings.php' => config_path('uneight-settings.php'),
        ], 'uneight-settings');
    }

    public function register ()
    {
        $this->mergeConfigFrom(
            $this->modulePath . '/../../config/uneight-settings.php', 'uneight-settings'
        );
    }
}