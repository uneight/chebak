<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 8/9/18
 * Time: 8:09 PM
 */

namespace Uneight\Settings\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Uneight\Core\Contracts\SaveRequest;

class SettingSaveRequest extends FormRequest implements SaveRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize ()
    {
        return true;
    }

    public function rules ()
    {
        $rules = [
            'form.entity.key'   => 'string',
            'form.entity.name'  => 'required|string',
            'form.entity.value' => 'string|nullable',
        ];

        if ($this->request->get('row.id')) {
            $rules['form.entity.key'] = 'required|string';
        }

        return $rules;
    }

    public function messages ()
    {
        return parent::messages();
    }
}