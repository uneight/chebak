<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 8/9/18
 * Time: 8:28 PM
 */

namespace Uneight\Settings\Http\DataTables;

use Uneight\Settings\Entities\SettingEntity;
use Yajra\DataTables\Services\DataTable;

class SettingsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     *
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable ($query)
    {
        return datatables($query)
            ->setRowId('id')
            ->editColumn('active', function ($row) {
                return ($row->active)
                    ? '<span class="badge badge-pill py-1 px-3 success">Да</span>'
                    : '<span class="badge badge-pill py-1 px-3 danger">Нет</span>';
            })
            ->rawColumns(['active', 'action'])
            ->addColumn('action', function ($row) {
                return view('uneight-apanel::dataTables.row-tools', [
                    'items' => [
                        [
                            'type'  => 'link',
                            'name'  => 'Редактировать',
                            'route' => route('ap::settings:edit.get', $row->id),
                            'icon'  => 'fa-pencil',
                        ],
                    ],
                ])->render();
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param CategoryEntity $model
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query (SettingEntity $model)
    {
        return $model->newQuery()->select('id', 'key', 'name', 'value', 'active', 'updated_at');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html ()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->parameters([
                'render'       => 'custom',
                'order'        => [0, 'asc'],
                'select'       => [
                    'style'    => 'os',
                    'selector' => 'td:first-child',
                ],
                'initComplete' => view('uneight-apanel::dataTables.init-complete'),
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns ()
    {
        return [
            'id'     => [
                'title'       => 'ID',
                'class'       => 'text-center',
                'placeholder' => '',
                'filter'      => [
                    'type' => 'input',
                ],
            ],
            'key'    => [
                'title'       => 'Ключ',
                'class'       => 'text-center',
                'placeholder' => '',
                'filter'      => [
                    'type' => 'input',
                ],
            ],
            'name'   => [
                'title'       => 'Название',
                'class'       => 'text-center',
                'placeholder' => '',
                'filter'      => [
                    'type' => 'input',
                ],
            ],
            'value'  => [
                'title'       => 'Значение',
                'class'       => 'text-center',
                'placeholder' => '',
                'filter'      => [
                    'type' => 'input',
                ],
            ],
            'active' => [
                'title'       => 'Активный',
                'class'       => 'text-center',
                'placeholder' => '',
                'filter'      => [
                    'type' => 'select',
                    'data' => [
                        'true'  => 'Да',
                        'false' => 'Нет',
                    ],
                ],
            ],
            'tools'  => [
                'name'       => 'tools',
                'data'       => 'action',
                'orderable'  => false,
                'searchable' => false,
                'filter'     => false,
                'title'      => '',
                'class'      => 'text-center',
            ],
        ];
    }
}