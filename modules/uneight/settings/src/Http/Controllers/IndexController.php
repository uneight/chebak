<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 23.04.2018
 * Time: 16:01
 */

namespace Uneight\Settings\Http\Controllers;

use Illuminate\Http\Request;
use Uneight\Core\Http\Controllers\CRUDController;
use App\Entities\Geo\WaterEntity;
use Uneight\Posts\Entities\PostEntity;
use Uneight\Settings\Entities\SettingEntity;
use Uneight\Settings\Http\DataTables\SettingsDataTable;
use Uneight\Settings\Http\Requests\SettingSaveRequest;
use DB;

class IndexController extends CRUDController
{
    public function __construct ()
    {
        $this->_viewPath = 'uneight-settings::';
        $this->entity = new SettingEntity();

        parent::__construct();
    }

    public function indexAction (SettingsDataTable $dataTable)
    {
        $this->header->setTitle('Настройки')
            ->setDescription('Список параметров')
            ->setButton('Добавить параметр', route('ap::settings:edit.get'))
            ->render();

        return $this->getIndex($dataTable);
    }

    public function editAction (Request $request)
    {
        $this->header->setTitle('Настройки')
            ->setDescription('Редактировать параметр')
            ->setButton('Назад', route('ap::settings:index.get'), 'success')
            ->render();

        return $this->getEdit($request);
    }

    public function storeAction (SettingSaveRequest $request)
    {
        $row = $request->input('form.row');
        $oldEntity = $this->entity->find($row['id']);

        $return = $this->postEdit($request);


        $oldArray = explode(',', $oldEntity->value);
        $newArray = explode(',', $this->entity->value);

        $deletedItems = array_diff($oldArray, $newArray);
        $addedItems = array_diff($newArray, $oldArray);

        if ($this->entity->key == SettingEntity::TYPE_WATER_CUT_WORDS) {
            $query = 'name';

            foreach ($addedItems as $item) {
                $query = "replace(" . $query . ", '" . $item . "', '')";
            }

            DB::update('UPDATE geo_water SET name = trim(' . $query . ')');
        }

        if ($this->entity->key == SettingEntity::TYPE_WATER_STOP_WORDS) {
            $string = implode(', ', $addedItems);
            $waters = DB::table('geo_water')
                ->whereRaw("plainto_tsquery('russian', name) @@ to_tsvector('russian', '" . trim($string, '?') . "')")
                ->pluck('id')
                ->toArray();

            DB::table('posts')->whereIn('water_id', $waters)->update(['water_id' => null]);
        }

        return $return;
    }

    public function autocompleteAction (Request $request)
    {
        return $this->entity->where('name', 'LIKE', '%' . $request->input('query') . '%')->get()->map(function ($item) {
            return [
                'id'   => $item->id,
                'text' => $item->name,
            ];
        });
    }
}