@extends('uneight-apanel::layouts.master-layout')

@section('page.content')

    {{ Form::open(['route' => Request::route()->getName(), 'action' => 'POST']) }}
    {{ Form::hidden('form[row][id]', (!empty($entity->id) ? $entity->id : null)) }}

    <div class="box">
        <div class="box-header light lt">
            <h4>Основные</h4>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="form-group col-6">
                    {{ Form::label('form[entity][key]', 'Ключ:') }}
                    @if(!empty($entity->id))
                        {{ Form::text('', (!empty($entity->key) ? $entity->key : null), ['class' => 'form-control', 'disabled' => 'disabled']) }}
                    @else
                        {{ Form::text('form[entity][key]', (!empty($entity->key) ? $entity->key : null), ['class' => 'form-control']) }}
                    @endif
                </div>
                <div class="form-group col-6">
                    {{ Form::label('form[entity][name]', 'Название:') }}
                    {{ Form::text('form[entity][name]', (!empty($entity->name) ? $entity->name : null), ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="row">
                <div class="form-group col-12">
                    {{ Form::label('form[entity][value]', 'Значение:') }}
                    {{ Form::text('form[entity][value]', (!empty($entity->value) ? $entity->value : null), ['class' => 'form-control']) }}
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 text-right">
            {!! Form::button('Сохранить', ['class' => 'btn btn-fw submit white', 'type' => 'submit']) !!}
        </div>
    </div>

    {{ Form::close() }}

@endsection
