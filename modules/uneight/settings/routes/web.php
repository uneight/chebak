<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 23.04.2018
 * Time: 16:00
 */

Route::group(['prefix' => 'ap', 'as' => 'ap::', 'namespace' => 'Uneight\\Settings\\Http\\Controllers', 'middleware' => ['web']], function () {

    Route::group(['prefix' => 'settings', 'as' => 'settings:'], function () {
        Route::get('/', ['as' => 'index.get', 'uses' => 'IndexController@indexAction']);
        Route::get('/edit/{id?}', ['as' => 'edit.get', 'uses' => 'IndexController@editAction']);
        Route::post('/edit/{id?}', ['as' => 'edit.post', 'uses' => 'IndexController@storeAction']);
    });

});