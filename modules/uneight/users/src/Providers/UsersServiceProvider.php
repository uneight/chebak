<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 26.03.2018
 * Time: 12:22
 */

namespace Uneight\Users\Providers;

use Uneight\Core\Providers\ModuleServiceProvider;

class UsersServiceProvider extends ModuleServiceProvider
{
    protected $moduleName = 'uneight-users';
    protected $modulePath = __DIR__;
}