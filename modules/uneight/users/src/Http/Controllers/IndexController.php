<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 26.03.2018
 * Time: 12:27
 */

namespace Uneight\Users\Http\Controllers;

use Uneight\Core\Http\Controllers\CRUDController;
use Uneight\Users\Entities\UserEntity;
use Uneight\Users\Http\DataTables\UsersDataTable;

class IndexController extends CRUDController
{
    public function __construct()
    {
        $this->_viewPath = 'uneight-users::';
        $this->entity = new UserEntity();
        parent::__construct();
    }

    public function indexAction(UsersDataTable $dataTable)
    {
        $this->header->setTitle('Пользователи')
            ->setDescription('Список пользователей')
            ->setButton('Добавить пользователя', route('ap::users:edit.get'))
            ->render();

        return $this->getIndex($dataTable);
    }

    public function editAction()
    {
        return view('uneight-users::edit');
    }

    public function storeAction()
    {

    }

    public function viewAction()
    {
        return view('uneight-users::view');
    }

    public function deleteAction()
    {

    }

    public function datatableAction()
    {

    }
}