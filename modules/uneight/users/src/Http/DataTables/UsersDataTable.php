<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 26.03.2018
 * Time: 15:25
 */

namespace Uneight\Users\Http\DataTables;

use Uneight\Users\Entities\UserEntity;
use Yajra\DataTables\Services\DataTable;

class UsersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->setRowId('id')
            ->addColumn('password', '')
            ->editColumn('role', function ($row) {
                return UserEntity::ROLES[$row->role];
            })
            ->addColumn('action', function($row) {
                return view('uneight-apanel::dataTables.row-tools', [
                    'items' => [
                        [
                            'type' => 'link',
                            'name' => 'Просмотр',
                            'route' => '',
                            'icon' => 'fa-eye'
                        ],
                        [
                            'type' => 'link',
                            'name' => 'Редактировать',
                            'route' => route('ap::users:edit.get', $row->id),
                            'icon' => 'fa-pencil'
                        ],
                        [
                            'type' => 'link',
                            'name' => 'Удалить',
                            'route' => '',
                            'icon' => 'fa-trash',
                            'onClickFunction' => ''
                        ],
                    ]
                ])->render();
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param UserEntity $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(UserEntity $model)
    {
        return $model->newQuery()->select('id', 'login', 'email', 'full_name', 'role', 'created_at');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->parameters([
                'render' => 'custom',
                'order' => [0, 'asc'],
                'select' => [
                    'style' => 'os',
                    'selector' => 'td:first-child',
                ],
                'initComplete' => view('uneight-apanel::dataTables.init-complete'),
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => [
                'title' => 'ID',
                'class' => 'text-center',
                'placeholder' => '',
                'filter' => [
                    'type' => 'input'
                ]
            ],
            'login' => [
                'title' => 'Логин',
                'class' => 'text-center',
                'placeholder' => '',
                'filter' => [
                    'type' => 'input'
                ]
            ],
            'email' => [
                'title' => 'Емейл',
                'class' => 'text-center',
                'placeholder' => '',
                'filter' => [
                    'type' => 'input'
                ]
            ],
            'full_name' => [
                'title' => 'Имя',
                'class' => 'text-center',
                'placeholder' => '',
                'filter' => [
                    'type' => 'input'
                ]
            ],
            'role' => [
                'title' => 'Роль',
                'class' => 'text-center',
                'placeholder' => '',
                'filter' => [
                    'type' => 'select',
                    'data' => UserEntity::ROLES
                ]
            ],
            'created_at' => [
                'title' => 'Зарегистрирован',
                'class' => 'text-center',
                'placeholder' => '',
                'filter' => [
                    'type' => 'input'
                ]
            ],
            'tools' => [
                'name' => 'tools',
                'data' => 'action',
                'orderable' => false,
                'searchable' => false,
                'filter' => false,
                'title' => '',
                'class' => 'text-center',
            ]
        ];
    }
}