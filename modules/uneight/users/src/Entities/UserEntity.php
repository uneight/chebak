<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 27.03.2018
 * Time: 13:31
 */

namespace Uneight\Users\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class UserEntity extends Eloquent
{
    use Notifiable, SoftDeletes;

    const ROLE_ROOT = 1001;
    const ROLE_ADMIN = 1002;
    const ROLE_MODERATOR = 1003;
    const ROLE_USER = 1004;

    const ROLES = [
        self::ROLE_ROOT => 'Владелец',
        self::ROLE_USER => 'Пользователь',
        self::ROLE_MODERATOR => 'Модератор',
        self::ROLE_ADMIN => 'Администратор',
    ];

    protected $table = 'users';
}