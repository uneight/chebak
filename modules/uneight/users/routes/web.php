<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 26.03.2018
 * Time: 12:22
 */

Route::group(['prefix' => 'ap', 'as' => 'ap::', 'namespace' => 'Uneight\\Users\\Http\\Controllers', 'middleware' => ['web']], function() {

    Route::group(['prefix' => 'users', 'as' => 'users:'], function () {
        Route::get('/', ['as' => 'index.get', 'uses' => 'IndexController@indexAction']);
        Route::get('/edit/{id?}', ['as' => 'edit.get', 'uses' => 'IndexController@editAction']);
    });

    Route::group(['prefix' => 'auth'], function () {

    });

});