<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 4/23/18
 * Time: 9:42 PM
 */

namespace Uneight\Categories\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Eloquent;
use Uneight\Posts\Entities\PostEntity;

class CategoryEntity extends Eloquent
{
    use SoftDeletes;

    const CATEGORY_UNDEFINED = 'undefined';

    protected $table = 'categories';

    protected $fillable = [
        'id', 'parent_id', 'type', 'name', 'alias', 'slug', 'active', 'old_id'
    ];

    public function posts ()
    {
        return $this->hasMany(PostEntity::class, 'category_id', 'id');
    }
}