<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 4/23/18
 * Time: 10:15 PM
 */

namespace Uneight\Categories\Http\DataTables;

use Uneight\Categories\Entities\CategoryEntity;
use Yajra\DataTables\Services\DataTable;

class CategoriesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     *
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable ($query)
    {
        return datatables($query)
            ->setRowId('id')
            ->editColumn('type', function ($row) {
                $list = config('uneight-categories.categoriables');
                return $list[$row->type]['name'];
            })
            ->editColumn('active', function ($row) {
                return ($row->active)
                    ? '<span class="badge badge-pill py-1 px-3 success">Да</span>'
                    : '<span class="badge badge-pill py-1 px-3 danger">Нет</span>';
            })
            ->rawColumns(['active', 'action'])
            ->addColumn('action', function ($row) {
                return view('uneight-apanel::dataTables.row-tools', [
                    'items' => [
                        [
                            'type'  => 'link',
                            'name'  => 'Редактировать',
                            'route' => route('ap::categories:edit.get', $row->id),
                            'icon'  => 'fa-pencil',
                        ],
                        [
                            'type'            => 'link',
                            'name'            => 'Удалить',
                            'route'           => '',
                            'icon'            => 'fa-trash',
                            'onClickFunction' => '',
                        ],
                    ],
                ])->render();
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param CategoryEntity $model
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query (CategoryEntity $model)
    {
        return $model->newQuery()->select('id', 'parent_id', 'type', 'name', 'alias', 'slug', 'active', 'sort_id');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html ()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->parameters([
                'render'       => 'custom',
                'order'        => [0, 'asc'],
                'select'       => [
                    'style'    => 'os',
                    'selector' => 'td:first-child',
                ],
                'initComplete' => view('uneight-apanel::dataTables.init-complete'),
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns ()
    {
        return [
            'id'     => [
                'title'       => 'ID',
                'class'       => 'text-center',
                'placeholder' => '',
                'filter'      => [
                    'type' => 'input',
                ],
            ],
            'type'    => [
                'title'       => 'Тип',
                'class'       => 'text-center',
                'placeholder' => '',
                'filter'      => [
                    'type' => 'select',
                    'data' => array_map(function ($item) {
                        return $item['name'];
                    }, config('uneight-categories.categoriables')),
                ],
            ],
            'name'    => [
                'title'       => 'Название',
                'class'       => 'text-center',
                'placeholder' => '',
                'filter'      => [
                    'type' => 'input',
                ],
            ],
            'sort_id' => [
                'title'       => 'Порядок вывода',
                'class'       => 'text-center',
                'placeholder' => '',
                'filter'      => [
                    'type' => 'input',
                ],
            ],
            'active'  => [
                'title'       => 'Активный',
                'class'       => 'text-center',
                'placeholder' => '',
                'filter'      => [
                    'type' => 'select',
                    'data' => [
                        'true'  => 'Да',
                        'false' => 'Нет',
                    ],
                ],
            ],
            'tools'   => [
                'name'       => 'tools',
                'data'       => 'action',
                'orderable'  => false,
                'searchable' => false,
                'filter'     => false,
                'title'      => '',
                'class'      => 'text-center',
            ],
        ];
    }
}