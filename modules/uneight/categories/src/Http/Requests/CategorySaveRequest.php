<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 4/23/18
 * Time: 10:16 PM
 */

namespace Uneight\Categories\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Uneight\Core\Contracts\SaveRequest;

class CategorySaveRequest extends FormRequest implements SaveRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize ()
    {
        return true;
    }

    public function rules ()
    {
        return [
            'form.entity.name'    => 'required|string',
            'form.entity.slug'    => 'required|string',
            'form.entity.sort_id' => 'integer|nullable',
        ];
    }

    public function messages ()
    {
        return parent::messages();
    }
}