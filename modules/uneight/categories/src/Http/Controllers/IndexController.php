<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 23.04.2018
 * Time: 16:01
 */

namespace Uneight\Categories\Http\Controllers;

use Illuminate\Http\Request;
use Uneight\Categories\Entities\CategoryEntity;
use Uneight\Categories\Http\DataTables\CategoriesDataTable;
use Uneight\Categories\Http\Requests\CategorySaveRequest;
use Uneight\Core\Http\Controllers\CRUDController;

class IndexController extends CRUDController
{
    public function __construct()
    {
        $this->_viewPath = 'uneight-categories::';
        $this->entity = new CategoryEntity();

        parent::__construct();
    }

    public function indexAction(CategoriesDataTable $dataTable)
    {
        $this->header->setTitle('Категории')
            ->setDescription('Список категорий')
            ->setButton('Добавить категорию', route('ap::categories:edit.get'))
            ->render();

        return $this->getIndex($dataTable);
    }

    public function editAction(Request $request, $id = null)
    {
        $this->header->setTitle('Категории')
            ->setDescription('Редактировать категорию')
            ->setButton('Назад', route('ap::categories:index.get'), 'success');

        return $this->getEdit($request);
    }

    public function storeAction(CategorySaveRequest $request)
    {
        return $this->postEdit($request);
    }

    public function deleteAction(Request $request)
    {

    }

    public function autocompleteAction(Request $request)
    {
        return $this->entity->where('name', 'LIKE', '%' . $request->input('query') . '%')->get()->map(function ($item) {
            return [
                'id' => $item->id,
                'text' => $item->name
            ];
        });
    }
}