<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 23.04.2018
 * Time: 15:57
 */

namespace Uneight\Categories\Providers;

use Uneight\Core\Providers\ModuleServiceProvider;

class CategoriesServiceProvider extends ModuleServiceProvider
{
    protected $moduleName = 'uneight-categories';
    protected $modulePath = __DIR__;

    public function boot()
    {
        parent::boot();

        $this->publishes([
            $this->modulePath . '/../../config/uneight-categories.php' => config_path('uneight-categories.php'),
        ], 'uneight-categories');
    }

    public function register()
    {
        $this->mergeConfigFrom(
            $this->modulePath . '/../../config/uneight-categories.php', 'uneight-categories'
        );
    }
}