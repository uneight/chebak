<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 4/23/18
 * Time: 9:41 PM
 */

namespace Uneight\Categories\Traits;

use Uneight\Categories\Entities\CategoryEntity;

trait HasCategory
{
    public function category()
    {
        return $this->belongsTo(CategoryEntity::class, 'category_id', 'id');
    }
}