<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 4/23/18
 * Time: 9:57 PM
 */

return [

    'categoriables' => [
        'uneight-posts' => [
            'name' => 'Публикации',
            'class' => \Uneight\Posts\Entities\PostEntity::class,
        ],
    ],

];