<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 19.04.2018
 * Time: 16:33
 */

namespace Uneight\Core\Providers;

use Illuminate\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    protected $moduleName = '';
    protected $modulePath;

    public function boot()
    {
        $this->loadRoutesFrom($this->modulePath . '/../../routes/web.php');
        $this->loadViewsFrom($this->modulePath . '/../../resources/views', $this->moduleName);
        $this->loadTranslationsFrom($this->modulePath . '/../../resources/lang', $this->moduleName);
        $this->loadMigrationsFrom($this->modulePath . '/../../database/migrations');
    }
}