<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 26.03.2018
 * Time: 16:23
 */

namespace Uneight\Core\Providers;

use Illuminate\Support\ServiceProvider;
use Uneight\Core\Commands\MakeMigration;

class CoreServiceProvider extends ServiceProvider
{
    protected $commands = [
        MakeMigration::class
    ];

    public function boot()
    {
        $this->commands($this->commands);
    }

    public function provides()
    {
        return $this->commands;
    }
}