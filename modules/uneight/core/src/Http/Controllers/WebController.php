<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 26.03.2018
 * Time: 13:35
 */

namespace Uneight\Core\Http\Controllers;

use Uneight\Apanel\View\Header;
use Uneight\Core\Abstracts\Controller;

/**
 * Class WebController
 * @package Uneight\Core\Http\Controllers
 */
class WebController extends Controller
{
    /**
     * @var Header
     */
    protected $header;
    protected $_viewPath = '';

    /**
     * WebController constructor.
     */
    public function __construct()
    {
        $this->header = app()->make(Header::class);
        $this->middleware('auth.admin');
    }

    public function view($view, $data = [])
    {
        $view = (!empty($this->_viewPath)) ? $this->_viewPath . '.' . $view : $view;

        return view($view, [
            'header' => $this->header->render()
        ], $data);
    }
}