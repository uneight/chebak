<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 17.04.2018
 * Time: 12:42
 */

namespace Uneight\Core\Http\Controllers;

use Uneight\Core\Contracts\SaveRequest;
use Illuminate\Http\Request;
use Exception;

class CRUDController extends WebController
{
    /**
     * @var null
     */
    public $view = null;

    protected $entity;
    protected $relations = [];

    /**
     * @param $dataTable
     *
     * @return mixed
     * @throws \Exception
     */
    public function getIndex($dataTable)
    {
        $view = (!empty($this->_viewPath)) ? $this->_viewPath . 'index' : 'index';

        return $dataTable->render($view, ['header' => $this->header->render()]);
    }

    public function getEdit(Request $request)
    {
        $id = $request->route()->parameter('id', 0);
        $data = $this->entity->with($this->relations)->find($id);

        return $this->view('edit', ['entity' => $data]);
    }

    public function postEdit(SaveRequest $request)
    {
        $row = $request->input('form.row');
        $entity = $request->input('form.entity');
        $relations = $request->input('form.relations');

        if (!empty($row['id'])) {
            $this->entity = $this->entity->find($row['id']);
        }

        foreach ($entity as $prop => $value) {
            $this->entity->{$prop} = $value;
        }

        $this->entity->save();

        return redirect(route(str_replace(['edit.post', 'edit.get'], 'index.get', $request->route()->getName())))
            ->with('notify', [
                'type' => 'success',
                'text' => 'Успешно сохранено',
                'header' => 'Сохранено'
            ]);
    }

    public function deleteRemove(Request $request, $id)
    {
        $this->entity = $this->entity->find($id);

        if (empty($this->entity)) {
            return redirect(route(str_replace(['remove.delete'], 'index.get', $request->route()->getName())))
                ->with('notify', [
                    'type' => 'danger',
                    'text' => 'Сущности с идентификатором ' . $id . ' не существует.',
                    'header' => 'Ошибка'
                ]);
        }

        try {
            $this->entity->delete();
        } catch (Exception $exception) {
            return redirect(route(str_replace(['remove.delete'], 'index.get', $request->route()->getName())))
                ->with('notify', [
                    'type' => 'danger',
                    'text' => $exception->getMessage(),
                    'header' => 'Ошибка'
                ]);
        }

        return redirect(route(str_replace(['remove.delete'], 'index.get', $request->route()->getName())))
            ->with('notify', [
                'type' => 'success',
                'text' => 'Успешно удалено',
                'header' => 'Удалено'
            ]);
    }

    public function getView (Request $request, $data = [])
    {
        $id = $request->route()->parameter('id', 0);
        $entity = $this->entity->with($this->relations)->find($id);

        return $this->view('view', compact('entity', 'data'));
    }
}