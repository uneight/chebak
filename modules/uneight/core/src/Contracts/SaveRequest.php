<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 17.04.2018
 * Time: 12:36
 */

namespace Uneight\Core\Contracts;

/**
 * Interface SaveRequest
 * @package Uneight\Core\Contracts
 */
interface SaveRequest
{
    /**
     * Validation rules.
     *
     * @return mixed
     */
    public function rules();

    /**
     * Validation messages.
     *
     * @return mixed
     */
    public function messages();
}