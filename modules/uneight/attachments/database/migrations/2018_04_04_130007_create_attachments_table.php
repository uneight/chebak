<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->text('value');
            $table->boolean('active');
            $table->timestamps();
        });

        Schema::create('attachmentables', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('attachment_id');
            $table->foreign('attachment_id')->references('id')->on('attachments')->onDelete('cascade');
            $table->integer('attachmentable_id');
            $table->string('attachmentable_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attachmentables');
        Schema::dropIfExists('attachments');
    }
}
