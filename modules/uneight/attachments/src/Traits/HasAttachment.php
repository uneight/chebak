<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 23.04.2018
 * Time: 16:21
 */

namespace Uneight\Attachments\Traits;

use Uneight\Attachments\Entities\AttachmentEntity;

trait HasAttachment
{
    public function attachments ()
    {
        return $this->morphToMany(AttachmentEntity::class, 'attachmentable', 'attachmentables',
            'attachmentable_id', 'attachment_id');
    }
}