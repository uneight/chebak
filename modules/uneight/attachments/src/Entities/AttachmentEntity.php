<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 4/27/18
 * Time: 9:29 PM
 */

namespace Uneight\Attachments\Entities;

use Eloquent;
use Uneight\Posts\Entities\PostEntity;

class AttachmentEntity extends Eloquent
{
    protected $table = 'attachments';

    protected $fillable = [
        'type', 'value', 'active',
    ];

    public function post ()
    {
        return $this->morphedByMany(PostEntity::class, 'attachmentable', 'attachmentables', 'attachment_id');
    }
}