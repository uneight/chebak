<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 23.04.2018
 * Time: 15:57
 */

namespace Uneight\Attachments\Providers;

use Uneight\Core\Providers\ModuleServiceProvider;

class AttachmentsServiceProvider extends ModuleServiceProvider
{
    protected $moduleName = 'uneight-attachments';
    protected $modulePath = __DIR__;
}