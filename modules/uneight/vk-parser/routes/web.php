<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 6/10/18
 * Time: 8:05 PM
 */

Route::group(['prefix' => 'ap', 'as' => 'ap::', 'namespace' => 'Uneight\\VkParser\\Http\\Controllers', 'middleware' => ['web', 'auth.admin']], function () {

    Route::group(['prefix' => 'vk', 'as' => 'vk:'], function () {

        Route::group(['prefix' => 'groups', 'as' => 'groups:'], function () {
            Route::get('/', ['as' => 'index.get', 'uses' => 'GroupsController@indexAction']);
            Route::get('/edit/{id?}', ['as' => 'edit.get', 'uses' => 'GroupsController@editAction']);
            Route::get('/view/{id?}', ['as' => 'view.get', 'uses' => 'GroupsController@viewAction']);
            Route::post('/edit/{id?}', ['as' => 'edit.post', 'uses' => 'GroupsController@storeAction']);
            Route::delete('/delete/{id}', ['as' => 'remove.delete', 'uses' => 'GroupsController@removeAction']);
            Route::post('/autocomplete', ['as' => 'autocomplete.post', 'uses' => 'GroupsController@autocompleteAction']);
        });

        Route::post('/parse/{id?}', ['as' => 'start-parse.get', 'uses' => 'GroupsController@startParse']);
    });

});