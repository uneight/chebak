@extends('uneight-apanel::layouts.master-layout')

@section('page.content')

    <div>
        <div class="box">
            <div class="box-header light lt">
                <h4>Основные</h4>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="form-group col-6">
                        {{ Form::label('form[entity][vk_id]', 'ID в ВКонтакте:') }}
                        {{ $entity->vk_id }}
                    </div>
                    <div class="form-group col-6">
                        {{ Form::label('form[entity][name]', 'Название в ВКонтакте:') }}
                        {{ $entity->name }}
                    </div>
                    <div class="form-group col-6">
                        {{ Form::label('form[entity][slug]', 'Ссылка в ВКонтакте:') }}
                        https://vk.com/{{ $entity->slug }}
                    </div>
                </div>
            </div>
        </div>

        <div class="box">
            <div class="box-header light lt">
                <h4>Владелец</h4>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="form-group col-6">
                        {{ Form::label('form[[entity]full_name]', 'Имя Фамилия:') }}
                        {{ $entity->full_name }}
                    </div>
                    <div class="form-group col-6">
                        {{ Form::label('form[entity][email]', 'Email:') }}
                        {{ $entity->email }}
                    </div>
                </div>

            </div>
        </div>

        <div class="box">
            <div class="box-header light lt">
                <h4>Водоемы</h4>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-12"><strong>Кол-во привязаных водоемов:</strong> {{ count($entity->waters) }}</div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <strong>Сприсок привязаных водоемов:</strong>

                        @foreach($entity->waters as $water)
                            <a href="{{ route('ap::geo:waters:view.get', $water->id) }}" class="btn btn-sm btn-outline btn-rounded b-primary mb-1">{{ $water->name }}</a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 text-right">
                {!! Form::button('Спарсить', ['class' => 'btn btn-fw submit white', 'type' => 'submit']) !!}
            </div>
        </div>

    </div>

@endsection
