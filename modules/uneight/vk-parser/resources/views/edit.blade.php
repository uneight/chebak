@extends('uneight-apanel::layouts.master-layout')

@section('page.content')

    {{ Form::open(['route' => Request::route()->getName(), 'action' => 'POST']) }}
    {{ Form::hidden('form[row][id]', (!empty($entity->id) ? $entity->id : null)) }}

    <div class="box">
        <div class="box-header light lt">
            <h4>Основные</h4>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="form-group col-6">
                    {{ Form::label('form[entity][vk_id]', 'ID в ВКонтакте:') }}
                    {{ Form::text('form[entity][vk_id]', (!empty($entity->vk_id) ? $entity->vk_id : null), ['class' => 'form-control']) }}
                </div>
                <div class="form-group col-6">
                    {{ Form::label('form[entity][name]', 'Название в ВКонтакте:') }}
                    {{ Form::text('form[entity][name]', (!empty($entity->name) ? $entity->name : null), ['class' => 'form-control']) }}
                </div>
                <div class="form-group col-6">
                    {{ Form::label('form[entity][slug]', 'Ссылка в ВКонтакте:') }}
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                https://vk.com/
                            </span>
                        </div>
                        {{ Form::text('form[entity][slug]', (!empty($entity->slug) ? $entity->slug : null), ['class' => 'form-control']) }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="box">
        <div class="box-header light lt">
            <h4>Владелец</h4>
        </div>
        <div class="box-body">


            <div class="row">
                <div class="form-group col-6">
                    {{ Form::label('form[[entity]full_name]', 'Имя Фамилия:') }}
                    {{ Form::text('form[entity][full_name]', (!empty($entity->full_name) ? $entity->full_name : null), ['class' => 'form-control']) }}
                </div>
                <div class="form-group col-6">
                    {{ Form::label('form[entity][email]', 'Email:') }}
                    {{ Form::text('form[entity][email]', (!empty($entity->email) ? $entity->email : null), ['class' => 'form-control']) }}
                </div>
            </div>

        </div>
    </div>

    <div class="box">
        <div class="box-header light lt">
            <h4>Гео данные</h4>
        </div>
        <div class="box-body">

            <div class="row">
                <div class="form-group col-6">
                    {{ Form::label('form[entity][country]', 'Страна:') }}
                    <uneight-autocomplete id="country"
                                          name="form[entity][country_id]"
                                          :selected='{!! json_encode((!empty($entity->country_id) ? ['id' => $entity->country_id, 'text' => $entity->country->name] : [])) !!}'
                                          placeholder=""
                                          route="{!! route('ap::geo:countries:autocomplete.post') !!}">
                    </uneight-autocomplete>
                </div>
                <div class="form-group col-6">
                    {{ Form::label('form[entity][region_id]', 'Регион:') }}
                    <uneight-autocomplete id="region"
                                          name="form[entity][region_id]"
                                          :selected='{!! json_encode((!empty($entity->region_id) ? ['id' => $entity->region_id, 'text' => $entity->region->name] : [])) !!}'
                                          placeholder=""
                                          route="{!! route('ap::geo:regions:autocomplete.post') !!}">
                    </uneight-autocomplete>
                </div>
                <div class="form-group col-6">
                    {{ Form::label('form[entity][district_id]', 'Район:') }}
                    <uneight-autocomplete id="district"
                                          name="form[entity][district_id]"
                                          :selected='{!! json_encode((!empty($entity->district_id) ? ['id' => $entity->district_id, 'text' => $entity->district->name] : [])) !!}'
                                          placeholder=""
                                          route="{!! route('ap::geo:districts:autocomplete.post') !!}">
                    </uneight-autocomplete>
                </div>
                <div class="form-group col-6">
                    {{ Form::label('form[entity][locality_id]', 'Населенный пункт:') }}
                    <uneight-autocomplete id="locality"
                                          name="form[entity][locality_id]"
                                          :selected='{!! json_encode((!empty($entity->locality_id) ? ['id' => $entity->locality_id, 'text' => $entity->locality->name] : [])) !!}'
                                          placeholder=""
                                          route="{!! route('ap::geo:localities:autocomplete.post') !!}">
                    </uneight-autocomplete>
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-12 text-right">
            {!! Form::button('Сохранить', ['class' => 'btn btn-fw submit white', 'type' => 'submit']) !!}
        </div>
    </div>

    {{ Form::close() }}

@endsection
