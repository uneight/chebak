<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVkGroupWatersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        Schema::create('vk_group_waters', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('group_id')->nullable()->unsigned();
            $table->foreign('group_id')->references('id')->on('vk_groups')->onDelete('cascade');

            $table->integer('water_id')->nullable()->unsigned();
            $table->foreign('water_id')->references('id')->on('geo_water')->onDelete('cascade');

            $table->index(['group_id', 'water_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        Schema::dropIfExists('vk_group_waters');
    }
}
