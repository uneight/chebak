<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVkGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        Schema::create('vk_groups', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('vk_id');
            $table->string('name');
            $table->string('slug');

            $table->string('full_name')->nullable();
            $table->string('email')->nullable();

            $table->integer('country_id')->nullable()->unsigned();
            $table->foreign('country_id')->references('id')->on('geo_countries')->onDelete('set null');

            $table->integer('region_id')->nullable()->unsigned();
            $table->foreign('region_id')->references('id')->on('geo_regions')->onDelete('set null');

            $table->integer('district_id')->nullable()->unsigned();
            $table->foreign('district_id')->references('id')->on('geo_districts')->onDelete('set null');

            $table->integer('locality_id')->nullable()->unsigned();
            $table->foreign('locality_id')->references('id')->on('geo_localities')->onDelete('set null');

            $table->integer('last_post_id')->default(0);
            $table->integer('last_count')->default(0);
            $table->timestamp('last_parsed_at')->nullable();

            $table->boolean('active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        Schema::dropIfExists('vk_groups');
    }
}
