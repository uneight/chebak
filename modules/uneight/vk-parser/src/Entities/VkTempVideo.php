<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 6/19/18
 * Time: 7:44 PM
 */

namespace Uneight\VkParser\Entities;

use Eloquent;

class VkTempVideo extends Eloquent
{

    protected $table = 'vk_post_video_temp';

    protected $fillable = [
        'group_id', 'video_id', 'access_token', 'platform', 'data', 'proceed',
    ];

}