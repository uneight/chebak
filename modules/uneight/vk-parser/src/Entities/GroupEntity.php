<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 6/10/18
 * Time: 8:08 PM
 */

namespace Uneight\VkParser\Entities;

use App\Entities\Geo\CountryEntity;
use App\Entities\Geo\DistrictEntity;
use App\Entities\Geo\LocalityEntity;
use App\Entities\Geo\RegionEntity;
use App\Entities\Geo\WaterEntity;
use Eloquent;

class GroupEntity extends Eloquent
{
    protected $table = 'vk_groups';

    protected $fillable = [
        'vk_id', 'name', 'slug', 'full_name', 'email', 'region_id', 'district_id', 'locality_id', 'last_count', 'last_parsed_at', 'last_post_id',
    ];

    public function locality()
    {
        return $this->belongsTo(LocalityEntity::class, 'locality_id', 'id');
    }

    public function district()
    {
        return $this->belongsTo(DistrictEntity::class, 'district_id', 'id');
    }

    public function region()
    {
        return $this->belongsTo(RegionEntity::class, 'region_id', 'id');
    }

    public function country()
    {
        return $this->belongsTo(CountryEntity::class, 'country_id', 'id');
    }

    public function waters()
    {
        return $this->belongsToMany(WaterEntity::class, 'vk_group_waters', 'group_id', 'water_id');
    }
}