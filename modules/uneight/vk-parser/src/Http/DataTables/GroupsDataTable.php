<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 6/10/18
 * Time: 8:22 PM
 */

namespace Uneight\VkParser\Http\DataTables;

use Uneight\VkParser\Entities\GroupEntity;
use Yajra\DataTables\Services\DataTable;

class GroupsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->setRowId('id')
            ->addColumn('action', function($row) {
                return view('uneight-apanel::dataTables.row-tools', [
                    'items' => [
                        [
                            'type'  => 'link',
                            'name'  => 'Спарсить',
                            'route' => route('ap::vk:groups:view.get', $row->id),
                            'icon'  => 'fa-exchange',
                        ],
                        [
                            'type'  => 'link',
                            'name'  => 'Просмотр',
                            'route' => route('ap::vk:groups:view.get', $row->id),
                            'icon'  => 'fa-eye'
                        ],
                        [
                            'type'  => 'link',
                            'name'  => 'Редактировать',
                            'route' => route('ap::vk:groups:edit.get', $row->id),
                            'icon'  => 'fa-pencil'
                        ],
                        [
                            'type'            => 'link',
                            'name'            => 'Удалить',
                            'route'           => '',
                            'icon'            => 'fa-trash',
                            'onClickFunction' => ''
                        ],
                    ]
                ])->render();
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param GroupEntity $model
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query (GroupEntity $model)
    {
        return $model->newQuery()->select('id', 'name', 'slug', 'last_count', 'last_parsed_at', 'active', 'created_at');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->parameters([
                'render'       => 'custom',
                'order'        => [0, 'asc'],
                'select'       => [
                    'style'    => 'os',
                    'selector' => 'td:first-child',
                ],
                'initComplete' => view('uneight-apanel::dataTables.init-complete'),
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id'             => [
                'title'       => 'ID',
                'class'       => 'text-center',
                'placeholder' => '',
                'filter'      => [
                    'type' => 'input'
                ]
            ],
            'name'           => [
                'title'       => 'Название',
                'class'       => 'text-center',
                'placeholder' => '',
                'filter'      => [
                    'type' => 'input'
                ]
            ],
            'slug'           => [
                'title'       => 'Адрес',
                'class'       => 'text-center',
                'placeholder' => '',
                'filter'      => [
                    'type' => 'input',
                ],
            ],
            'last_count'     => [
                'title'       => 'Посл. кол-во постов',
                'class'       => 'text-center',
                'placeholder' => '',
                'filter'      => [
                    'type' => 'input',
                ],
            ],
            'last_parsed_at' => [
                'title'       => 'Посл. дата парсинга',
                'class'       => 'text-center',
                'placeholder' => '',
                'filter'      => [
                    'type' => 'input',
                ],
            ],
            'created_at'     => [
                'title'       => 'Добавлена',
                'class'       => 'text-center',
                'placeholder' => '',
                'filter'      => [
                    'type' => 'input'
                ]
            ],
            'active'         => [
                'title'       => 'Активный',
                'class'       => 'text-center',
                'placeholder' => '',
                'filter'      => [
                    'type' => 'select',
                    'data' => [
                        'true'  => 'Да',
                        'false' => 'Нет',
                    ],
                ],
            ],
            'tools'          => [
                'name'       => 'tools',
                'data'       => 'action',
                'orderable'  => false,
                'searchable' => false,
                'filter'     => false,
                'title'      => '',
                'class'      => 'text-center',
            ]
        ];
    }
}