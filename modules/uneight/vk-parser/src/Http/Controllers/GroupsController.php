<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 6/10/18
 * Time: 8:07 PM
 */

namespace Uneight\VkParser\Http\Controllers;

use Illuminate\Http\Request;
use Uneight\Core\Http\Controllers\CRUDController;
use Uneight\VkParser\Entities\GroupEntity;
use Uneight\VkParser\Http\DataTables\GroupsDataTable;
use Uneight\VkParser\Http\Requests\GroupSaveRequest;
use Artisan;

class GroupsController extends CRUDController
{
    public function __construct ()
    {
        $this->_viewPath = 'uneight-vk-parser::';
        $this->entity = new GroupEntity();
        $this->relations = ['waters'];

        parent::__construct();
    }

    /**
     * @param GroupsDataTable $dataTable
     *
     * @return mixed
     * @throws \Exception
     */
    public function indexAction (GroupsDataTable $dataTable)
    {
        $this->header->setTitle('Группы')
            ->setDescription('Список групп')
            ->setButton('Добавить группу', route('ap::vk:groups:edit.get'))
            ->render();

        return $this->getIndex($dataTable);
    }

    public function editAction (Request $request, $id = null)
    {
        $this->header->setTitle('Группы')
            ->setDescription('Редактировать группу')
            ->setButton('Назад', route('ap::vk:groups:index.get'), 'success');

        return $this->getEdit($request);
    }

    public function storeAction (GroupSaveRequest $request)
    {
        return $this->postEdit($request);
    }

    public function viewAction (Request $request)
    {

        return $this->getView($request);
    }

    public function deleteAction (Request $request)
    {

    }

    public function autocompleteAction (Request $request)
    {
        return $this->entity->where('name', 'LIKE', '%' . $request->input('query') . '%')->get()->map(function ($item) {
            return [
                'id'   => $item->id,
                'text' => $item->name,
            ];
        });
    }

    public function startParse (Request $request, $id = null)
    {
        Artisan::call('parse:vk');
    }
}