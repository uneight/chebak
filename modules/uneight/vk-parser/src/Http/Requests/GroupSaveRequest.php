<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 6/10/18
 * Time: 8:23 PM
 */

namespace Uneight\VkParser\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Uneight\Core\Contracts\SaveRequest;
use Illuminate\Validation\Rule;
use Uneight\VkParser\Entities\GroupEntity;

class GroupSaveRequest extends FormRequest implements SaveRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize ()
    {
        return true;
    }

    public function rules ()
    {
        $id = !(empty($this->request->get('form.row.id'))) ? $this->request->get('form.row.id') : null;

        return [
            'form.entity.vk_id'          => $this->prepareUnique($id, [
                'required',
                'numeric',
            ]),
            'form.entity.name'           => 'required|string',
            'form.entity.slug'           => $this->prepareUnique($id, [
                'required',
                'string',
            ]),
            'form.entity.full_name'      => 'required|string',
            'form.entity.email'          => 'required|email',
            'form.entity.country_id'     => 'sometimes|numeric|nullable',
            'form.entity.region_id'      => 'sometimes|numeric|nullable',
            'form.entity.district_id'    => 'sometimes|numeric|nullable',
            'form.entity.locality_id'    => 'sometimes|numeric|nullable',
            'form.entity.last_post_id'   => 'numeric|nullable',
            'form.entity.last_count'     => 'numeric|nullable',
            'form.entity.last_parsed_at' => 'timestamp|nullable',
        ];
    }

    public function messages ()
    {
        return parent::messages();
    }

    protected function prepareUnique ($id = null, $array = [])
    {
        if ($id) {
            array_push($array, Rule::unique('vk_groups')->ignore($id));
        }

        return $array;
    }
}