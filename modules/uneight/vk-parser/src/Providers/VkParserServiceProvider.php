<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 6/10/18
 * Time: 8:00 PM
 */

namespace Uneight\VkParser\Providers;

use Uneight\Core\Providers\ModuleServiceProvider;

class VkParserServiceProvider extends ModuleServiceProvider
{
    protected $moduleName = 'uneight-vk-parser';
    protected $modulePath = __DIR__;

    public function boot ()
    {
        parent::boot();

        $this->publishes([
            $this->modulePath . '/../../config/uneight-vk-parser.php' => config_path('uneight-vk-parser.php'),
        ], 'uneight-vk-parser');
    }

    public function register ()
    {
        $this->mergeConfigFrom(
            $this->modulePath . '/../../config/uneight-vk-parser.php', 'uneight-vk-parser'
        );
    }
}