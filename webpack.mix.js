let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
var dist_folder = 'public/dist/uneight/apanel';

mix.js('resources/assets/js/app.js', 'public/dist/js')
   .sass('resources/assets/sass/app.scss', 'public/dist/css');

var dist_folder = 'public/dist/uneight/apanel';
var apanel_folder = 'modules/uneight/apanel/resources/assets';

mix.js(apanel_folder + '/js/app.js', dist_folder + '/js')
    .sass(apanel_folder + '/sass/app.scss', dist_folder + '/css')
    .copyDirectory(apanel_folder + '/images', dist_folder + '/images')
    .copyDirectory(apanel_folder + '/js/libs', dist_folder + '/js/libs')
    .copyDirectory('node_modules/font-awesome/fonts', dist_folder + '/fonts/font-awesome');