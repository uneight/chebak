<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 8/26/18
 * Time: 7:07 PM
 */

namespace App\Http\Controllers;

use Uneight\Posts\Entities\PostEntity;
use Illuminate\Http\Request;
use Uneight\VkParser\Entities\GroupEntity;
use App\Entities\Geo\WaterEntity;

class CommunitiesController extends _Controller
{

    public function getListAction ()
    {
        $communities = GroupEntity::all();

        return view('communities.list', compact('communities'));
    }

    public function getPostsAction (Request $request, $id)
    {
        $community = GroupEntity::find($id);

        $waters = WaterEntity::whereHas('groups', function ($query) use ($id) {
            return $query->where('group_id', $id);
        })->withCount('posts')->orderBy('posts_count', 'desc')->where('name', '<>', '')->limit(15)->get();

        $posts = PostEntity::with(['attachments', 'user', 'group', 'water'])
            ->where('group_id', $id)->orderBy('posts.created_at', 'DESC')->paginate(20);

        return view('communities.posts', compact('community', 'posts', 'waters'));
    }

}