<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 05.04.2018
 * Time: 12:07
 */

namespace App\Http\Controllers;

use App\Entities\Geo\RegionEntity;
use App\Jobs\Groups\SearchGroupWatersJob;
use App\Jobs\Waters\ProceedWaterJob;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Uneight\VkParser\Entities\GroupEntity;

class OsmController extends _Controller
{
    public function lakes(Request $request)
    {
        $regions = (!empty($request->get('id')))
            ? RegionEntity::with('districts')->where('id', $request->get('id'))->get()
            : RegionEntity::with('districts')->get();

        foreach ($regions as $region) {
            foreach ($region->districts as $district) {
                ProceedWaterJob::dispatch($district, 'lakes');
            }
        }
    }

    public function rivers(Request $request)
    {
        $regions = (!empty($request->get('id')))
            ? RegionEntity::where('id', $request->get('id'))->get()
            : RegionEntity::all();

        foreach ($regions as $region) {
            foreach ($region->districts as $district) {
                ProceedWaterJob::dispatch($district, 'rivers');
            }
        }

    }

    public function findGroupWaters (Request $request)
    {
        $type = $request->get('type', null);
        $group = $request->get('group_id', null);

        if (!$type) {
            return new JsonResponse(['message' => 'Type is required'], 404);
        }

        if (!$group) {
            return new JsonResponse(['message' => 'Group id is required'], 404);
        }

        $group = GroupEntity::find($request->get('group_id'));
        SearchGroupWatersJob::dispatch($group, $type);
    }
}