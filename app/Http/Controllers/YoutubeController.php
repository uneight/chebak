<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 10/23/18
 * Time: 10:45 AM
 */

namespace App\Http\Controllers;

use App\Entities\Posts\PostEntity;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Uneight\Attachments\Entities\AttachmentEntity;
use Uneight\Categories\Entities\CategoryEntity;
use Youtube;

class YoutubeController extends _Controller
{
    protected $selfChannel = 'UCLipDk5b73CGutsBdQdClRw';

    public function index()
    {
        $list = [];
        $client = new Client();
        $response = $client->get('https://www.youtube.com/channel/' . $this->selfChannel . '/channels');

        preg_match_all('/href=["\']\/channel\/?([^"\'>]+)["\']?/', $response->getBody()->getContents(), $matches);
        $channels = array_unique($matches[1]);

        $key = array_search($this->selfChannel, $channels);
        $offset = array_search($key, array_keys($channels));
        $channels = array_slice($channels, $offset + 1);

        unset($channels[0]);

        foreach ($channels as $channel) {
            $params = [
                'type'           => 'video',
                'channelId'      => $channel,
                'part'           => implode(', ', ['id', 'snippet']),
                'maxResults'     => 50,
                'publishedAfter' => Carbon::now()->subDay()->toRfc3339String(),
            ];

            $videos = Youtube::searchAdvanced($params);
            $list = $videos ? array_merge($list, $videos) : $list;
        }

        foreach ($list as $item) {
            $post = PostEntity::where('old_id', $this->item->id)->where('receive_type', 2)->first();

            if (empty($post)) {
                $post = PostEntity::create([
                    'old_id'       => $item->id->videoId,
                    'receive_type' => 2,
                    'title'        => $item->snipet->title,
                    'content'      => $item->snipet->description,
                    'old_data'     => json_encode($item),
                    'created_at'   => Carbon::createFro('d/m/Y H:i:s', $item->snipet->publishedAt),
                    'updated_at'   => Carbon::createFromFormat('d/m/Y H:i:s', $item->snipet->publishedAt),
                    'active'       => true,
                ]);

                $category = CategoryEntity::where('slug', CategoryEntity::CATEGORY_UNDEFINED)->first();

                $post->category()->attach($category);

                $attachment = AttachmentEntity::create([
                    'type'   => 'youtube',
                    'value'  => $item->id->videoId,
                    'active' => true,
                ]);

                $post->attachments()->save($attachment);
            }
        }
    }
}