<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 10/30/18
 * Time: 1:46 PM
 */

namespace App\Http\Controllers\Auth;

use App\Entities\UserEntity;
use App\Http\Controllers\_Controller;
use Illuminate\Http\Request;
use Socialite;
use Auth;

class SocialAuthController extends _Controller
{
    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Return a callback method from facebook api.
     *
     * @param Request $request
     * @param $provider
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     */
    public function callback(Request $request, $provider)
    {
        $method = 'proceed' . mb_convert_case($provider, MB_CASE_TITLE, "UTF-8") . 'Callback';

        if (method_exists($this, $method)) {
            return $this->$method($request);
        }

        return response()->view('errors.custom', [
            'title' => 'Ошибка авторизации!',
            'message' => 'Что-то пошло не так, попробуйте позже или обратитесь в поддержку'
        ], 500);
    }

    protected function proceedFacebookCallback(Request $request)
    {
        $account = Socialite::driver('facebook')->user();
        $user = UserEntity::firstOrCreate([
            'social_provider' => 'facebook',
            'social_provider_id' => $account->id,
        ], [
            'email' => $account->email,
            'full_name' => $account->name,
            'avatar' => $account->avatar,
        ]);

        Auth::login($user, true);

        return redirect()->route('index.get');
    }

    protected function proceedVkontakteCallback(Request $request)
    {
        $account = Socialite::driver('facebook')->user();
        $user = UserEntity::firstOrCreate([
            'social_provider' => 'facebook',
            'social_provider_id' => $account->id,
        ], [
            'email' => $account->email,
            'full_name' => $account->name,
            'avatar' => $account->avatar,
        ]);

        Auth::login($user, true);

        return redirect()->route('index.get');
    }
}