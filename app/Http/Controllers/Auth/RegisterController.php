<?php

namespace App\Http\Controllers\Auth;

use App\Entities\UserEntity;
use App\Http\Controllers\_Controller;
use App\Rules\EmailOrPhoneValidationRule;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Validation\Rule;

class RegisterController extends _Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $id = null;
        $login = trim($data['form']['login'], '+');
        $user = UserEntity::where('email', $login)->orWhere('phone', $login)->first();

        if (empty($user)) {
            $uniqueRule = is_numeric($login) ? Rule::unique('users', 'phone') : Rule::unique('users', 'email');
        } else {
            $id = $user->id;
            $uniqueRule = is_numeric($login) ? Rule::unique('users', 'phone')->ignore($id) : Rule::unique('user', 'email')->ignore($id);
        }

        return Validator::make($data, [
            'form.login'     => [
                'required',
                'string',
                $uniqueRule,
                new EmailOrPhoneValidationRule($id),
            ],
            'form.full_name' => 'required|string|max:255',
            'form.password'  => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\Entities\UserEntity
     */
    protected function create(array $data)
    {
        if (is_numeric($data['form']['login'])) {
            $data = [
                'phone'     => $data['form']['login'],
                'email'     => null,
                'full_name' => $data['form']['full_name'],
                'role'      => UserEntity::ROLE_USER,
                'password'  => Hash::make($data['form']['password']),
            ];
        } else {
            $data = [
                'phone'     => null,
                'email'     => $data['form']['login'],
                'full_name' => $data['form']['full_name'],
                'role'      => UserEntity::ROLE_USER,
                'password'  => Hash::make($data['form']['password']),
            ];
        }

        return UserEntity::create($data);
    }
}
