<?php

namespace App\Http\Controllers\Auth;

use App\Entities\UserEntity;
use App\Http\Controllers\_Controller;
use App\Http\Requests\AuthFormRequest;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Uneight\Categories\Entities\CategoryEntity;
use Auth;

class LoginController extends _Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        $categories = CategoryEntity::where('type', 'uneight-posts')->withCount('posts')->get();

        return view('auth.login', compact($categories));
    }

    /**
     * Handle a login request to the application.
     *
     * @param  AuthFormRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(AuthFormRequest $request)
    {
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if (Auth::attempt(['email' => $request->form['login'], 'password' => $request->form['password']])) {
            return $this->sendLoginResponse($request);
        } else if (Auth::attempt(['phone' => $request->form['login'], 'password' => $request->form['password']])) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }
}
