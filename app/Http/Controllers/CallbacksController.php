<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 6/10/18
 * Time: 1:48 PM
 */

namespace App\Http\Controllers;

use App\Entities\Posts\PostEntity;
use Illuminate\Http\Request;
use Carbon\Carbon;

class CallbacksController extends _Controller
{
    public function vk (Request $request)
    {
        $data = $request->all();
        $object = $data['object'];

        if ($data['type'] == 'wall_post_new') {
            $post = PostEntity::where('old_id', $object['owner_id'] . '_' . $object['id'])->first();

            if (empty($post)) {
                $opst = PostEntity::create([
                    'title'         => '',
                    'content'       => $object['text'],
                    'old_id'        => $object['owner_id'] . '_' . $object['id'],
                    'region_id'     => null,
                    'district_id'   => null,
                    'locality_id'   => null,
                    'water_id'      => null,
                    'dot_id'        => null,
                    'external_user' => $object['from_id'],
                    'external_link' => 'wall' . $object['owner_id'] . '_' . $object['id'],
                    'external_type' => 'post',
                    'old_data'      => json_encode($data),
                    'rate'          => 0,
                    'created_at'    => $object['date'],
                    'updated_at'    => $object['date'],
                    'active'        => true,
                ]);

                foreach ($object['attachments'] as $attachment) {
                    switch ($attachment['type']) {
                        case "photo":
                            break;
                        case "video":
                            break;
                    }

                    $post->attachments()->save($attachment);
                }
            }
        }

        return '';
    }
}