<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 4/27/18
 * Time: 10:26 PM
 */

namespace App\Http\Controllers;

use App\Http\Requests\PostSaveRequest;
use Illuminate\Http\Request;
use Auth;
use Uneight\Posts\Entities\PostEntity;

/**
 * Class PostsController
 * @package App\Http\Controllers
 */
class PostsController
{
    /**
     * @param Request $request
     * @param         $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view (Request $request, $id)
    {
        $post = PostEntity::with(['attachments', 'user', 'group', 'water'])
            ->find($id);

        return view('posts.view', compact('post'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        if (!Auth::user()) {
            return redirect()->route('login')->with('message', 'Для добавления публикации необходимо авторизироваться');
        }

        return view('posts.create');
    }

    /**
     * @param PostSaveRequest $request
     */
    public function save(PostSaveRequest $request)
    {

    }
}