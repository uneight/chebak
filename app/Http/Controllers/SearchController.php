<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 23.08.2018
 * Time: 10:29
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Uneight\Posts\Entities\PostEntity;

class SearchController extends _Controller
{
    public function searchAction(Request $request)
    {
        $posts = PostEntity::with(['attachments', 'user', 'group', 'water'])
            ->orderBy('posts.created_at', 'DESC');

        switch ($request->get('type')) {
            case 'waters':
                $posts->whereHas('water', function ($query) use ($request) {
                    $query->where('name', 'ILIKE', '%' . $request->get('q') . '%');
                });
                break;
            case 'posts':
                $posts->whereRaw("plainto_tsquery('russian', '" . $request->get('q') . "') @@ to_tsvector('russian', content)");
                break;
            case 'all':
                $posts->whereHas('water', function ($query) use ($request) {
                    $query->where('name', 'ILIKE', '%' . $request->get('q') . '%');
                })->orWhereRaw("plainto_tsquery('russian', '" . $request->get('q') . "') @@ to_tsvector('russian', content)");
                break;
        }

        $posts = $posts->paginate(20)->appends(request()->except('page'));

        return view('search', compact('posts'));
    }
}