<?php

namespace App\Http\Controllers;

use App\Entities\Geo\DistrictEntity;
use App\Entities\Geo\RegionEntity;
use App\Entities\Geo\WaterEntity;
use Illuminate\Http\Request;
use Uneight\Categories\Entities\CategoryEntity;
use Uneight\Posts\Entities\PostEntity;
use Uneight\Users\Entities\UserEntity;
use Youtube;

class IndexController extends _Controller
{
    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndexAction(Request $request)
    {
        $categories = CategoryEntity::where('type', 'uneight-posts')->withCount('posts')->get();
        $posts = PostEntity::with(['attachments', 'user', 'group', 'water'])->whereHas('category', function ($query) {
            $query->where('slug', '<>', 'undefined');
        })
            ->orderBy('posts.created_at', 'DESC');

        if (!is_null($request->route('slug'))) {
            $posts->whereHas('category', function ($query) use ($request) {
                return $query->where('slug', $request->route('slug'));
            });
        }

        $postCount = PostEntity::whereHas('category', function ($query) {
            $query->where('slug', '<>', 'undefined');
            })->count('id');
        $waterCount = WaterEntity::has('posts')->count('id');
        $userCount = UserEntity::count('id');

        $posts = $posts->paginate(20);

        return view('index', compact('posts', 'categories', 'waters', 'postCount', 'waterCount', 'userCount'));
    }

    /**
     * @param Request $request
     * @param null $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getRegionsAction(Request $request)
    {
        $regions = RegionEntity::orderBy('name', 'ASC')->get();
        $waters = WaterEntity::has('posts')->withCount('posts')->orderBy('posts_count', 'desc')->limit(15)->get();

        return view('regions', compact('waters', 'regions'));
    }

    /**
     * @param Request $request
     * @param integer $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getRegionAction(Request $request, $id)
    {
        $region = RegionEntity::with('districts')->find($id);
        $postCount = PostEntity::where('region_id', $id)->whereHas('category', function ($query) {
            $query->where('slug', '<>', 'undefined');
        })->count();
        $topWaters = WaterEntity::whereHas('regions', function ($query) use ($id) {
            return $query->where('region_id', $id);
        })->withCount('posts')->orderBy('posts_count', 'desc')->where('name', '<>', '')->get();
        $waters = WaterEntity::whereHas('regions', function ($query) use ($id) {
            return $query->where('region_id', $id);
        })->count('id');

        $posts = PostEntity::with(['attachments', 'user', 'group', 'water'])->where('region_id', $id)->whereHas('category', function ($query) {
            $query->where('slug', '<>', 'undefined');
        })->orderBy('posts.created_at', 'DESC')->paginate(20);

        return view('region', compact('topWaters', 'postCount', 'waters', 'posts', 'region'));
    }

    /**
     * @param Request $request
     * @param integer $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDistrictAction(Request $request, $id)
    {
        $district = DistrictEntity::with('waters')->find($id);
        $postCount = PostEntity::where('district_id', $id)->whereHas('category', function ($query) {
            $query->where('slug', '<>', 'undefined');
        })->count();

        $topWaters = WaterEntity::whereHas('districts', function ($query) use ($id) {
            return $query->where('district_id', $id);
        })->withCount('posts')->orderBy('posts_count', 'desc')->where('name', '<>', '')->get();

        $waters = WaterEntity::whereHas('districts', function ($query) use ($id) {
            return $query->where('district_id', $id);
        })->count('id');

        $posts = PostEntity::with(['attachments', 'user', 'group', 'water'])->where('district_id', $id)->whereHas('category', function ($query) {
                $query->where('slug', '<>', 'undefined');
            })->orderBy('posts.created_at', 'DESC')->paginate(20);

        return view('district', compact('topWaters', 'postCount', 'waters', 'posts', 'district'));
    }

    /**
     * @param Request $request
     * @param integer $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getRegionWaterAction(Request $request, $regionID, $waterID)
    {
        $region = RegionEntity::with('districts')->find($regionID);
        $regions = RegionEntity::whereHas('waters', function ($query) use ($waterID) {
            return $query->where('water_id', $waterID);
        })->get();

        $postCount = PostEntity::where('region_id', $regionID)->whereHas('category', function ($query) {
            $query->where('slug', '<>', 'undefined');
        })->count();
        $water = WaterEntity::find($waterID);

        $node = (!empty(json_decode($water->nodes)[0])) ? json_decode($water->nodes)[0] : null;

        $posts = PostEntity::with(['attachments', 'user', 'group', 'water'])->whereHas('category', function ($query) {
            $query->where('slug', '<>', 'undefined');
        })->where('region_id', $regionID)->where('water_id', $waterID)->orderBy('posts.created_at', 'DESC')->paginate(20);

        return view('waters', compact('postCount', 'water', 'posts', 'region', 'regions', 'node'));
    }

    /**
     * @param Request $request
     * @param integer $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDistrictWaterAction(Request $request, $districtID, $waterID)
    {
        $district = DistrictEntity::with('waters')->find($districtID);
        $region = RegionEntity::find($district->region_id);

        $postCount = PostEntity::where('district_id', $districtID)->whereHas('category', function ($query) {
            $query->where('slug', '<>', 'undefined');
        })->count();
        $topWaters = WaterEntity::whereHas('districts', function ($query) use ($districtID) {
            return $query->where('district_id', $districtID);
        })->withCount('posts')->orderBy('posts_count', 'desc')->where('name', '<>', '')->get();

        $waters = WaterEntity::whereHas('districts', function ($query) use ($districtID) {
            return $query->where('district_id', $districtID);
        })->count('id');

        $posts = PostEntity::with(['attachments', 'user', 'group', 'water'])->whereHas('category', function ($query) {
            $query->where('slug', '<>', 'undefined');
        })->where('district_id', $districtID)->orderBy('posts.created_at', 'DESC')->paginate(20);

        return view('region', compact('topWaters', 'postCount', 'waters', 'posts', 'district', 'region'));
    }

    /**
     * @param Request $request
     * @param null $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getVodoemAction(Request $request, $id)
    {
        $waters = WaterEntity::select('id', 'name')->limit(50)->get();
        $water = WaterEntity::find($id);
        $node = (!empty(json_decode($water->nodes)[0])) ? json_decode($water->nodes)[0] : null;

        $regions = RegionEntity::whereHas('waters', function ($query) use ($id) {
            return $query->where('water_id', $id);
        })->get();
        $posts = PostEntity::with(['attachments', 'user', 'group', 'water'])->whereHas('category', function ($query) {
            $query->where('slug', '<>', 'undefined');
        })->where('water_id', $id)->orderBy('posts.created_at', 'DESC')->paginate(20);

        return view('waters', compact('posts', 'water', 'regions', 'node'));
    }
}
