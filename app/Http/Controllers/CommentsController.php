<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 10.05.2018
 * Time: 14:55
 */

namespace App\Http\Controllers;

use App\Http\Requests\CommentSaveRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Uneight\Attachments\Entities\AttachmentEntity;
use Uneight\Comments\Entities\CommentEntity;
use Uneight\Posts\Entities\PostEntity;
use Storage;
use Auth;

class CommentsController extends _Controller
{
    public function create(CommentSaveRequest $request)
    {
        $post = PostEntity::find($request->input('post'));

        $comment = CommentEntity::create([
            'content' => $request->input('text'),
            'user_id' => Auth::user()->id,
        ]);

        $attachments = $request->input('attachments');

        if (!empty($attachments)) {
            foreach ($attachments as $type => $list) {
                foreach ($list as $item) {
                    if ($type == 'videos') {
                        $attachment = AttachmentEntity::create([
                            'type'   => 'youtube',
                            'value'  => $item,
                            'active' => true,
                        ]);
                    }

                    $comment->attachments()->save($attachment);
                }
            }
        }

        if (!empty($request->files)) {
            foreach ($request->files as $list) {
                foreach ($list as $attachments) {
                    foreach ($attachments as $item) {
                        $path = '/uploads/posts/' . $post->id;
                        $file = $item->move(storage_path('app/public' . $path), md5($item->getClientOriginalName()));
                        $attachment = AttachmentEntity::create([
                            'type'   => 'image',
                            'value'  => '/storage'. $path . '/' . md5($item->getClientOriginalName()),
                            'active' => true,
                        ]);

                        $comment->attachments()->save($attachment);
                    }
                }
            }
        }

        $post->comments()->save($comment);

        return new JsonResponse();
    }

    public function delete(Request $request)
    {

    }

    public function load(Request $request)
    {
        $comments = CommentEntity::with(['user', 'attachments'])->whereHas('post', function ($query) use ($request) {
            return $query->where('commentable_id', $request->get('post'))
                ->where('commentable_type', PostEntity::class);
        })->get();

        return new JsonResponse($comments);
    }
}