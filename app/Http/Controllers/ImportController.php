<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 14.03.2018
 * Time: 09:56
 */

namespace App\Http\Controllers;

use App\Entities\Geo\CountryEntity;
use App\Entities\Geo\DistrictEntity;
use App\Entities\Geo\RegionEntity;
use App\Entities\UserEntity;
use App\Jobs\Posts\ProceedPostComments;
use App\Jobs\Posts\ProceedPostsItemJob;
use App\Jobs\Posts\ProceedPostsListJob;
use App\Jobs\ProceedCountryJob;
use App\Jobs\ProceedDistrictLocalitiesJob;
use App\Jobs\ProceedRegionDistrictsJob;
use App\Jobs\ProceedRegionJob;
use App\Jobs\Users\ProceedUserJob;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Storage;

class ImportController extends _Controller
{
    /**
     * @var Client
     */
    protected $client;
    protected $api_key = '907c51415d075543e30e073c9a264d57';

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'https://htmlweb.ru',
        ]);
    }

    public function getImportCountries(Request $request, $id)
    {
        $response = $this->client->get('/geo/api.php', [
            'query' => [
                'country' => $id,
                'api_key' => $this->api_key,
                'info'    => true,
                'json'    => true,
            ],
        ])->getBody()->getContents();

        ProceedCountryJob::dispatch(json_decode($response));
    }

    public function getImportRegions(Request $request)
    {
        $region_id = $request->get('region_id');

        $countries = CountryEntity::all();
        $regions = RegionEntity::select(['api_id_htmlweb'])->pluck('api_id_htmlweb')->toArray();

        if (!$region_id) {
            $countries->each(function ($country) use ($regions) {
                $response = $this->client->get('/geo/api.php', [
                    'query' => [
                        'country' => $country->api_id_htmlweb,
                        'api_key' => $this->api_key,
                        'json'    => true,
                        'perpage' => '99999',
                    ],
                ])->getBody()->getContents();

                foreach ((array)json_decode($response) as $region) {
                    if (isset($region->id) && !in_array($region->id, $regions)) {
                        ProceedRegionJob::dispatch($region->id, $country->id);
                    }
                }

            });
        } else {
            ProceedRegionJob::dispatch($region_id, $request->get('country_id'));
        }
    }

    public function getImportDistricts(Request $request)
    {
        $region_id = $request->get('region_id');

        if ($region_id) {
            $region = RegionEntity::where('api_id_htmlweb', $region_id)->first();
            ProceedRegionDistrictsJob::dispatch($region);
        } else {
            $regions = RegionEntity::all();

            foreach ($regions as $region) {
                ProceedRegionDistrictsJob::dispatch($region);
            }
        }
    }

    public function getImportLocalities(Request $request)
    {
        if ($request->get('region_id')) {
            $region = RegionEntity::where('api_id_htmlweb', $request->get('region_id'))->first();
            $districts = DistrictEntity::where('region_id', $region->id)->get();
        } else {
            $districts = DistrictEntity::all();
        }

        foreach ($districts as $district) {
            ProceedDistrictLocalitiesJob::dispatch($district);
        }
    }

    public function getImportPosts()
    {
        $oldPosts = json_decode(Storage::get('/old_data/Post.json'));

        foreach ($oldPosts->RECORDS as $row) {
            ProceedPostsItemJob::dispatch($row);
        }
    }

    public function getImportComments()
    {
        $oldComments = json_decode(Storage::get('/old_data/Comments.json'));

        foreach ($oldComments->RECORDS as $comment) {
            ProceedPostComments::dispatch($comment);
        }
    }

    public function getImportUsers()
    {
        $users = json_decode(Storage::get('/old_data/PassportProfile.json'));

        foreach ($users->RECORDS as $item) {
            $user = UserEntity::where('old_id', $item->id)->first();

            if (empty($user) && !empty($item->iname) && !empty($item->iemail)) {
                ProceedUserJob::dispatch($item);
            }
        }
    }
}