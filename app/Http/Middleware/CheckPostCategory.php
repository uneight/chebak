<?php

namespace App\Http\Middleware;

use Closure;
use Uneight\Categories\Entities\CategoryEntity;

class CheckPostCategory
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle ($request, Closure $next)
    {
        $slug = $request->route('slug');
        $categories = CategoryEntity::where('type', 'uneight-posts')->withCount('posts')->get()->keyBy('slug')->keys();

        if (is_null($slug) || in_array($slug, $categories->toArray()) || $slug == 'ap') {
            return $next($request);
        } else {
            return redirect()->route('index.get');
        }
    }
}
