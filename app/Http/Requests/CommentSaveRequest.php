<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 16.05.2018
 * Time: 10:18
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class CommentSaveRequest extends FormRequest
{
    public function authorize()
    {
        return Auth::check();
    }

    public function rules()
    {
        return [
            'post'                 => 'required|integer|exists:posts,id',
            'text'                 => 'required|string|min:10',
            'attachments.images.*' => 'file|mimes:jpeg,bmp,png,gif',
            'attachmwnts.videos.*' => 'string|regex:/^(http://|https://)(?:www.)?(?:youtube.com|youtu.be)/(?:watch?(?=.*v=([\w-]+))(?:\S+)?|([\w-]+))$/'
        ];
    }
}