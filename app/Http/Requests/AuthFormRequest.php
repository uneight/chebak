<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 11/12/18
 * Time: 11:02 AM
 */

namespace App\Http\Requests;


use App\Entities\UserEntity;
use App\Rules\EmailOrPhoneValidationRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AuthFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $passwordValidation = ($this->route()->getName() === 'login.post') ? 'required|string' : 'required|string|confirmed';
        $form = $this->request->get('form');
        $login = trim($form['login'], '+');
        $user = UserEntity::where('email', $login)->orWhere('phone', $login)->first();
        $uniqueRule = is_numeric($login) ? Rule::unique('users', 'phone')->ignore($user->id) : Rule::unique('users', 'email')->ignore($user->id);

        return [
            'form.login'    => [
                'required',
                'string',
                $uniqueRule,
                new EmailOrPhoneValidationRule($user->id),
            ],
            'form.password' => $passwordValidation,
        ];
    }

    public function messages()
    {
        return parent::messages();
    }
}