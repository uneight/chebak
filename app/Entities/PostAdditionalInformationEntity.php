<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 27.04.2018
 * Time: 14:12
 */

namespace App\Entities;

use Eloquent;

class PostAdditionalInformationEntity extends Eloquent
{
    protected $table = 'post_additional_information';

    protected $fillable = [
        'post_id', 'region_id', 'district_id', 'locality_id', 'water_id', 'dot_id', 'external_user', 'external_link', 'external_type', 'old_data',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'old_data' => 'array',
    ];
}