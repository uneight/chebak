<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 5/6/18
 * Time: 8:31 PM
 */

namespace App\Entities\Posts;

use Illuminate\Database\Eloquent\SoftDeletes;
use Uneight\Attachments\Traits\HasAttachment;
use Uneight\Categories\Traits\HasCategory;

class PostEntity extends \Uneight\Posts\Entities\PostEntity
{
    use SoftDeletes, HasCategory, HasAttachment;
}