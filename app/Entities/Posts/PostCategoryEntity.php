<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 5/6/18
 * Time: 8:32 PM
 */

namespace App\Entities\Posts;

use Uneight\Categories\Entities\CategoryEntity;

class PostCategoryEntity extends CategoryEntity
{
    public function posts ()
    {
        return $this->morphedByMany(\Uneight\Posts\Entities\PostEntity::class, 'categoriable', 'categoriables',
            'categoriable_id', 'category_id');
    }
}