<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 19.03.2018
 * Time: 10:02
 */

namespace App\Entities\Geo;

use Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class CountryEntity
 * @package App\Entities\Geo
 *
 * @property $regions
 */
class CountryEntity extends Eloquent
{
    use SoftDeletes;

    protected $table = 'geo_countries';

    protected $fillable = [
        'name', 'full_name', 'iso_code', 'iso_name', 'source', 'active', 'api_id_htmlweb', 'api_id_kladr'
    ];

    public function regions()
    {
        return $this->hasMany(RegionEntity::class, 'country_id', 'id')->orderBy('name');
    }
}