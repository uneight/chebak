<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 19.03.2018
 * Time: 10:08
 */

namespace App\Entities\Geo;

use Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class LocalityEntity extends Eloquent
{
    use SoftDeletes;

    protected $table = 'geo_localities';

    protected $fillable = [
        'district_id', 'region_id', 'type', 'name', 'latitude', 'longitude', 'source', 'active', 'api_id_htmlweb', 'api_id_kladr'
    ];

    public function district()
    {
        return $this->belongsTo(DistrictEntity::class, 'id', 'district_id');
    }

    public function region ()
    {
        return $this->belongsTo(RegionEntity::class, 'id', 'region_id');
    }
}