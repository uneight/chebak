<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 19.03.2018
 * Time: 10:07
 */

namespace App\Entities\Geo;

use Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;
use Uneight\Posts\Entities\PostEntity;

class DistrictEntity extends Eloquent
{
    use SoftDeletes;

    protected $table = 'geo_districts';

    protected $fillable = [
        'type', 'region_id', 'name', 'source', 'active', 'api_id_htmlweb', 'api_id_kladr', 'wikidata'
    ];

    public function region()
    {
        return $this->belongsTo(RegionEntity::class, 'region_id', 'id')->orderBy('name');
    }

    public function waters()
    {
        return $this->belongsToMany(WaterEntity::class, 'geo_district_waters', 'district_id', 'water_id');
    }

    public function posts()
    {
        return $this->hasMany(PostEntity::class, 'district_id', 'id');
    }
}