<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 19.03.2018
 * Time: 10:03
 */

namespace App\Entities\Geo;

use Illuminate\Database\Eloquent\SoftDeletes;
use Uneight\Posts\Entities\PostEntity;
use Eloquent;

/**
 * Class RegionEntity
 * @package App\Entities\Geo
 *
 * @property $country
 */
class RegionEntity extends Eloquent
{
    use SoftDeletes;

    const TYPES = [
        'Область' => 'Область',
        'Край' => 'Край',
        'Республика' => 'Республика',
        'Автономная область' => 'Автономная область',
    ];

    protected $table = 'geo_regions';

    protected $fillable = [
        'type', 'country_id', 'name', 'auto_number_codes', 'iso', 'timezone', 'source', 'active', 'api_id_htmlweb', 'api_id_kladr'
    ];

    public function country()
    {
        return $this->belongsTo(CountryEntity::class, 'country_id', 'id');
    }

    public function waters()
    {
        return $this->belongsToMany(WaterEntity::class, 'geo_region_waters', 'region_id', 'water_id');
    }

    public function districts()
    {
        return $this->hasMany(DistrictEntity::class, 'region_id', 'id')->orderBy('name');
    }

    public function localities ()
    {
        return $this->hasMany(LocalityEntity::class, 'region_id', 'id')->orderBy('name');
    }

    public function posts()
    {
        return $this->hasMany(PostEntity::class, 'region_id', 'id');
    }
}