<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 4/9/18
 * Time: 9:02 PM
 */

namespace App\Entities\Geo;

use Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;
use Uneight\Posts\Entities\PostEntity;
use Uneight\VkParser\Entities\GroupEntity;

class WaterEntity extends Eloquent
{
    use SoftDeletes;

    protected $table = 'geo_water';

    protected $fillable = [
        'api_id', 'api_type', 'type', 'name', 'alias', 'nodes', 'active', 'waters_founded'
    ];

    public function regions ()
    {
        return $this->belongsToMany(RegionEntity::class, 'geo_region_waters', 'water_id', 'region_id');
    }

    public function districts ()
    {
        return $this->belongsToMany(DistrictEntity::class, 'geo_district_waters', 'water_id', 'district_id');
    }

    public function posts ()
    {
        return $this->hasMany(PostEntity::class, 'water_id', 'id');
    }

    public function groups ()
    {
        return $this->belongsToMany(GroupEntity::class, 'vk_group_waters', 'water_id', 'group_id');
    }
}