<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 6/10/18
 * Time: 5:17 PM
 */

namespace App\Entities;

use Eloquent;

class VkParserHistoryEntity extends Eloquent
{
    protected $table = 'vk_parser_history';

    protected $fillable = [
        'group_id', 'last_count',
    ];
}