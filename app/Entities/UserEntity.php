<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserEntity extends Authenticatable
{
    use Notifiable, SoftDeletes;

    const ROLE_ROOT = 1001;
    const ROLE_ADMIN = 1002;
    const ROLE_MODERATOR = 1003;
    const ROLE_USER = 1004;

    const ROLES = [
        self::ROLE_ROOT => 'Владелец',
        self::ROLE_USER => 'Пользователь',
        self::ROLE_MODERATOR => 'Модератор',
        self::ROLE_ADMIN => 'Администратор',
    ];

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phone', 'email', 'full_name', 'password', 'role', 'referrer_id', 'avatar', 'old_id', 'social_provider', 'social_provider_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function accounts()
    {
        return $this->hasMany(UserSocialAccountsEntity::class);
    }
}
