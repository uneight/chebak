<?php

namespace App\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserSocialAccountsEntity extends Eloquent
{
    use SoftDeletes;

    protected $table = 'users_social_accounts';

    protected $fillable = [
        'user_id', 'provider_name', 'provider_id'
    ];

    public function user()
    {
        return $this->belongsTo(UserEntity::class);
    }
}
