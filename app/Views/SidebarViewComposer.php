<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 24.07.2018
 * Time: 10:58
 */

namespace App\Views;

use Uneight\Categories\Entities\CategoryEntity;
use Illuminate\View\View;

class SidebarViewComposer
{
    protected $categories;

    /**
     * Create a new profile composer.
     *
     * @return void
     */
    public function __construct(CategoryEntity $categoryEntity)
    {
        $this->categories = $categoryEntity->where('type', 'uneight-posts')->where('slug', '<>', 'undefined')->withCount('posts')->get();
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('categories', $this->categories);
    }
}