<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 24.07.2018
 * Time: 11:01
 */

namespace App\Providers;

use App\Views\SidebarViewComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(
            '*', SidebarViewComposer::class
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}