<?php

namespace App\Jobs;

use App\Entities\Geo\DistrictEntity;
use App\Entities\Geo\LocalityEntity;
use App\Entities\Geo\RegionEntity;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProceedDistrictLocalitiesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $district;
    protected $api_key = '907c51415d075543e30e073c9a264d57';

    /**
     * Create a new job instance.
     *
     * @param $district
     *
     * @return void
     */
    public function __construct(DistrictEntity $district)
    {
        $this->district = $district;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $client = new Client();
        $localies = LocalityEntity::select(['api_id_htmlweb'])->pluck('api_id_htmlweb')->toArray();

        $response = $client->get('https://htmlweb.ru/geo/api.php', [
            'query' => [
                'rajon_city' => $this->district->api_id_htmlweb,
                'api_key' => $this->api_key,
                'json' => true,
                'limit' => 9999999
            ]
        ])->getBody()->getContents();

        foreach ((array)json_decode($response) as $locality) {
            if (isset($locality->id) && !in_array($locality->id, $localies)) {
                ProceedLocalityJob::dispatch($locality, $this->district);
            }
        }
    }
}
