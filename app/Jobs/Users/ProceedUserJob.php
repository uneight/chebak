<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 10.05.2018
 * Time: 11:32
 */

namespace App\Jobs\Users;

use App\Entities\PostAdditionalInformationEntity;
use App\Entities\UserEntity;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Uneight\Attachments\Entities\AttachmentEntity;
use Uneight\Categories\Entities\CategoryEntity;
use Uneight\Posts\Entities\PostEntity;
use DB;

class ProceedUserJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $item;

    /**
     * Create a new job instance.
     *
     * @param $item mixed Response from API
     *
     * @return void
     */
    public function __construct($item = null)
    {
        $this->item = $item;
    }

    /**
     * @throws \Throwable
     */
    public function handle()
    {
        UserEntity::create([
            'old_id' => $this->item->id,
            'login' => $this->item->iemail,
            'email' => $this->item->iemail,
            'full_name' => $this->item->iname,
            'role' => UserEntity::ROLE_USER,
            'referrer_id' => null,
            'avatar' => '',
        ]);
    }
}