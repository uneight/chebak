<?php

namespace App\Jobs;

use App\Entities\Geo\DistrictEntity;
use App\Entities\Geo\LocalityEntity;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProceedLocalityJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $locality;
    protected $district;

    /**
     * Create a new job instance.
     *
     * @param $locality
     * @param $district
     *
     * @return void
     */
    public function __construct($locality, DistrictEntity $district)
    {
        $this->locality = $locality;
        $this->district = $district;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $client = new Client();
        $params = explode(' ', $this->locality->name);
        $type = null;
        $kladr = null;

        $response = json_decode($client->get('http://kladr-api.ru/api.php', [
            'query' => [
                'query'       => $params[0],
                'contentType' => 'city',
                'withParent'  => 1,
            ],
        ])->getBody()->getContents());

        foreach ($response->result as $result) {
            foreach ($result->parents as $parent) {
                if (($parent->id == $this->district->api_id_kladr && $parent->contentType == 'district') ||
                    ($parent->id == $this->district->region->api_id_kladr && $parent->contentType == 'region')) {
                    $type = $result->type;
                    $kladr = $result->id;
                }
            }
        }

        $locality = LocalityEntity::where('api_id_htmlweb', $this->locality->id)->first();

        if (!$locality) {
            LocalityEntity::create([
                'api_id_htmlweb' => $this->locality->id,
                'api_id_kladr'   => $kladr,
                'district_id'    => $this->district->id,
                'region_id'      => $this->district->region_id,
                'type'           => $type,
                'name'           => $this->locality->name,
                'latitude'       => $this->locality->latitude,
                'longitude'      => $this->locality->longitude,
                'source'         => json_encode(['count' => count($response->result), 'response' => [
                    'htmlweb' => $this->locality,
                    'kladr'   => $response->result,
                ]]),
                'active'         => true,
            ]);
        }
    }
}
