<?php

namespace App\Jobs;

use App\Entities\Geo\CountryEntity;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProceedCountryJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $source;

    /**
     * Create a new job instance.
     *
     * @param $source mixed Response from API
     *
     * @return void
     */
    public function __construct($source = null)
    {
        $this->source = $source;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        CountryEntity::create([
            'name' => $this->source->country->name,
            'api_id_htmlweb' => $this->source->country->id,
            'full_name' => $this->source->fullname,
            'iso_code' => $this->source->iso,
            'iso_name' => $this->source->country->id,
            'source' => json_encode($this->source),
            'active' => true,
        ]);
    }
}
