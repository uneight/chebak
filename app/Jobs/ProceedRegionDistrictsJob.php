<?php

namespace App\Jobs;

use App\Entities\Geo\DistrictEntity;
use App\Entities\Geo\RegionEntity;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProceedRegionDistrictsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $region;
    protected $api_key = '907c51415d075543e30e073c9a264d57';

    /**
     * Create a new job instance.
     *
     * @param $region
     *
     * @return void
     */
    public function __construct(RegionEntity $region)
    {
        $this->region = $region;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $districts = DistrictEntity::select(['api_id_htmlweb'])->pluck('api_id_htmlweb')->toArray();

        $client = new Client([
            'base_uri' => 'https://htmlweb.ru'
        ]);

        $response = json_decode($client->get('/geo/api.php', [
            'query' => [
                'area_rajon' => $this->region->api_id_htmlweb,
                'api_key' => $this->api_key,
                'json' => true,
                'limit' => 9999999
            ]
        ])->getBody()->getContents());

        foreach ((array)$response as $district) {
            if (isset($district->id) && !in_array($district->id, $districts)) {
                ProceedDistrictJob::dispatch($district, $this->region);
            }
        }
    }
}
