<?php

namespace App\Jobs;

use App\Entities\Geo\DistrictEntity;
use App\Entities\Geo\RegionEntity;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProceedDistrictJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $disctrict;
    protected $region;

    /**
     * Create a new job instance.
     *
     * @param $district
     * @param $region
     *
     * @return void
     */
    public function __construct($district, RegionEntity $region)
    {
        $this->region = $region;
        $this->disctrict = $district;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $client = new Client();
        $response = new \stdClass();
        $response->result = [];

        $type = 'Городской округ';
        $kladr = null;

        if (!str_contains($this->disctrict->name, 'городской округ')) {
            $params = explode(' ', $this->disctrict->name);

            $response = json_decode($client->get('http://kladr-api.ru/api.php', [
                'query' => [
                    'query' => $params[0],
                    'contentType' => 'district',
                    'withParent' => 1
                ]
            ])->getBody()->getContents());

            if (!empty($response->result)) {
                foreach ($response->result as $result) {
                    foreach ($result->parents as $parent) {

                        if ($parent->contentType == 'region') {
                            $type = $result->type;
                            $kladr = $result->id;
                        }
                    }
                }
            }
        }

        $districts = DistrictEntity::where('api_id_kladr', $kladr)->get();

        if (empty($districts)) {
            DistrictEntity::create([
                'api_id_htmlweb' => $this->disctrict->id,
                'api_id_kladr'   => $kladr,
                'region_id'      => $this->region->id,
                'type'           => $type,
                'name'           => $this->disctrict->name,
                'source'         => json_encode([
                    'count' => count($response->result), 'response' => [
                        'htmlweb' => $this->disctrict,
                        'kladr'   => $response->result,
                    ],
                ]),
                'active'         => true,
            ]);
        }
    }
}
