<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 4/27/18
 * Time: 9:18 PM
 */

namespace App\Jobs\Posts;

use App\Entities\PostAdditionalInformationEntity;
use App\Entities\UserEntity;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Uneight\Attachments\Entities\AttachmentEntity;
use Uneight\Categories\Entities\CategoryEntity;
use Uneight\Posts\Entities\PostEntity;
use DB;

class ProceedPostsItemJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $item;

    /**
     * Create a new job instance.
     *
     * @param $item mixed Response from API
     *
     * @return void
     */
    public function __construct($item = null)
    {
        $this->item = $item;
    }

    /**
     * @throws \Throwable
     */
    public function handle()
    {
        $post = PostEntity::where('old_id', $this->item->id)->first();

        if (empty($post)) {
            DB::transaction(function () {
                $user = UserEntity::where('old_id', $this->item->iprofile)->first();

                $post = PostEntity::create([
                    'user_id'       => (!empty($user->id)) ? $user->id : null,
                    'old_id'        => $this->item->id,
                    'title'         => $this->item->iname,
                    'content'       => $this->item->itext,
                    'region_id'     => null,
                    'district_id'   => null,
                    'locality_id'   => null,
                    'water_id'      => null,
                    'dot_id'        => null,
                    'external_user' => $this->item->ivkprofile,
                    'external_link' => null,
                    'external_type' => null,
                    'old_data'      => json_encode($this->item),
                    'rate'          => $this->item->irate,
                    'created_at'    => Carbon::createFromFormat('d/m/Y H:i:s', $this->item->icdate),
                    'updated_at'    => Carbon::createFromFormat('d/m/Y H:i:s', $this->item->icdate),
                    'active'        => true,
                    'source'        => 'vk',
                ]);

                $category = CategoryEntity::where('old_id', $this->item->itype)->first();

                $post->category()->attach($category);

                $attaches = json_decode($this->item->iattach);

                if (!empty($attaches)) {
                    foreach ($attaches as $attach) {
                        $attachment = AttachmentEntity::create([
                            'type'   => $attach->type,
                            'value'  => ($attach->type == 'image') ? 'http://chebak.ru' . $attach->path : $attach->path,
                            'active' => true,
                        ]);

                        $post->attachments()->save($attachment);
                    }
                }
            });
        }
    }
}