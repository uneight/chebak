<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 5/7/18
 * Time: 9:07 PM
 */

namespace App\Jobs\Posts;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Uneight\Attachments\Entities\AttachmentEntity;
use Uneight\Comments\Entities\CommentEntity;
use Uneight\Posts\Entities\PostEntity;
use Uneight\Users\Entities\UserEntity;

class ProceedPostComments implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $comment;

    /**
     * ProceedPostComments constructor.
     *
     * @param null $comment
     */
    public function __construct($comment = null)
    {
        $this->comment = $comment;
    }

    /**
     * @throws \Throwable
     */
    public function handle()
    {
        $post = PostEntity::where('old_id', $this->comment->ipost)->first();
        $user = UserEntity::where('old_id', $this->comment->iprofile)->first();

        if ($post) {
            $comment = CommentEntity::create([
                'user_id'    => (!empty($user)) ? $user->id : null,
                'content'    => $this->comment->itext,
                'rate'       => 0,
                'active'     => true,
                'created_at'    => Carbon::createFromFormat('d/m/Y H:i:s', $this->comment->icdate),
                'updated_at'    => Carbon::createFromFormat('d/m/Y H:i:s', $this->comment->icdate),
            ]);

            $attaches = json_decode($this->comment->iattach);

            if (!empty($attaches)) {
                foreach ($attaches as $attach) {
                    $attachment = AttachmentEntity::create([
                        'type'   => $attach->type,
                        'value'  => ($attach->type == 'image') ? 'http://chebak.ru' . $attach->path : $attach->path,
                        'active' => true,
                    ]);

                    $comment->attachments()->save($attachment);
                }
            }

            $post->comments()->save($comment);
        }
    }
}