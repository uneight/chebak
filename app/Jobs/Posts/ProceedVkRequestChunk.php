<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 8/9/18
 * Time: 6:32 PM
 */

namespace App\Jobs\Posts;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Uneight\VkParser\Entities\GroupEntity;
use VK\Client\VKApiClient;

class ProceedVkRequestChunk implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $group;
    public $count;
    public $offset;

    /**
     * Create a new job instance.
     *
     * @param $response array
     * @param $group    GroupEntity
     *
     * @return void
     */
    public function __construct (GroupEntity $group, $count, $offset)
    {
        $this->group = $group;
        $this->count = $count;
        $this->offset = $offset;
    }

    /**
     * @throws \Throwable
     */
    public function handle ()
    {
        $vk = new VKApiClient();

        $response = $vk->wall()->get('b98a2e811c5e2a635246aabf93ac9bfee0a9acbc94308cea36e01a20f319ad1081c280ee8fb4ee192ed5f', [
            'owner_id' => $this->group->vk_id,
            'count'    => $this->count,
            'extended' => 1,
            'offset'   => $this->offset,
        ]);

        ProceedVkResponse::dispatch($response, $this->group);
    }
}
