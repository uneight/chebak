<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 6/19/18
 * Time: 9:01 PM
 */

namespace App\Jobs\Posts;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Uneight\VkParser\Entities\GroupEntity;

class ProceedVkResponse implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $response;
    public $group;

    /**
     * Create a new job instance.
     *
     * @param $response array
     * @param $group    GroupEntity
     *
     * @return void
     */
    public function __construct ($response = [], GroupEntity $group)
    {
        $this->response = $response;
        $this->group = $group;
    }

    /**
     * @throws \Throwable
     */
    public function handle ()
    {
        foreach ($this->response['items'] as $item) {
            ProceedVkParsedPost::dispatch($item, $this->group);
        }
    }
}
