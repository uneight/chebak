<?php

namespace App\Jobs\Posts;

use App\Entities\Geo\WaterEntity;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Uneight\Attachments\Entities\AttachmentEntity;
use Uneight\Categories\Entities\CategoryEntity;
use Uneight\Posts\Entities\PostEntity;
use Carbon\Carbon;
use Storage;
use DB;
use Uneight\Settings\Entities\SettingEntity;
use Uneight\VkParser\Entities\GroupEntity;
use VK\Client\VKApiClient;
use Uneight\Geo\Entities\DotEntity;

class ProceedVkParsedPost implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $post;
    public $group;

    /**
     * Create a new job instance.
     *
     * @param $post  array
     * @param $group GroupEntity
     *
     * @return void
     */
    public function __construct ($post = [], GroupEntity $group)
    {
        $this->post = $post;
        $this->group = $group;
    }

    /**
     * @throws \Throwable
     */
    public function handle ()
    {
        $post = PostEntity::where('old_id', $this->post['id'])->first();

        if (empty($post) && ((!empty($this->post['text']) || !empty($this->post['attachments'])) || !empty($this->post['copy_history']))) {
            DB::transaction(function () {

                if (!empty($this->post['copy_history'])) {
                    $this->post = $this->post['copy_history'][0];
                }

                if (!empty($this->post['id'])) {

                    $category = CategoryEntity::where('slug', 'post')->first();

                    $post = PostEntity::create([
                        'user_id'           => null,
                        'old_id'            => $this->post['id'],
                        'category_id'       => $category->id,
                        'group_id'          => $this->group->id,
                        'receive_type'      => 0,
                        'title'             => '',
                        'content'           => $this->post['text'],
                        'region_id'         => $this->group->region_id,
                        'district_id'       => $this->group->district_id,
                        'locality_id'       => $this->group->locality_id,
                        'water_id'          => null,
                        'dot_id'            => null,
                        'external_from_id'  => $this->post['from_id'],
                        'external_owner_id' => $this->post['owner_id'],
                        'external_type'     => $this->post['post_type'],
                        'old_data'          => json_encode($this->post),
                        'rate'              => 0,
                        'created_at'        => Carbon::createFromTimestamp($this->post['date']),
                        'updated_at'        => Carbon::createFromTimestamp($this->post['date']),
                        'active'            => true,
                    ]);

                    if (!empty($this->post['text'])) {
                        $settings = SettingEntity::where('key', SettingEntity::TYPE_WATER_STOP_WORDS)->first();
                        $list = trim($settings->value, ',');
                        $value = implode("','", array_map('trim', explode(',', $list)));

                        $dots = DotEntity::select('id', 'name', 'water_id', 'region_id', 'district_id')
                            ->whereRaw("plainto_tsquery('russian', name) @@ to_tsvector('russian', '" . trim($this->post['text'], '?') . "') AND (name NOT IN ('" . $value . "') OR (name IN ('" . $value . "') AND array_upper(string_to_array(name, ' '), 1) > 1))")
                            ->orWhereRaw("plainto_tsquery('russian', alias) @@ to_tsvector('russian', '" . trim($this->post['text'], '?') . "')")
                            ->get();

                        if ($dots->count() > 0) {
                            $post->update([
                                'dot_id'      => $dots->first()->id,
                                'water_id'    => $dots->first()->water_id,
                                'region_id'   => $dots->first()->region_id,
                                'district_id' => $dots->first()->district_id,
                                'waters_founded' => $dots->toJson(),
                            ]);
                        } else {
                            $waters = WaterEntity::select('id', 'name')
                                ->whereRaw("plainto_tsquery('russian', name) @@ to_tsvector('russian', '" . trim($this->post['text'], '?') . "') AND (name NOT IN ('" . $value . "') OR (name IN ('" . $value . "') AND array_upper(string_to_array(name, ' '), 1) > 1))")
                                ->orWhereRaw("plainto_tsquery('russian', alias) @@ to_tsvector('russian', '" . trim($this->post['text'], '?') . "')")
                                ->get();

                            if ($waters->count() > 0) {
                                $post->update([
                                    'water_id' => $waters->first()->id,
                                    'waters_founded' => $waters->toJson(),
                                ]);
                            }
                        }
                    }

                    if (!empty($this->post['attachments'])) {
                        foreach ($this->post['attachments'] as $attachment) {
                            $file = '';

                            if ($attachment['type'] == 'photo') {
                                if (empty($attachment['proto']['sizes'])) {
                                    $size = 0;

                                    foreach ($attachment['photo'] as $key => $value) {
                                        if (str_contains($key, 'photo_')) {
                                            list($photoString, $photoSize) = explode('_', $key);

                                            if ($photoSize > $size) {
                                                $size = $photoSize;
                                            }
                                        }
                                    }
                                    $file = $attachment['photo']['photo_' . $size];
                                } else {
                                    $size = 0;

                                    foreach ($attachment['photo']['sizes'] as $photo) {
                                        if ($photo['width'] > $size) {
                                            $attachment = $photo;
                                            $size = $photo['width'];
                                        }
                                    }

                                    $file = $attachment['url'];
                                }

                                $filename = basename($file);
                                $folderName = $post->old_id . '_' . $this->post['date'] . '_' . md5($post->content);
                                $path = '/uploads/posts/' . $folderName . '/' . $filename;

                                if (!file_exists($path)) {
                                    $contents = file_get_contents($file);
                                    Storage::put('/public' . $path, $contents);
                                }

                                $attachment = AttachmentEntity::create([
                                    'type'   => 'image',
                                    'value'  => '/storage' . $path,
                                    'active' => true,
                                ]);
                            }

                            if ($attachment['type'] == 'video') {

                                if (!empty($attachment['video']['platform'])) {
                                    $attachment['video']['platform'] = strtolower($attachment['video']['platform']);
                                } else {
                                    $attachment['video']['platform'] = 'vkontakte';
                                }

                                $vk = new VKApiClient();
                                $videos = $vk->video()->get('b98a2e811c5e2a635246aabf93ac9bfee0a9acbc94308cea36e01a20f319ad1081c280ee8fb4ee192ed5f', [
                                    'owner_id' => $attachment['video']['owner_id'],
                                    'videos'   => $attachment['video']['owner_id'] . '_' . $attachment['video']['id'],
                                ]);

                                if (!empty($videos['items'])) {
                                    foreach ($videos['items'] as $video) {
                                        if (preg_match('/youtube\.com\/embed\/([^\&\?\/]+)/', $video['player'], $player)) {
                                            $attachment = AttachmentEntity::create([
                                                'type'   => $attachment['video']['platform'],
                                                'value'  => $player[1],
                                                'active' => true,
                                            ]);
                                        } else {
                                            $attachment = AttachmentEntity::create([
                                                'type'   => $attachment['video']['platform'],
                                                'value'  => $video['player'],
                                                'active' => false,
                                            ]);
                                        }
                                    }
                                } else {
                                    $size = 0;

                                    foreach ($attachment['video'] as $key => $value) {
                                        if (str_contains($key, 'photo_')) {
                                            list($photoString, $photoSize) = explode('_', $key);

                                            if ($photoSize > $size) {
                                                $size = $photoSize;
                                            }
                                        }
                                    }

                                    $file = $attachment['video']['photo_' . $size];
                                    $filename = basename($file);
                                    $folderName = $post->old_id . '_' . $this->post['date'] . '_' . md5($post->content);
                                    $path = '/uploads/posts/' . $folderName . '/' . $filename;

                                    if (file_exists($path)) {
                                        $contents = file_get_contents($file);
                                        Storage::put('/public' . $path, $contents);
                                    }

                                    $attachment = AttachmentEntity::create([
                                        'type'   => 'image',
                                        'value'  => '/storage' . $path,
                                        'active' => true,
                                    ]);
                                }
                            }

                            if ($attachment['type'] == 'doc' && !(empty($attachment['type']['doc'])) && is_array($attachment['type']['doc']) && in_array($attachment['type']['doc']['ext'], ['jpg', 'png', 'gif'])) {
                                $file = $attachment['type']['doc']['url'];
                                $filename = basename($file);
                                $folderName = $post->old_id . '_' . $this->post['date'] . '_' . md5($post->content);
                                $path = '/uploads/posts/' . $folderName . '/' . $filename;

                                if (file_exists($path)) {
                                    $contents = file_get_contents($file);
                                    Storage::put('/public' . $path, $contents);
                                }

                                $attachment = AttachmentEntity::create([
                                    'type'   => 'image',
                                    'value'  => '/storage' . $path,
                                    'active' => true,
                                ]);
                            }

                            if ($attachment instanceof AttachmentEntity) {
                                $post->attachments()->save($attachment);
                            }
                        }
                    }
                } else {
                    \Log::info(json_encode($this->post));
                }
            });
        }
    }
}
