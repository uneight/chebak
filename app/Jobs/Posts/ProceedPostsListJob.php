<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 4/27/18
 * Time: 9:18 PM
 */

namespace App\Jobs\Posts;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProceedPostsListJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $list;

    /**
     * Create a new job instance.
     *
     * @param $list mixed Response from API
     *
     * @return void
     */
    public function __construct ($list = null)
    {
        $this->list = $list;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle ()
    {
        foreach ($this->list as $item) {
            ProceedPostsItemJob::dispatch($item);
        }
    }
}