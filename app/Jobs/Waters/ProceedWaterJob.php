<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 4/10/18
 * Time: 8:57 PM
 */

namespace App\Jobs\Waters;

use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use App\Entities\Geo\DistrictEntity;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Storage;

class ProceedWaterJob implements ShouldQueue
{    
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    const TYPE_LAKES = 'lakes';
    const TYPE_RIVERS = 'rivers';
    const TYPES = [
        'lakes' => '"natural"="water"',
        'rivers' => '"waterway"="river"',
    ];

    protected $type;
    protected $district;
    protected $data = [];

    /**
     * Create a new job instance.
     *
     * @param $district DistrictEntity
     * @param $type     string List of waters
     *
     * @return void
     */
    public function __construct (DistrictEntity $district, string $type = '')
    {
        $this->district = $district;
        $this->type = $type;
    }

    /**
     * Execute the job.
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle ()
    {
        $client = new Client();
        $filename = $this->type . '_' . $this->district->id . '_' .$this->district->region_id . '.json';

        if (!Storage::exists($filename)) {
            $data = $client->request('GET', 'https://overpass-api.de/api/interpreter', [
                'query' => [
                    'data' => '[out:json][timeout:25];area["name"="'. $this->district->name .'"]["addr:region"="'. trim(str_replace('Республика', '', $this->district->region->name)) .'"]->.searchArea;(node[' . self::TYPES[$this->type] . '](area.searchArea);way[' . self::TYPES[$this->type] . '](area.searchArea);relation[' . self::TYPES[$this->type] . '](area.searchArea););out;>;out skel qt;',
                ]
            ])->getBody()->getContents();
            Storage::put($filename, $data);
        } else {
            $data = Storage::get($filename);
        }

        $this->data = json_decode($data)->elements;

        switch ($this->type) {
            case self::TYPE_LAKES:
                ProceedLakesArrayJob::dispatch($this->district, $this->data);
                break;
            case self::TYPE_RIVERS:
                ProceedRiversArrayJob::dispatch($this->district, $this->data);
                break;
        }
    }
}