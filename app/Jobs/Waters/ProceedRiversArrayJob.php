<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 17.05.2018
 * Time: 15:38
 */

namespace App\Jobs\Waters;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Entities\Geo\DistrictEntity;

class ProceedRiversArrayJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;
    protected $district;

    /**
     * Create a new job instance.
     *
     * @param $district DistrictEntity
     * @param $data     array List of waters
     *
     * @return void
     */
    public function __construct (DistrictEntity $district, array $data = [])
    {
        $this->district = $district;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle ()
    {
        $data = [
            'way' => [],
            'node' => [],
            'relation' => [],
        ];

        foreach ($this->data as $item) {
            $data[$item->type][] = $item;
        }

        foreach (array_merge($data['way'], $data['relation']) as $way) {
            ProceedRiverJob::dispatch($this->district, $way, $data['node']);
        }
    }
}