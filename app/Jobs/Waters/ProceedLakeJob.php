<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 4/10/18
 * Time: 9:45 PM
 */

namespace App\Jobs\Waters;

use App\Entities\Geo\DistrictEntity;
use Illuminate\Bus\Queueable;
use App\Entities\Geo\WaterEntity;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use DB;
use Uneight\Settings\Entities\SettingEntity;

class ProceedLakeJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $district;
    protected $nodes;
    protected $way;

    /**
     * Create a new job instance.
     *
     * @param $district  DistrictEntity
     * @param $way       object
     * @param $nodes     array List of nodes
     *
     * @return void
     */
    public function __construct (DistrictEntity $district, $way, array $nodes = [])
    {
        $this->way = $way;
        $this->nodes = $nodes;
        $this->district = $district;
    }

    /**
     * Execute the job.
     */
    public function handle ()
    {
        $nodes = [];

        $water = WaterEntity::where('api_id', $this->way->id)
            ->orWhere(function ($query) {
                if (!empty($this->way->tags->name)) {
                    $query->where('name', $this->way->tags->name);
                }
                if (!empty($this->way->tags->water)) {
                    $query->where('type', $this->way->tags->water);
                }
                $query->whereHas('districts', function ($query) {
                    $query->where('geo_districts.id', $this->district->id);
                });
            })->first();

        foreach ($this->nodes as $node) {
            if (!empty($this->way->nodes) && in_array($node->id, $this->way->nodes)) {
                $nodes[] = $node;
            }
        }

        if (!$water) {

            if (!empty($this->way->tags->water)) {
                $strReplace = SettingEntity::where('key', SettingEntity::TYPE_WATER_CUT_WORDS)->first();
                $arrayReplace = explode(',', $strReplace);

                array_walk($arrayReplace, function ($item, $key) {
                    $this->way->tags->water = str_replace($item, '', $this->way->tags->water);
                });
            }

            $water = new WaterEntity();
            $water->api_id = $this->way->id;
            $water->api_type = $this->way->type ?? null;
            $water->type = !(empty($this->way->tags->water)) ? $this->way->tags->water : null;
            $water->name = !(empty($this->way->tags->name)) ? trim($this->way->tags->name) : null;
            $water->nodes = json_encode($nodes);
        } else {
            $nodes = collect($nodes)->keyBy('id');
            $waterNodes = collect(json_decode($water->nodes))->keyBy('id');

            if (count($nodes) > count($waterNodes)) {
                $diff = [];
                $diffKeys = array_diff($nodes->keys()->toArray(), $waterNodes->keys()->toArray());

                foreach ($diffKeys as $key) {
                    $diff[] = $nodes[$key];
                }

                $water->nodes = json_encode(array_merge(json_decode($water->nodes), $diff));
            }
        }

        $aliases = [];

        if (!empty($this->way->tags)) {
            $tags = (array)$this->way->tags;

            foreach ($tags as $key => $value) {
                if (strpos($key, 'name')) {
                    $aliases[] = $value;
                }
            }

            if (count($aliases) > 0) {
                $water->alias = trim(implode(',', array_unique(array_merge(explode(',', $water->alias), $aliases))), ',');
            }
        }

        $water->save();

        $this->district->waters()->syncWithoutDetaching($water->id);
        $this->district->region->waters()->syncWithoutDetaching($water->id);
    }
}