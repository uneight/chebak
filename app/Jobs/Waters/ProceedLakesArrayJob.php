<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 4/10/18
 * Time: 9:18 PM
 */

namespace App\Jobs\Waters;

use Illuminate\Bus\Queueable;
use App\Entities\Geo\DistrictEntity;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProceedLakesArrayJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;
    protected $district;

    /**
     * Create a new job instance.
     *
     * @param $district DistrictEntity
     * @param $data     array List of waters
     *
     * @return void
     */
    public function __construct (DistrictEntity $district, array $data = [])
    {
        $this->district = $district;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle ()
    {
        $data = [
            'way' => [],
            'node' => [],
            'relation' => [],
        ];

        foreach ($this->data as $item) {
            $data[$item->type][] = $item;
        }

        foreach (array_merge($data['way'], $data['relation']) as $way) {
            ProceedLakeJob::dispatch($this->district, $way, $data['node']);
        }
    }
}