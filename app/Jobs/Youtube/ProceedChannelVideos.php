<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 10/24/18
 * Time: 11:05 AM
 */

namespace App\Jobs\Youtube;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Uneight\Attachments\Entities\AttachmentEntity;
use Uneight\Categories\Entities\CategoryEntity;
use Uneight\Posts\Entities\PostEntity;
use Youtube;


class ProceedChannelVideos implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $videos;

    /**
     * Create a new job instance.
     *
     * @param $videos array
     *
     * @return void
     */
    public function __construct($videos = [])
    {
        $this->videos = $videos;
    }

    /**
     * @throws \Throwable
     */
    public function handle()
    {
        foreach ($this->videos as $video) {

            $attachment = AttachmentEntity::where('value', $video->id->videoId)->first();

            if (empty($attachment) && !empty($video->id) && !empty($video->snippet)) {
                $source = Youtube::getVideoInfo($video->id->videoId, ['id', 'snippet']);

                $category = CategoryEntity::where('slug', CategoryEntity::CATEGORY_UNDEFINED)->first();

                $post = PostEntity::create([
                    'category_id'  => $category->id,
                    'receive_type' => 2,
                    'title'        => $source->snippet->title,
                    'content'      => $source->snippet->description,
                    'old_data'     => json_encode($source),
                    'created_at'   => Carbon::parse($source->snippet->publishedAt),
                    'updated_at'   => Carbon::parse($source->snippet->publishedAt),
                    'active'       => true,
                    'source'       => 'youtube',
                ]);

                $attachment = AttachmentEntity::create([
                    'type'   => 'youtube',
                    'value'  => $video->id->videoId,
                    'active' => true,
                ]);

                $post->attachments()->save($attachment);
            }
        }
    }
}