<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 7/16/18
 * Time: 9:27 PM
 */

namespace App\Jobs\Groups\Item;

use Illuminate\Bus\Queueable;
use App\Entities\Geo\WaterEntity;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Uneight\VkParser\Entities\GroupEntity;

class ProceedLakeJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $group;
    protected $nodes;
    protected $way;

    /**
     * Create a new job instance.
     *
     * @param $group     GroupEntity
     * @param $way       object
     *
     * @return void
     */
    public function __construct (GroupEntity $group, $way)
    {
        $this->way = $way;
        $this->group = $group;
    }

    /**
     * Execute the job.
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle ()
    {
        $water = WaterEntity::where('api_id', $this->way->id)
            ->orWhere(function ($query) {
                if (!empty($this->way->tags->name)) {
                    $query->where('name', $this->way->tags->name);
                }
                if (!empty($this->way->tags->water)) {
                    $query->where('type', $this->way->tags->water);
                }
            })->first();

        if (!empty($water) && !$this->group->waters->contains($water->id)) {
            $this->group->waters()->syncWithoutDetaching($water->id);
        }
    }
}