<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 7/16/18
 * Time: 9:24 PM
 */

namespace App\Jobs\Groups\Chunk;

use App\Jobs\Groups\Item\ProceedLakeJob;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Uneight\VkParser\Entities\GroupEntity;

class ProceedLakesArrayJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;
    protected $group;

    /**
     * Create a new job instance.
     *
     * @param $group    GroupEntity
     * @param $data     array List of waters
     *
     * @return void
     */
    public function __construct (GroupEntity $group, array $data = [])
    {
        $this->group = $group;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle ()
    {
        $data = [
            'way'      => [],
            'node'     => [],
            'relation' => [],
        ];

        foreach ($this->data as $item) {
            $data[$item->type][] = $item;
        }

        foreach ($data['way'] as $way) {
            ProceedLakeJob::dispatch($this->group, $way);
        }
    }
}