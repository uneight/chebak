<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 7/16/18
 * Time: 9:22 PM
 */

namespace App\Jobs\Groups;

use App\Jobs\Groups\Chunk\ProceedLakesArrayJob;
use App\Jobs\Groups\Chunk\ProceedRiversArrayJob;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Storage;
use Uneight\VkParser\Entities\GroupEntity;

class SearchGroupWatersJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const TYPE_LAKES = 'lakes';
    const TYPE_RIVERS = 'rivers';
    const TYPES = [
        'lakes'  => '"natural"="water"',
        'rivers' => '"waterway"="river"',
    ];
    const DISTANCE = 150000; // In meters

    protected $type;
    protected $group;
    protected $data = [];

    /**
     * Create a new job instance.
     *
     * @param $group    GroupEntity
     * @param $type     string List of waters
     *
     * @return void
     */
    public function __construct (GroupEntity $group, string $type = '')
    {
        $this->group = $group;
        $this->type = $type;
    }

    /**
     * Execute the job.
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle ()
    {
        $client = new Client();
        $filename = 'waters/groups/' . $this->type . '_' . $this->group->locality->id . '_' . $this->group->locality->region_id . '.json';
        $around = 'around:' . self::DISTANCE . ',' . $this->group->locality->latitude . ',' . $this->group->locality->longitude;

        if (!Storage::exists($filename)) {
            $data = $client->request('GET', 'https://overpass-api.de/api/interpreter', [
                'query' => [
                    'data' => '[out:json][timeout:25];(node(' . $around . ')[' . self::TYPES[$this->type] . '];way(' . $around . ')[' . self::TYPES[$this->type] . '];relation(' . $around . ')[' . self::TYPES[$this->type] . '];);out;>;out skel qt;',
                ],
            ])->getBody()->getContents();
            Storage::put($filename, $data);
        } else {
            $data = Storage::get($filename);
        }

        $this->data = json_decode($data)->elements;

        switch ($this->type) {
            case self::TYPE_LAKES:
                ProceedLakesArrayJob::dispatch($this->group, $this->data);
                break;
            case self::TYPE_RIVERS:
                ProceedRiversArrayJob::dispatch($this->group, $this->data);
                break;
        }
    }
}