<?php

namespace App\Jobs;

use App\Entities\Geo\RegionEntity;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProceedRegionJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $region_id;
    protected $country_id;
    protected $api_key = '907c51415d075543e30e073c9a264d57';

    /**
     * Create a new job instance.
     *
     * @param $region_id
     * @param $country_id
     *
     * @return void
     */
    public function __construct($region_id, $country_id = null)
    {
        $this->region_id = $region_id;
        $this->country_id = $country_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $client = new Client();

        $response = json_decode($client->get('https://htmlweb.ru/geo/api.php', [
            'query' => [
                'area' => $this->region_id,
                'api_key' => $this->api_key,
                'info' => true,
                'json' => true,
            ],
        ])->getBody()->getContents());

        $params = explode(' ', $response->region->name);

        $response2 = json_decode($client->get('http://kladr-api.ru/api.php', [
            'query' => [
                'query' => $params[0],
                'contentType' => 'region'
            ]
        ])->getBody()->getContents());

        if (count($response2->result) == 0) {
            $type = '';
            $kladr = null;
        } else {
            $type = $response2->result[0]->type;
            $kladr = $response2->result[0]->id;
        }

        RegionEntity::create([
            'country_id' => $this->country_id,
            'api_id_htmlweb' => $this->region_id,
            'api_id_kladr' => $kladr,
            'type' => $type,
            'name' => $response->region->name,
            'auto_number_codes' => $response->region->autocod,
            'iso' => isset($response->region->iso) ? $response->region->iso : '',
            'timezone' => $response->time_zone,
            'source' => json_encode(['count' => count($response2->result), 'ressponse' => [
                'htmlweb' => $response,
                'kladr' => $response2->result,
            ]]),
            'active' => true
        ]);
    }
}
