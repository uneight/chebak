<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Validator;

class EmailOrPhoneValidationRule implements Rule
{

    protected $id;

    /**
     * Create a new rule instance.
     *
     * @param null $id
     */
    public function __construct($id = null)
    {
        $this->id = $id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (is_numeric($value)) {
            $validator = Validator::make([
                'phone' => $value,
            ], [
                'phone' => 'phone:AUTO',
            ]);
        } else {
            $validator = Validator::make([
                'email' => $value,
            ], [
                'email' => 'email',
            ]);
        }

        return $validator->passes();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {

    }
}
