<?php
/**
 * Created by PhpStorm.
 * User: levcenkoivan
 * Date: 10/24/18
 * Time: 11:01 AM
 */

namespace App\Console\Commands;

use App\Jobs\Youtube\ProceedChannelVideos;
use Illuminate\Console\Command;
use GuzzleHttp\Client;
use Carbon\Carbon;
use Youtube;

class ParseYoutubeChannels extends Command
{
    protected $selfChannel = 'UCLipDk5b73CGutsBdQdClRw';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:youtube';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse Youtube channels for get videos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct ()
    {
        parent::__construct();
    }

    /**
     * @throws \Throwable
     */
    public function handle ()
    {
        $client = new Client();
        $response = $client->get('https://www.youtube.com/channel/' . $this->selfChannel . '/channels');

        preg_match_all('/href=["\']\/channel\/?([^"\'>]+)["\']?/', $response->getBody()->getContents(), $matches);
        $channels = array_unique($matches[1]);

        $key = array_search($this->selfChannel, $channels);
        $offset = array_search($key, array_keys($channels));
        $channels = array_slice($channels, $offset + 1);

        unset($channels[0]);

        $this->info("Start parse!");

        foreach ($channels as $channel) {
            $this->proceedChannel($channel);
        }

        $this->info("Finish parse!");
    }

    protected function proceedChannel($channel)
    {
        $params = [
            'type'           => 'video',
            'channelId'      => $channel,
            'part'           => implode(', ', ['id', 'snippet']),
            'maxResults'     => 50,
            'publishedAfter' => Carbon::now()->subDay(1)->toRfc3339String(),
        ];

        $videos = Youtube::searchAdvanced($params);

        if (!empty($videos)) {
            ProceedChannelVideos::dispatch($videos);
        }
    }
}