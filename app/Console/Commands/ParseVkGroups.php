<?php

namespace App\Console\Commands;

use App\Jobs\Posts\ProceedVkRequestChunk;
use Illuminate\Console\Command;
use Uneight\VkParser\Entities\GroupEntity;
use VK\Client\VKApiClient;

class ParseVkGroups extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:vk {groups?*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse VK groups for get posts.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct ()
    {
        parent::__construct();
    }

    /**
     * @throws \Throwable
     */
    public function handle ()
    {
        $list = $this->argument('groups');

        $groups = (!empty($list)) ? GroupEntity::whereIn('vk_id', $list)->get() : GroupEntity::all();

        $this->info("Start parse!");

        foreach ($groups as $group) {
            $this->info("Proceed " . $group->vk_id . " group...");
            $this->proceedGroup($group);
        }

        $this->info("Finish parse!");
    }

    protected function proceedGroup(GroupEntity $group)
    {

        $count = 100;
        $vk = new VKApiClient();

        try {
            $response = $vk->wall()->get('b98a2e811c5e2a635246aabf93ac9bfee0a9acbc94308cea36e01a20f319ad1081c280ee8fb4ee192ed5f', [
                'owner_id' => $group->vk_id,
                'count'    => 1,
                'extended' => 1,
            ]);

            if ($group->last_count < $response['count']) {
                for ($offset = 0; $offset <= $response['count'] - $group->last_count + 100; $offset += $count) {
                    ProceedVkRequestChunk::dispatch($group, $count, $offset);
                }
            }

            $group->update([
                'last_count'     => $response['count'],
                'last_parsed_at' => date('Y-m-d H:i:s'),
            ]);

            $this->info("Getted ". ($response['count'] - $group->last_count) ." posts!");

        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage());
        }
    }
}
